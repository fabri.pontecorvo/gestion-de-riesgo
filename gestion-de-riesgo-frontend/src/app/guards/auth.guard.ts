import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { RoutesEnum } from '../common/routes-enum';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate, CanLoad {
  constructor(private auth: AuthService, private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const url: any = state.url;
    if (this.auth.isLogged()) {
      if (url === '/'+RoutesEnum.LOGIN || url === '/' || url === '') {
        this.router.navigate(['/' + RoutesEnum.PAGES]);
      }
    } else {
      if (url === '/' || url === '') {
        this.router.navigate(['/' + RoutesEnum.LOGIN]);
      }
    }

    return true;



  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isLogged()) {
      return true;
    } else {
      return false;
    }
  }

}
