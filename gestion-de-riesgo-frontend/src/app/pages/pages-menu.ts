import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Inicio',
    icon: 'home-outline',
    link: '/pages/inicio',
    home: true,
  },
  {
    title: 'Gestionar programa',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Creación de programas',
        link: '/pages/programa/creacion-programas',
      },
      {
        title: 'Consulta de programas',
        link: '/pages/programa/consulta',
      },
      {
        title: 'Seguimiento de programas',
        link: '/pages/programa/seguimiento',
      },
    ],
  },
  {
    title: 'Gestión de beneficiarios',
    icon: 'person-outline',
    children: [
      {
        title: 'Consutla beneficiarios',
        link: '/pages/beneficiario/consulta',
      },
      {
        title: 'Consulta de auditoría',
        link: '/pages/beneficiario/auditoria',
      }
    ],
  },
  {
    title: 'Auditoría',
    icon: 'clipboard-outline',
    children: [
      {
        title: 'Bandeja de auditoría',
        link: '/pages/auditoria/bandeja',
      }
    ],
  },

];
