import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { StateService } from '../@core/utils/state.service';


@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu" ></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  menu: any;


  constructor(private state: StateService) { }

  ngOnInit(): void {
    this.menu = MENU_ITEMS;
  }




}
