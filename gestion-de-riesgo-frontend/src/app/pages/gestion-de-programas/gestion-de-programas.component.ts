import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gestion-de-programas',
  template:'<router-outlet></router-outlet>',
})
export class GestionDeProgramasComponent  implements OnInit{

  ngOnInit(): void {
    
  }
}
