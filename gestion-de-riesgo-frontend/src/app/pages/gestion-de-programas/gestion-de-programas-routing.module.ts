import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GestionDeProgramasComponent } from './gestion-de-programas.component';
import { CreacionProgramasComponent } from './creacion-programas/creacion-programas.component';


const routes: Routes = [
  {
    path:'',
    component:GestionDeProgramasComponent,
    children:[
      {
        path:'creacion-programas',
        component:CreacionProgramasComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionDeProgramasRoutingModule { }
