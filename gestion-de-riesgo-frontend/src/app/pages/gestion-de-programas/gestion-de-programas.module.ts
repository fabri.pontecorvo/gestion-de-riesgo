import { NgModule, LOCALE_ID, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { GestionDeProgramasRoutingModule } from './gestion-de-programas-routing.module';
import { CreacionProgramasComponent } from './creacion-programas/creacion-programas.component';
import { GestionDeProgramasComponent } from './gestion-de-programas.component';
import {
  NbCardModule, NbInputModule, NbDatepickerModule, NbButtonModule, NbRadioModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule, NbCheckboxModule,
} from '@nebular/theme';
import { DatosGeneralesComponent } from './creacion-programas/stepper-poblacion-objeto/datos-generales/datos-generales.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { GraficoComponent } from './creacion-programas/stepper-poblacion-objeto/grafico/grafico.component';
import { ChartModule } from 'angular2-chartjs';
import { GrupoDeRiesgosComponent } from './creacion-programas/stepper-poblacion-objeto/grupo-de-riesgos/grupo-de-riesgos.component';
import { PoblacionObjetoSexoEdadComponent } from './creacion-programas/stepper-poblacion-objeto/poblacion-objeto-sexo-edad/poblacion-objeto-sexo-edad.component';
import { PoblacionObjetoEspecialComponent } from './creacion-programas/stepper-poblacion-objeto/poblacion-objeto-especial/poblacion-objeto-especial.component';
import { PoblacionAgregadaComponent } from './creacion-programas/stepper-poblacion-objeto/poblacion-agregada/poblacion-agregada.component';
import { StepperPoblacionObjetoComponent } from './creacion-programas/stepper-poblacion-objeto/stepper-poblacion-objeto.component';
import { StepperEtapasComponent } from './creacion-programas/stepper-etapas/stepper-etapas.component';
import { DiagnosticosComponent } from './creacion-programas/stepper-etapas/diagnosticos/diagnosticos.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { ProcedimientosComponent } from './creacion-programas/stepper-etapas/procedimientos/procedimientos.component';



registerLocaleData(es);

@NgModule({
  declarations: [
    GestionDeProgramasComponent,
    CreacionProgramasComponent,
    DatosGeneralesComponent,
    GraficoComponent,
    GrupoDeRiesgosComponent,
    PoblacionObjetoSexoEdadComponent,
    PoblacionObjetoEspecialComponent,
    PoblacionAgregadaComponent,
    StepperPoblacionObjetoComponent,
    StepperEtapasComponent,
    DiagnosticosComponent,
    ProcedimientosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule,

    NbStepperModule,
    NbTabsetModule,
    AngularFontAwesomeModule,
    GestionDeProgramasRoutingModule,
    NbCardModule,
    NbInputModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
    NbEvaIconsModule,
    Ng2SmartTableModule,
    NbIconModule,
    ChartModule,
    AutocompleteLibModule,
    NbCheckboxModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es-ES' }],


})
export class GestionDeProgramasModule { }
