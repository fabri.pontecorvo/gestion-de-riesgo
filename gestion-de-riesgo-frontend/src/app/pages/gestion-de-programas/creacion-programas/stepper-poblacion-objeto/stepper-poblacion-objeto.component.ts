import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PoblacionAgregadaComponent } from './poblacion-agregada/poblacion-agregada.component';
import { GraficoComponent } from './grafico/grafico.component';
import { HttpService } from '../../../../services/http.service';
import { environment } from '../../../../../environments/environment';
import { ControllerUrlEnum } from '../../../../common/urlEnum';
import { CreacionProgramaMockService } from '../../../../@core/mock/creacion-programa-mock.service';
import { Init } from '../../../../@core/data/creacion-programa';
import { GrupoDeRiesgosComponent } from './grupo-de-riesgos/grupo-de-riesgos.component';
import { debugOutputAstAsTypeScript } from '@angular/compiler';

@Component({
  selector: 'app-stepper-poblacion-objeto',
  templateUrl: './stepper-poblacion-objeto.component.html',
  styleUrls: ['./stepper-poblacion-objeto.component.scss']
})
export class StepperPoblacionObjetoComponent implements OnInit {

  @Input() formPrograma: FormGroup;
  datosInit: Init = JSON.parse(localStorage.getItem('init'));
  @ViewChild('poblacionAgregada', { static: false }) public poblacionAgregada: PoblacionAgregadaComponent;
  @ViewChild('grupoRiesgo', { static: false }) public grupoRiesgo: GrupoDeRiesgosComponent;

  @ViewChild('grafico', { static: false }) public grafico: GraficoComponent;

  dataPoblacionAgregada = [];
  dataGrupoRiesgo: any;
  urlActualizarGrafico: string = environment.url + ControllerUrlEnum.poblacionObjeto;
  constructor(private service: HttpService) { }

  ngOnInit() {
  }

  agregarPoblacion(event: any) {
    this.poblacionAgregada.agregar(event);
  }

  agregarGrupoRiesgo(event: any) {
    this.grupoRiesgo.agregar(event)
  }

  setDataPoblacionAgregada(event: any) {
    if (event.reiniciar) { this.dataPoblacionAgregada = []; }
    if (event.event.sexo !== undefined) {
      let dato = {
        codGenero: event.event.sexo.codigo,
        edadDesde: event.event.edadDesde,
        edadHasta: event.event.edadHasta,
        codProvincia: event.event.provincia.codigo,
        codLocalidad: event.event.localidad.codigo
      };
      this.dataPoblacionAgregada.push(dato);

    } else {
      let dato = {
        codGenero: event.event.poblacion.codigo,
        codProvincia: event.event.provincia.codigo,
        codLocalidad: event.event.localidad.codigo
      };
      this.dataPoblacionAgregada.push(dato);

    }

  }

  eliminarFila(indice: any) {
    this.dataPoblacionAgregada = this.dataPoblacionAgregada.filter((x, i) =>
      i !== indice
    )

    this.dataPoblacionAgregada;
  }

  actualizar() {
    // this.setDataPoblacionAgregada(this.poblacionAgregada.source.getAll());
    this.service.postRequest(this.urlActualizarGrafico, this.dataPoblacionAgregada).subscribe(
      (resp) => {
        this.grafico.graficar(resp.cantMasculinos, this.grafico.doNegative(resp.cantFemeninos), resp.rangosEtarios);
        this.grafico.cargarTabla(resp);
      },
      (error) => {
      },
      () => {
      });

  }

}
