import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'app-datos-generales',
  templateUrl: './datos-generales.component.html',
  styleUrls: ['./datos-generales.component.scss']
})
export class DatosGeneralesComponent implements OnInit {

  @Input() formPrograma: FormGroup;
  @Input() isSubmitted: boolean;
  minFechaInicio: Date;
  datePickerFechaInicio: Date;

  constructor(public dateService: NbDateService<Date>) {
    this.minFechaInicio = this.dateService.today();
    this.datePickerFechaInicio = this.dateService.today();
    this.minFechaInicio.setDate(this.minFechaInicio.getDate() - 1)
  }

  ngOnInit() {
  }

  updateDate() {
  }
  get formControlsPrograma(): any { return this.formPrograma.controls; }

} 
