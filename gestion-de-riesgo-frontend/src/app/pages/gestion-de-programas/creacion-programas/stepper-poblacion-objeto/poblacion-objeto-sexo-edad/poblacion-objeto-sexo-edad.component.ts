import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { PoblacionObjetoSexoEdadDto } from '../../../../../@core/data/poblacion-objeto-sexo-edad-dto';
import { Init } from '../../../../../@core/data/creacion-programa';



@Component({
  selector: 'app-poblacion-objeto-sexo-edad',
  templateUrl: './poblacion-objeto-sexo-edad.component.html',
  styleUrls: ['./poblacion-objeto-sexo-edad.component.scss']
})
export class PoblacionObjetoSexoEdadComponent {


  datosInit: Init = JSON.parse(localStorage.getItem('init'));
  @Output() agregarPoblacion: EventEmitter<any> = new EventEmitter();
  dto: PoblacionObjetoSexoEdadDto;
  sexoSelected: string;
  edadDesdeSelected: string;
  edadHastaSelected: string;
  provinciaSelected: any;
  localidadSelected: any;

  constructor() {
  }

  agregar() {
    this.provinciaSelected;
    this.dto = {
      sexo: this.sexoSelected,
      edadDesde: this.edadDesdeSelected,
      edadHasta: this.edadHastaSelected,
      provincia: this.provinciaSelected.provincia,
      localidad: this.localidadSelected
    }
    this.agregarPoblacion.emit(this.dto);
    this.clear();

  }
  clear() {
    this.sexoSelected = null;
    this.edadDesdeSelected = null;
    this.edadHastaSelected = null;
    this.provinciaSelected = null;
    this.localidadSelected = null;
  }
}







