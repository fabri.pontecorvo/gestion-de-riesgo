import { Component, OnDestroy, OnInit, Output, EventEmitter, } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import { Init } from '../../../../../@core/data/creacion-programa';
import { HttpService } from '../../../../../services/http.service';
import { environment } from '../../../../../../environments/environment';
import { ControllerUrlEnum } from '../../../../../common/urlEnum';
@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.scss']
})
export class GraficoComponent implements OnDestroy, OnInit {

  @Output() agregarGrupoRiesgo: EventEmitter<any> = new EventEmitter();

  datosInit: Init = JSON.parse(localStorage.getItem('init'));

  data: any;
  options: any;
  grafico: any;

  settings = {
    noDataMessage: "Sin datos",
    hideSubHeader: true,
    pager: {
      perPage: 6
    },
    actions:
    {
      delete: false,
      add: false,
      edit: false,
    },
    columns: {

      edad: {
        title: 'Edad',
        type: 'string',
      },
      hombres: {
        title: 'Hombres',
        type: 'string',
      },
      mujeres: {
        title: 'Mujeres',
        type: 'string',

      },
    }
  }
  source: LocalDataSource = new LocalDataSource();
  dTable = [];
  urlPoblacionObjeto = environment.url + ControllerUrlEnum.poblacionObjeto;
  filtros: any[] = [{
    codGenero: null,
    edadDesde: 0,
    edadHasta: 120,
    codProvincia: null,
    codLocalidad: null,
  }];

  constructor(private theme: NbThemeService, private service: HttpService) {
    // this.datosInit;
    this.source = new LocalDataSource(this.dTable);
  }
  ngOnInit(): void {
    this.datosInit;
    this.service.postRequest(this.urlPoblacionObjeto, this.filtros).subscribe(
      (resp) => {
        this.graficar(resp.cantMasculinos, this.doNegative(resp.cantFemeninos), resp.rangosEtarios);
        this.cargarTabla(resp);
      },
      (error) => {
      },
      () => {
      });

  }
  cargarTabla(resp: any) {
    this.source.empty();
    this.source.reset();

    for (let i = 0; resp.rangosEtarios.length > i; i++) {
      let dato = {
        edad: resp.rangosEtarios[i],
        hombres: resp.cantMasculinos[i],
        mujeres: resp.cantFemeninos[i]
      }
      this.source.add(dato);
    }
    this.source.refresh();
    this.agregarGrupoRiesgo.emit(resp.gruposRiegos)
  }
  doNegative(list: any): any {
    let listNegative = [];
    list.forEach(element => {
      listNegative.push(element * -1)
    });
    debugger
    return listNegative;
  }
  graficar(datosMasculino: any, datosFemenino: any, rangosEtarios: any) {
    this.grafico = this.theme.getJsTheme().subscribe(config => {

      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: rangosEtarios,
        datasets: [{
          label: 'Mujeres',
          backgroundColor: "#ffa500",
          borderWidth: 0,
          data: datosFemenino,
        }, {
          label: 'Hombres',
          borderWidth: 0,
          backgroundColor: '#00c9deb0',
          data: datosMasculino,
        },
        ],
      };

      this.options = {
        responsive: true,
        maintainAspectRatio: true,
        elements: {
          rectangle: {
            borderWidth: 2,
          },
        },
        scales: {
          xAxes: [
            {
              stacked: true,
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
                callback: function (label, index, labels) {
                  return Math.abs(label);
                }
              },
            },
          ],
          yAxes: [
            {
              stacked: true,
              gridLines: {
                display: false,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return data.datasets[tooltipItem.datasetIndex].label + ': ' + Math.abs(tooltipItem.xLabel);
            }
          }
        },
        legend: {
          position: 'top',
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    }
    );
  }

  actualizarGrafico(data: any) {
    this.data = data;
  }

  ngOnDestroy(): void {
    this.grafico.unsubscribe();
  }


}
