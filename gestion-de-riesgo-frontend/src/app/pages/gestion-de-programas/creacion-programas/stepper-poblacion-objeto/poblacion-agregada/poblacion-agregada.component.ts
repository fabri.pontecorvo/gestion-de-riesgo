import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../../../@core/data/smart-table';

@Component({
  selector: 'app-poblacion-agregada',
  templateUrl: './poblacion-agregada.component.html',
  styleUrls: ['./poblacion-agregada.component.scss']
})
export class PoblacionAgregadaComponent implements OnInit {
  @Output() setDataPoblacionAgregada: EventEmitter<{event: any,reiniciar:boolean}> = new EventEmitter();
  @Output() eliminarFila: EventEmitter<any> = new EventEmitter();

  settingsPoblacion = {
    noDataMessage: "Sin datos",
    hideSubHeader: true,
    mode: 'external',
    delete: {
      deleteButtonContent: "<i class='fas fa-trash-alt'></i>"
    },
    actions:
    {
      delete: true,
      add: false,
      edit: false,
    },
    columns: {

      poblacion: {
        title: 'Población',
        type: 'string',
        width: "30%",

      },
      provincia: {
        title: 'Provincia',
        type: 'string',
        width: "30%",
      },
      localidad: {
        title: 'Localidad',
        type: 'string',
        width: "30%",
      },
    }
  }
  settingsSexoEdad = {
    noDataMessage: "Sin datos",
    hideSubHeader: true,
    mode: 'external',
    delete: {
      deleteButtonContent: "<i class='fas fa-trash-alt'></i>"
    },
    actions:
    {
      delete: true,
      add: false,
      edit: false,
    },
    columns: {

      sexo: {
        title: 'Sexo',
        type: 'string',
        width: "18%",

      },
      edadDesde: {
        title: 'Edad Desde',
        type: 'string',
        width: "18%",

      },
      edadHasta: {
        title: 'Edad Hasta',
        type: 'string',
        width: "18%",
      },
      provincia: {
        title: 'Provincia',
        type: 'string',
        width: "18%",
      },
      localidad: {
        title: 'Localidad',
        type: 'string',
        width: "18%",
      },
    }
  }
  settings: any;
  source: LocalDataSource = new LocalDataSource();

  data = [
    // ... aca va la data!
  ];
  tablaActual: string;
  constructor(private service: SmartTableData) {
    this.source = new LocalDataSource(this.data);
  }

  ngOnInit() {
    this.settings = this.settingsSexoEdad;
    this.tablaActual = "sexoEdad";
  }

  agregar(event: any) {
    let reiniciar = false;
    if (event.sexo == null) {
      if (this.tablaActual === "sexoEdad") {
        this.source.empty();
        this.source.reset();
        this.settings = this.settingsPoblacion;
        reiniciar = true;
      }
      this.source.add({
        poblacion: event.poblacion.descripcion,
        provincia: event.provincia.descripcion,
        localidad: event.localidad.descripcion
      });
      this.tablaActual = 'poblacion';
    } else {
      if (this.tablaActual === "poblacion") {
        this.settings = this.settingsSexoEdad;
        this.source.empty();
        this.source.reset();
        reiniciar = true;
      }
      this.source.add({
        sexo: event.sexo.descripcion,
        edadDesde: event.edadDesde,
        edadHasta: event.edadHasta,
        provincia: event.provincia.descripcion,
        localidad: event.localidad.descripcion
      });
      this.tablaActual = 'sexoEdad';
    }
    this.source.refresh();
    this.setDataPoblacionAgregada.emit({event,reiniciar});
  }

  deleteRow(event : any){
    this.source.remove(event.data);
    this.source.refresh();
    this.eliminarFila.emit(event.index);
  }

}
