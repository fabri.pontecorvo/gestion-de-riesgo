import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PoblacionObjetoEspecialDto } from '../../../../../@core/data/poblacion-objeto-especial';
import { Init } from '../../../../../@core/data/creacion-programa';

@Component({
  selector: 'app-poblacion-objeto-especial',
  templateUrl: './poblacion-objeto-especial.component.html',
  styleUrls: ['./poblacion-objeto-especial.component.scss']
})
export class PoblacionObjetoEspecialComponent {
  datosInit: Init = JSON.parse(localStorage.getItem('init'));
  @Output() agregarPoblacion: EventEmitter<any> = new EventEmitter();
  dto: PoblacionObjetoEspecialDto;
  poblacionSelected: string;
  provinciaSelected: any;
  localidadSelected: any;

  constructor() { }

  agregar() {
    this.dto = {
      poblacion: this.poblacionSelected,
      provincia: this.provinciaSelected.provincia,
      localidad: this.localidadSelected
    }
    
    this.agregarPoblacion.emit(this.dto);
    this.clear();
  }

  clear() {
    this.poblacionSelected = null,
    this.provinciaSelected = null;
    this.localidadSelected = null;
  }

}
