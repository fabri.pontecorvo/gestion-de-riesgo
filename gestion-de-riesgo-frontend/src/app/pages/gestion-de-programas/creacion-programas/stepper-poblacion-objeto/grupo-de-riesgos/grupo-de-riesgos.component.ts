import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../../../@core/data/smart-table';
import { PercentPipe } from '@angular/common';

@Component({
  selector: 'app-grupo-de-riesgos',
  templateUrl: './grupo-de-riesgos.component.html',
  styleUrls: ['./grupo-de-riesgos.component.scss']
})
export class GrupoDeRiesgosComponent implements OnInit {

  settings = {
    noDataMessage: "Sin datos",
    hideSubHeader: true,
    actions:
    {
      delete: false,
      add: false,
      edit: false,
    },
    columns: {

      descripcion: {
        title: 'Descripción de Riesgo',
        type: 'string',
        width: "80%",

      },
      frecuenciaPoblacional: {
        title: 'Frecuencia poblacional',
        type: 'string',
        width: "10%",

      },
      porcentajePoblacionRiesgo: {
        title: 'Porcentaje de población en riesgo',
        type: 'string',
        width: "10%",
        valuePrepareFunction: (data) => {  
          debugger; 
          let d = new PercentPipe(data);   
          return d.transform(data/100,'2.2-2','es-ES');
      },
      },
    }
  }




  source: LocalDataSource = new LocalDataSource();

  data = [
  ];
  constructor(private service: SmartTableData) {
    this.source = new LocalDataSource(this.data);
  }

  ngOnInit() {
  }
  agregar(event: any) {
    this.source.empty();
    this.source.reset();
    this.source = new LocalDataSource(event.filter( x => x.frecuenciaPoblacional !=0));
    this.source.refresh();
  }

}
