import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../../services/http.service';
import { CreacionProgramaMockService } from '../../../@core/mock/creacion-programa-mock.service';
import { StepperPoblacionObjetoComponent } from './stepper-poblacion-objeto/stepper-poblacion-objeto.component';
import { Init } from '../../../@core/data/creacion-programa';

@Component({
  selector: 'app-creacion-programas',
  templateUrl: './creacion-programas.component.html',
  styleUrls: ['./creacion-programas.component.scss']
})
export class CreacionProgramasComponent implements OnInit {



  formPrograma: FormGroup;
  formDiagnostico: FormGroup;
  isSubmitted: boolean;
  @ViewChild("step1",{static:false}) step1: StepperPoblacionObjetoComponent;
  datosInit: Init = JSON.parse(localStorage.getItem('init'));
  constructor(private service: HttpService, private formBuilder: FormBuilder, private mock: CreacionProgramaMockService) { }

  ngOnInit(): void {
  
    this.formPrograma = this.formBuilder.group({
      codigo: ['', Validators.required],
      nombre: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFin: [''],
    });

    this.formDiagnostico = this.formBuilder.group({
      descripcion: ['', Validators.required]
    });
  }

  submit() {
    this.formPrograma.controls;
    this.formDiagnostico.controls;
    this.isSubmitted = true;
  }

}