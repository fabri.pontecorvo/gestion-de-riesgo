import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stepper-etapas',
  templateUrl: './stepper-etapas.component.html',
  styleUrls: ['./stepper-etapas.component.scss']
})
export class StepperEtapasComponent implements OnInit {

  @Input() formDiagnostico: FormGroup;

  constructor() { }

  ngOnInit() {
  }  

}
