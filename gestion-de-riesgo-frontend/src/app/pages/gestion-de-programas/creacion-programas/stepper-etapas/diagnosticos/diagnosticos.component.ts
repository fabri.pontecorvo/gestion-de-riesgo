import { Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../../../@core/data/smart-table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutocompleteComponent } from 'angular-ng-autocomplete';


@Component({
  selector: 'app-diagnosticos',
  templateUrl: './diagnosticos.component.html',
  styleUrls: ['./diagnosticos.component.scss']
})
export class DiagnosticosComponent implements OnInit {

  @ViewChild('autocomplete', {static: false}) autocomplete: AutocompleteComponent
 
  @Input() formDiagnostico: FormGroup;
  @Input() isSubmitted: boolean;

  @Output() descripcionNueva = new EventEmitter();
  @Output() dataDiagnosticos = new EventEmitter();

  public source: LocalDataSource;
  public diagnosticoSeleccionado: String;
  public descripcionIngresada: String;

  settings = {
    noDataMessage: "No hay registros",
    hideSubHeader: true,
    actions: { edit: false, columnTitle: 'Acciones', position:'right'},    
    pager: {
      perPage: 5
    },    
    delete: {
      deleteButtonContent: "<i class='fas fa-trash-alt'></i>"
    },
    columns: {
      diagnosticos: {
        title: 'Diagnósticos',
        type: 'string'
      }
    },
    Acciones:
    {
      delete: true,
      add: false,
      edit: false,
    }
  };

  // Data mock prueba
  data = [
    {
      id: 1,
      diagnosticos: "Tratamiento especial alternativo"
    },
    {
      id: 1,
      diagnosticos: "Tratamiento inicial experimental"
    }
  ];


  // Data mock prueba
  keyword = 'name';
  public diagnosticos = [
    {
      id: 1,
      name: 'Examen pesquisa tumor intestino',
    },
    {
      id: 2,
      name: 'Examen pesquisa tumor benigno',
    },
    {
      id: 3,
      name: 'Examen oncológico',
    },
    {
      id: 4,
      name: 'Tratamiento inicial',
    },
    {
      id: 5,
      name: 'Tratamiento final',
    },
    {
      id: 6,
      name: 'Examen intermedio',
    },
    {
      id: 7,
      name: 'Examen pesquisa enfermedad',
    },
    {
      id: 8,
      name: 'Tratamiento traumatologia',
    },
    {
      id: 9,
      name: 'Examen fisioterapeuta',
    },
    {
      id: 10,
      name: 'Examen no convencional',
    }
  ];  
    
  constructor(private service: SmartTableData) {
    this.source = new LocalDataSource(this.data);
  }

  ngOnInit() {    
  }

  selectEvent(item) {
    this.diagnosticoSeleccionado = item.name;       
  }  

  agregarDiagnostico() {
    this.source.add({
      diagnosticos: this.diagnosticoSeleccionado
    });
    this.source.refresh();
    this.clear();
       
  }

  clear() {
    this.diagnosticoSeleccionado = null   
    this.autocomplete.close();
    this.autocomplete.clear();     
  }  

  enviarDatosDiagnostico()
  {
    this.dataDiagnosticos.emit({ diagnosticos: this.source});   
    this.descripcionNueva.emit({descripcion: this.descripcionIngresada}); 
  }

  get formControlsDiagnostico(): any { return this.formDiagnostico.controls; }
    
}
