import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../../../@core/data/smart-table';
import { AutocompleteComponent } from 'angular-ng-autocomplete';

@Component({
  selector: 'app-procedimientos',
  templateUrl: './procedimientos.component.html',
  styleUrls: ['./procedimientos.component.scss']
})
export class ProcedimientosComponent implements OnInit {

  @ViewChild('autocomplete', {static: false}) autocomplete: AutocompleteComponent

  public ambitoSeleccionado: any;
  public especialidadSeleccionada: any;
  public rolSeleccionado: String;
  public cantidadSeleccionada: String;
  public periodoSeleccionado: String;
  public unidadTiempoSeleccionado: any;
  public isReempadronamiento: boolean;
  public source: LocalDataSource;
  public procedimientoSeleccionado: String;
  public isPreescriptor: boolean = false;
  public isEfector: boolean = false;
 
  settings = {
    noDataMessage: "No hay registros",
    hideSubHeader: true,
    actions: { edit: false, columnTitle: 'Acciones', position: 'right' },
    pager: {
      perPage: 5
    },
    delete: {
      deleteButtonContent: "<i class='fas fa-trash-alt'></i>"
    },
    columns: {
      procedimientos: {
        title: 'Descripción de procedimiento',
        type: 'string'
      },
      ambito: {
        title: 'Ámbito',
        type: 'string'
      },
      especialidad: {
        title: 'Especialidad',
        type: 'string'
      },
      rolEspecialidad: {
        title: 'Rol de especialidad',
        type: 'string'
      },
      cantidad: {
        title: 'Cantidad',
        type: 'string'
      },
      periodo: {
        title: 'Periodo',
        type: 'string'
      },
      requerido: {
        title: 'Requerido',
        type: 'string'
      },
      costo: {
        title: 'Costo',
        type: 'string'
      }
    },
    Acciones:
    {
      delete: true,
      add: false,
      edit: false,
    }
  };

  // Data mock prueba
  data = [
    {
      id: 1,
      procedimientos: "Consulta de primera vez por medicina general",
      ambito: "Ambulatorio",
      especialidad:"Clínica Médica",
      rolEspecialidad:"Prescriptor y Efector",
      cantidad:"1",
      periodo:"3 Meses",
      requerido:"Si",
      costo:"535"
    },
    {
      id: 2,
      procedimientos: "Consulta de Control",
      ambito: "No Ambulatorio",
      especialidad:"Clínica Ortopédica",
      rolEspecialidad:"Efector",
      cantidad:"5",
      periodo:"5 Meses",
      requerido:"No",
      costo:"400"
    }
  ];

  // Data mock prueba
  keyword = 'name';
  public procedimientos = [
    {
      id: 1,
      name: 'Consulta especializada',
    },
    {
      id: 2,
      name: 'Consulta educación individual',
    },
    {
      id: 3,
      name: 'Consulta inicial',
    },
    {
      id: 4,
      name: 'Consulta de seguimiento',
    },
    {
      id: 5,
      name: 'Consulta médico internista',
    },
    {
      id: 6,
      name: 'Consulta prioritaria',
    },
    {
      id: 7,
      name: 'Consulta pediátrica',
    },
    {
      id: 8,
      name: 'Consulta de gestación',
    },
    {
      id: 9,
      name: 'Consulta adulto mayor',
    },
    {
      id: 10,
      name: 'Consulta sintomática',
    }
  ];

  // Data mock prueba
  ambitos = [
    {
      id: 1,
      nombre: "ambito prueba uno"
    },
    {
      id: 2,
      nombre: "ambito prueba dos"
    }
  ];

  // Data mock prueba
  especialidades = [
    {
      id: 1,
      nombre: "especialidad uno"
    },
    {
      id: 2,
      nombre: "especialidad dos"
    }
  ];

  // Data mock prueba
  unidadesTiempo = [
    {
      id: 1,
      nombre: "Semanas"
    },
    {
      id: 2,
      nombre: "Meses"
    },
    {
      id: 3,
      nombre: "Días"
    }
  ];

  constructor(private service: SmartTableData) {
    this.source = new LocalDataSource(this.data);
  }

  ngOnInit() {
  }

  selectEvent(item) {
    this.procedimientoSeleccionado = item.name;         
  }  

  agregarProcedimientos() {
    this.source.add({
      procedimientos: this.procedimientoSeleccionado,
      ambito: this.ambitoSeleccionado.nombre,
      especialidad: this.especialidadSeleccionada.nombre,
      rolEspecialidad: this.obtenerEspecialidad(),
      cantidad: this.cantidadSeleccionada,
      periodo: this.periodoSeleccionado + ' ' + this.unidadTiempoSeleccionado.nombre,
      requerido: this.isReempadronamiento ? "Si": "No",
      costo: "777" 
    });
    this.source.refresh();
    this.clear();    
  }

  clear() {
    this.procedimientoSeleccionado = null;   
    this.ambitoSeleccionado = null;   
    this.especialidadSeleccionada = null;
    this.cantidadSeleccionada = null;
    this.periodoSeleccionado = null;
    this.unidadTiempoSeleccionado = null;
    this.isReempadronamiento = false;
    this.isPreescriptor = false;
    this.isEfector = false;

    this.autocomplete.close();
    this.autocomplete.clear();     
  } 

  obtenerEspecialidad():string{    
    if (this.isPreescriptor && this.isEfector) {
      return "Preescriptor y Efector"
    }else{
      return this.isPreescriptor? "Preescriptor": "Efector";
    }
  } 

}
