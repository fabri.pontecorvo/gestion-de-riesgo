import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  options: any;
  userToken: string;
  constructor(private http: HttpClient, private spinner: NgxSpinnerService) { }

  getToken(): any {
    if (this.options === undefined) {
      this.userToken = localStorage.getItem('token');
      this.spinner.show();

      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.userToken
      });
      this.options = { headers: headers };
      return this.options;
    }
    return this.options;

  }


  getRequest(url: string): Observable<any> {

    // let paramRequest = this.getParamsRequest();
    // this.userToken = paramRequest.token;

    // let options = { headers: paramRequest.headers };
    this.spinner.show();
    return this.http.get(url, this.getToken()).pipe(
      map((resp) => {
        this.spinner.hide();
        return resp;
      }),
      catchError((error) => {
        this.spinner.hide();
        return throwError(new Error('oops!'));
      })
    );
  }

  postRequest(url: string, body: any): Observable<any> {

    this.spinner.show();

    return this.http.post(url, body, this.getToken()).pipe(
      map((resp) => {
        this.spinner.hide();
        return resp;
      }),
      catchError((error) => {
        this.spinner.hide();
        return throwError(new Error('oops!'));
      })
    );
  }

}
