import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../model/usuario.model';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RoutesEnum } from '../common/routes-enum';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userToken: string;

  constructor(private http: HttpClient, 
    private spinner: NgxSpinnerService, 
    private router: Router) {
    this.leerToken();

  }

  logout() {
    localStorage.clear();
    this.userToken = '';
    this.router.navigate(['/' + RoutesEnum.LOGIN]);
  }

  login(usuario: Usuario): Observable<boolean> {
    this.spinner.show();
    const url: string = environment.url + '/authenticate/login';

    return this.http.post(url, usuario).pipe(
      /* map solo se ejecuta si la peticion tiene exito, si quuiero cachear error lo puedo hacer con catchError */
      map(resp => {
       this.spinner.hide();
        this.guardarToken(resp['token']);
        /* El map siempre tiene que retornar algo */
        return true;
      })
    );
  }


  private guardarToken(token: string) {
    this.userToken = token;

    localStorage.setItem('token', token);

    let hoy = new Date();

    hoy.setSeconds(3600);

    localStorage.setItem('expira', hoy.getTime.toString());
  }

  leerToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }

  //TODO- descargar un decouder de token y verificar validez 
  isLogged(): boolean {
    if (this.userToken.length < 2) {
      return false;
    } else {
      return true;
    }
  }
}
