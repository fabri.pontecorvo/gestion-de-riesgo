/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']

})
export class AppComponent implements OnInit {
  title = 'Gestión de Riesgos';
  fechaDerechos = new Date().getFullYear();

  constructor(private route: ActivatedRoute,
    private router: Router, ) {
  }

  ngOnInit(): void {
    this.router.navigate(['/login']);
  }
}
