import { Component, OnInit } from '@angular/core';
import { Usuario } from '../model/usuario.model';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoutesEnum } from '../common/routes-enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertClassEnum } from '../common/alert-class-enum';
import { environment } from '../../environments/environment';
import { ControllerUrlEnum } from '../common/urlEnum';
import { HttpService } from '../services/http.service';
import { Init } from '../@core/data/creacion-programa';

declare var grecaptcha: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  datosInit: Init;
  usuario: Usuario = new Usuario();
  error: string;
  ALERTDANGER = AlertClassEnum.ALERTDANGER;
  loginForm: FormGroup;

  submitted: boolean = false;
  invalidLogin: boolean = false;
  captchaError: boolean = false;
  loginResponse: string;
  captchaWidgetId:any;  
  intentosLoggin: number;
  private urlInit: string = environment.url + ControllerUrlEnum.init;

  constructor(private formBuilder: FormBuilder,private auth: AuthService, private router: Router, private spinner: NgxSpinnerService,private service: HttpService) {

  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
   this.intentosLoggin =  localStorage.getItem('intentosLoggin') != null ? +localStorage.getItem('intentosLoggin'): 0;
   
  }
  get formControlsLogin(): any { return this.loginForm.controls; }


  verifyCallback(response: any){
   };
  
  login(form: NgForm) {
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.intentosLoggin++;
      return;
    }

    if(this.intentosLoggin >= 2){

      this.captchaWidgetId = this.captchaWidgetId == null ? grecaptcha.render( 'myCaptcha', {
        'sitekey' : '6LdVYhgTAAAAAKtvNfmUiiLL0uaxEMjEL9xFfx0D',  // required
        'theme' : 'light',  // optional
      }): this.captchaWidgetId;

      let response = grecaptcha.getResponse(this.captchaWidgetId);
      if (response.length === 0) {
       this.captchaError = true;
     }else{
       this.captchaError = false;
     }
     this.usuario.recaptchaResponse = response;
    }


    if(!this.captchaError){
      this.spinner.show();
      this.usuario.captchaError = this.captchaError;
      this.auth.login(this.usuario).subscribe(
        (resp) => {
          localStorage.setItem('username',this.usuario.username);
          localStorage.setItem("intentosLoggin",'0');
          this.setDatosInit();
          this.router.navigate(['/' + RoutesEnum.PAGES]);
        },
        (err) => {
          this.intentosLoggin++;
          localStorage.setItem("intentosLoggin",this.intentosLoggin.toString());
          this.spinner.hide();
          if (err.error === 'INVALID_CREDENTIALS') {
            this.error = 'Usuario o Contraseña Inválida.';
            if(this.captchaWidgetId != null) grecaptcha.reset(this.captchaWidgetId);
          }else if(err.error.token === 'INVALID_CAPTCHA') {
            this.error = 'Captcha Invalido';
            if(this.captchaWidgetId != null) grecaptcha.reset(this.captchaWidgetId);
          } else {
            this.error = 'Error inesperado';
            if(this.captchaWidgetId != null) grecaptcha.reset(this.captchaWidgetId);
          }
        }
      );
    }else{
      this.error = 'Demuestre que no es un robot.';
    }
  }
  private setDatosInit() {
    
    this.service.getRequest(this.urlInit).subscribe(
      (resp) => {
        this.datosInit = resp;
        localStorage.setItem('init', JSON.stringify(this.datosInit));
        this.spinner.hide();

      },
      (error) => {
      },
      () => {
      });
  }
}
