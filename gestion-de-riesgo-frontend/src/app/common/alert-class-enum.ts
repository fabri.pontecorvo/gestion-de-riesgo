export enum AlertClassEnum {
    ALERTSUCCES = 'alert alert-success alert-dismissible animated flipInX show',
    ALERTDANGER = 'alert alert-danger alert-dismissible animated flipInX show'
}
