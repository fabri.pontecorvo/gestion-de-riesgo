export interface PoblacionObjetoSexoEdadDto{
    sexo:string;
    edadDesde:string;
    edadHasta:string;
    provincia:any;
    localidad:any;
  }
  