import { Observable } from 'rxjs';

export interface Sexo {
    sexo: string;
}

export interface PoblacionEspecial {
    poblacion: string;
}

export interface Localidad {
    localidad: string;
}

export interface Provincia {
    provincia: string;
    localidades: Localidad[];
}

export interface Init {
    sexos: Sexo[];
    poblaciones: PoblacionEspecial[];
    provincias: Provincia[];
    dataGraficoMasculino: any;
    dataGraficoFemenino: any;
}


export abstract class CreacionProgramaData {
    abstract getCreacionProgramaData(): Observable<Init[]>;
}