import { Injectable } from '@angular/core';
import { CreacionProgramaData, Init } from '../data/creacion-programa';

@Injectable({
  providedIn: 'root'
})
export class CreacionProgramaMockService extends CreacionProgramaData {

  private dataInit: Init = 
    {
      sexos: [
        { sexo: 'Masculino' },
        { sexo: 'Femenino' },
        { sexo: 'Ambos' }],
      poblaciones: [
        { poblacion: '#1' },
        { poblacion: '#2' },
        { poblacion: '#3' }],
      provincias: [
        {
          provincia: 'Buenos Aires',
          localidades: [
            { localidad: 'CABA' },
            { localidad: 'Moreno ATR' },
            { localidad: 'En el oeste esta el agite' }]
        },
        {
          provincia: 'La Pampa',
          localidades: [
            { localidad: 'La Pampa' },
            { localidad: 'Santa Rosa' }]
        },
      ],
      dataGraficoMasculino: [this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random()],
      dataGraficoFemenino:[this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1]
    }
  ;

  private random() {
    return Math.round(Math.random() * 100);
  }

  getCreacionProgramaData(): any {
    return this.dataInit;
  }

  getDataGrafico(): any {
    return {
      labels: ['115 a 120', '110 a 115', '105 a 110', '105 a 110', '100 a 105', '95 a 100', '90 a 95', '85 a 90', '80 a 85', '75 a 80',
        '70 a 75', '65 a 70', '60 a 65', '55 a 60', '50 a 55', '45 a 50', '40 a 45', '35 a 40', '30 a 35', '25 a 30', '20 a 25', '15 a 20',
        '10 a 15', '5 a 10', '0 a 5'],
      datasets: [{
        label: 'Mujeres',
        backgroundColor: "#ffa500",
        borderWidth: 0,
        data: [this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1, this.random() * -1],

      }, {
        label: 'Hombres',
        borderWidth: 0,
        backgroundColor: '#00c9deb0',
        data: [this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random(), this.random()],
      },
      ],
    };
  }
}

