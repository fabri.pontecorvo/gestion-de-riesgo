import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RoutesEnum } from './common/routes-enum';
import { AuthGuard } from './guards/auth.guard';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';


const ROUTES: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
     {
       path: RoutesEnum.LOGIN,
       loadChildren: () =>  import('./login/login.module').then(mod => mod.LoginModule),
       canActivate: [AuthGuard]
     },
     {
       path: RoutesEnum.PAGES,
       loadChildren: () =>  import('./pages/pages.module').then(mod => mod.PagesModule),
       canLoad: [AuthGuard],
       canActivate: [AuthGuard]
     }
    ],
    canActivate: [AuthGuard]
  },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [    CommonModule,
    RouterModule.forRoot(ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
