create index idx_beneficiario_genero_fecha_nacimiento on maestro.beneficiario (genero_id, fecha_nacimiento);

CREATE OR replace FUNCTION gestion_riesgo.get_poblacion_objeto_por_edad_area_geografica(p_cod_genero varchar, p_edad_desde integer, p_edad_hasta integer, p_cod_localidad varchar, p_cod_provincia varchar)
RETURNS TABLE(edad integer, cantidad_femenino integer, cantidad_masculino integer)
LANGUAGE sql
AS $function$
	select date_part('year',age(b.fecha_nacimiento))::int edad, 
	       sum(case when b.genero_id = 100 then 1 else 0 end)::int femenino,
	       sum(case when b.genero_id = 200 then 1 else 0 end)::int masculino
	from (
		select b.genero_id, fecha_nacimiento, count(1)
		from maestro.beneficiario b
		     inner join pais_argentina.beneficiario_direccion bd on (b.id = bd.beneficiario_id)
		     inner join pais_argentina.localidad l on (bd.localidad_id = l.id)
		     inner join pais_argentina.provincia p on (l.provincia_id = p.id)
		     inner join parametro.genero g on (b.genero_id = g.id)
	    where l.codigo = CASE WHEN p_cod_localidad != '-1' THEN p_cod_localidad::integer ELSE l.codigo END
	      and p.codigo = CASE WHEN p_cod_provincia != '-1' THEN p_cod_provincia ELSE p.codigo END 
	      and g.codigo = case when p_cod_genero != '-1' then p_cod_genero else g.codigo end
		group by genero_id, fecha_nacimiento
	) b
	where date_part('year',age(b.fecha_nacimiento)) between coalesce(p_edad_desde, 0) and coalesce(p_edad_hasta, 999)	
	group by date_part('year',age(b.fecha_nacimiento))
$function$ ;

GRANT EXECUTE ON FUNCTION gestion_riesgo.get_poblacion_objeto_por_edad_area_geografica TO postgres;
GRANT EXECUTE ON FUNCTION gestion_riesgo.get_poblacion_objeto_por_edad_area_geografica TO grupo_escritura;
GRANT EXECUTE ON FUNCTION gestion_riesgo.get_poblacion_objeto_por_edad_area_geografica TO grupo_lectura;

