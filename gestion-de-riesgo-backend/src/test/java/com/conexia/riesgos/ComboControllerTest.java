package com.conexia.riesgos;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;

import com.conexia.riesgos.dto.response.ComboDto;
import com.conexia.riesgos.model.TipoPariente;
import com.conexia.riesgos.repository.TipoParienteRepository;
import com.conexia.riesgos.service.ComboService;
import com.conexia.riesgos.service.ComboServiceImpl;

@SpringBootTest	
public class ComboControllerTest {
	
	@Mock
    private TipoParienteRepository tipoParienteRepository;

	@InjectMocks
	private ComboService comboService = new ComboServiceImpl();
	
  @BeforeEach
    void setMockOutput() {

		TipoPariente tipoPariente1 = new TipoPariente();
		tipoPariente1.setId(11L);
		tipoPariente1.setCodigo("002");
		tipoPariente1.setDescripcion("HIJO/A");
		tipoPariente1.setAbreviatura("ALGO");
		
			List<TipoPariente> lista = new ArrayList<TipoPariente>();
		lista.add(tipoPariente1);
	  
	  
	  when(tipoParienteRepository.findAll()).thenReturn(lista);
    }
  
	@Test
	public void retrieveQuestions() throws Exception {
		List<ComboDto> parientes =  comboService.getTiposParientes();
	
		JSONAssert.assertEquals("002", parientes.get(0).getCodigo(), true);
	}
}