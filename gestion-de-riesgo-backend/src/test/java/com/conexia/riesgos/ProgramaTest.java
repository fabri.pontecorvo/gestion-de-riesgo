package com.conexia.riesgos;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.conexia.riesgos.model.EmpadronamientoCondicion;
import com.conexia.riesgos.model.EstadoPrograma;
import com.conexia.riesgos.model.Programa;
import com.conexia.riesgos.model.TipoEmpadronamiento;
import com.conexia.riesgos.model.UnidadTiempo;
import com.conexia.riesgos.repository.ProgramaRepository;

@SpringBootTest
public class ProgramaTest {
	
	@Autowired
	private ProgramaRepository programaRepository;
	
	@DisplayName("Test Spring @Autowired Integration 1")
	@Test
	public void insertPrograma() {
		Programa newPrograma = createPrograma("INSERTPROGRAMA");
		
		programaRepository.save(newPrograma);
	
		assertNotNull(newPrograma.getId());
		
		programaRepository.delete(newPrograma);
	}
	
	@DisplayName("Test Spring @Autowired Integration 2")
	@Test
	public void findPrograma() {
		
		Programa newPrograma = createPrograma("PARABUSCAR");
		
		programaRepository.save(newPrograma);
		
		Programa programa = programaRepository.findByCodigo("PARABUSCAR");
		
		assertEquals("PARABUSCAR", programa.getCodigo());	
		
		programaRepository.delete(newPrograma);
		
	}
	
	
	@DisplayName("Test Spring @Autowired Integration 3")
	@Test
	public void deletePrograma() {
		Programa newPrograma = createPrograma("PARABORRAR");
		
		programaRepository.save(newPrograma);
		
		Programa programa = programaRepository.findByCodigo("PARABORRAR");
		
		programaRepository.delete(programa);
		
		programa = programaRepository.findByCodigo("PARABORRAR");
		
		assertNull(programa);	
		
	}
	
	private Programa createPrograma(String codigo) {
		
		Programa programa = new Programa();
		
		programa.setCodigo(codigo);
		programa.setDescripcion("PROGRAMA TEST");
		programa.setFechaDesde(LocalDate.now());
		programa.setFechaHasta(LocalDate.now());
		programa.setEdadDesde(null);
		programa.setEdadHasta(null);
		programa.setEmpadronamientoCondicion(new EmpadronamientoCondicion(7L));
		programa.setEstadoPrograma(new EstadoPrograma(1L));
		programa.setPrograma(null);
		programa.setTipoEmpadronamiento(new TipoEmpadronamiento(4L));
		programa.setUnidadTiempoEdad(new UnidadTiempo(2L));
		programa.setVigenciaCantidad(20);
		programa.setVigenciaPeriodo(10);
		programa.setVigenciaPeriodoUnidadTiempo(new UnidadTiempo(2L));
		
		programa.setDeleted(Boolean.FALSE);
		
		return programa;
		
		
	}
}
