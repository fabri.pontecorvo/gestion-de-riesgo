<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
  <meta name="generator" content= "conexia"/>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Auto-Respuesta Aplicativo Comei</title>
</head>

<body>
  <table style="width:60%;"  border="0" align="center"
  cellpadding="0" cellspacing="0">
    <tbody>
  

	<tr>
	  	<#include "/HeaderBase.ftl">
	
		   <td style=
		        "border-left-color: #e9e9e9; border-left-width:1px; border-left-style: solid;border-right-color: #e9e9e9; border-right-width:1px; border-right-style: solid; padding:0% 5% 0px 5%;"
		        height="10" colspan="7" align="center" bgcolor="#fff">
		          <p style="margin:0px;font-family:verdana;font-size:12px;line-height:26px;text-align:left;color:#707070; ">
			          
			          	 <br/> Le informamos que hemos procedido al alta del siguiente usuario.<br/>
			          	 <span style="font-weight:700;"> ${labelrs} </span>${razonSocialNombreApellido}<br>  
						 <span style="font-weight:700;"> ${labelnp} </span>${nprestadorMatricula}<br>  
						 <span style="font-weight:700;"> Cuit: </span>${cuit}<br>  
						 <span style="font-weight:700;"> Domicilio: </span>${domicilio}<br>  
						 <span style="font-weight:700;"> Email de contacto: </span>  ${email} <br>
		        		 <span style="font-weight:700;"> Tel&eacute;fono de contacto: </span>  ${telefono} <br>	
		        		 		        		 <br>       		 	        		 
		        		 <span style="font-weight:700;"> Usuario: </span>${usuario}<br>  
		        		 <span style="font-weight:700;"> Contraseña: </span>  ${contraseña} <br> 
		        		 <span style="font-weight:700;"> Rol: </span>  ${rol} <br><br><br>
		        		 <b>Se solicita efectuar la capacitaci&oacute;n del modulo pertinente.</b>
			          
		         </p>
		    </td>
		 </tr>
		
     <#include "/FooterBase.ftl">
            
    </tbody>
  </table>
</body>
</html>