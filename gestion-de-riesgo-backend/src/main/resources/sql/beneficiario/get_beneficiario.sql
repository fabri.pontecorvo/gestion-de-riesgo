CREATE OR REPLACE FUNCTION transaccional.get_beneficiario(_nro_beneficiario character varying, _apellidonombre character varying, _abreviatura character varying, _numero_documento character varying)
 RETURNS TABLE(id bigint, apellido_nombre character varying, sexo character, plan_descripcion character varying, fecha_nacimiento date, tipo_documento character, numero_documento character, email character varying, telefono character varying, celular character varying, cliente_id character varying, fecha_habilitacion_desde_ambulatorio date, fecha_habilitacion_desde_mediana_complejidad date, fecha_habilitacion_desde_alta_complejidad date, fecha_habilitacion_desde_baja_complejidad date, telefono_notificacion character varying, mail_notificacion character varying, notificar_por_mail boolean, notificar_por_telefono boolean, sml double precision)
 LANGUAGE plpgsql
AS $function$ 
begin
	
	if  (_nro_beneficiario is not null	
		or _apellidonombre is not null
		or (_numero_documento is not null and _abreviatura is not null) and (select exists(select tdoc.id from transaccional.tipo_documento tdoc where upper(tdoc.abreviatura) = upper(_abreviatura)))
		)
		
		then		
			
		return query
		
		select bene.id, trim(bene.apellido_nombre)::varchar, bene.sexo, trim(plan.descripcion)::varchar, bene.fecha_nacimiento, tdoc.abreviatura, bene.numero_documento, bene.email, bene.telefono, bene.celular, bene.cliente_id, bene.fecha_habilitacion_desde_ambulatorio, bene.fecha_habilitacion_desde_mediana_complejidad, bene.fecha_habilitacion_desde_alta_complejidad, bene.fecha_habilitacion_desde_baja_complejidad, bene.telefono_notificacion, bene.mail_notificacion, bene.notificar_por_mail, bene.notificar_por_telefono, similarity(bene.apellido_nombre, _apellidonombre)::float as sml 
		from transaccional.beneficiario bene  
		inner join transaccional.tipo_documento tdoc on tdoc.id = bene.tipo_documento_id
		inner join transaccional.tipo_situacion tsitua on tsitua.id = bene.tipo_situacion_id
		left join transaccional.plan plan on plan.id = bene.plan_id
		where bene.cliente_id = coalesce(nullif(_nro_beneficiario, ''), bene.cliente_id) and similarity(trim(bene.apellido_nombre)::varchar, coalesce(nullif(_apellidonombre, ''), trim(bene.apellido_nombre)::varchar)) > 0.3  and bene.numero_documento = coalesce(nullif(_numero_documento, ''), bene.numero_documento) and tdoc.abreviatura = coalesce(nullif(upper(_abreviatura), ''), tdoc.abreviatura)
		order by sml desc, bene.apellido_nombre;

	else
	
		raise exception 'Debe ingresar por lo menos un parámetro distinto de null';

	end if;

end
 $function$
;
