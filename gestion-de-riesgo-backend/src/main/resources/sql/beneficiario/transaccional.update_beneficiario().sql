CREATE OR REPLACE FUNCTION transaccional.update_beneficiario(_usuario_original character varying, _usuario_editar character varying, _password character varying, _email character varying, _telefono character varying, _habilitar boolean)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
AS $function$ 
declare
	_detalle character varying;
	_status character varying;

begin
		select storedmu.detalle, storedmu.status into _detalle, _status from seguridad.modificar_usuario(_usuario_original, _usuario_editar, _password , _email , _telefono , _habilitar) storedmu;				
	
		if (_status = 'SUCCESS' and _detalle = 'Los datos fueron modificados exitosamente.') then			
			
			update transaccional.beneficiario set email = _email, fecha_update = now() where id = (select id_beneficiario from seguridad.sist_seguridad_usuario where usuario = _usuario_editar);
					
			return query select 'Los datos fueron modificados exitosamente.'::varchar, 'SUCCESS'::varchar;	
			
		else		
			return query select 'No existe, o fue borrado el beneficiario o el usuario'::varchar, 'ERROR'::varchar;	
		
		end if;			
	
end;

 $function$
;
