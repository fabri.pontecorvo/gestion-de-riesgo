CREATE OR REPLACE FUNCTION seguridad.set_usuario_beneficiario(_nro_beneficiario character varying, _numero_documento character, _abreviatura character, _usuario character varying)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
AS $function$ 
declare
	_id_tipo_doc int8;
	_id int8;
begin
	if (_usuario is not null and trim(_usuario) <> '') then
		if (_nro_beneficiario is not null and trim(_nro_beneficiario) <> '') then
		
			_id := (select id from transaccional.beneficiario where cliente_id = _nro_beneficiario);
		
			if((select exists(select * from transaccional.beneficiario where id = _id  and (fecha_delete is null or fecha_delete > now())))
			and (select exists(select * from seguridad.sist_seguridad_usuario where id_beneficiario = _id and (fecha_delete is null or fecha_delete > now()))))then
				update seguridad.sist_seguridad_usuario set usuario = _usuario
					where id_beneficiario = _id;
				return query select 'OK!'::varchar, 'SUCCESS'::varchar;
			else
				return query select 'No existe, o fue borrado el beneficiario o el usuario'::varchar, 'ERROR'::varchar;
			end if;
		else
			if (_numero_documento is not null and trim(_numero_documento) <> '') then
			
				_id_tipo_doc:= (select id from transaccional.tipo_documento where abreviatura = upper(_abreviatura));
				_id := (select id from transaccional.beneficiario where _numero_documento = numero_documento and tipo_documento_id = _id_tipo_doc);
				if((select exists(select * from transaccional.beneficiario where id = _id  and (fecha_delete is null or fecha_delete > now())))
				and (select exists(select * from seguridad.sist_seguridad_usuario where id_beneficiario = _id and (fecha_delete is null or fecha_delete > now()))))then
					update seguridad.sist_seguridad_usuario set usuario = _usuario
						where id_beneficiario = _id;
					return query select 'OK!'::varchar, 'SUCCESS'::varchar;
				else
					return query select 'No existe, o fue borrado el beneficiario o el usuario'::varchar, 'ERROR'::varchar;
				end if;
			else
				return query select 'Debe ingresar como parametro el nro beneficiario o el tipo y numero de documento'::varchar, 'ERROR'::varchar;
			end if;
		end if;
	else
		return query select 'Usuario no puede ser null'::varchar, 'ERROR'::varchar;
	end if;
end
 $function$
;
