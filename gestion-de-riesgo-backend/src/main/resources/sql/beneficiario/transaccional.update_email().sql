CREATE OR REPLACE FUNCTION transaccional.update_email(_nro_beneficiario character varying, _email character varying)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
AS $function$ 
declare

begin		
		if (_email is not null) then	
			if(_nro_beneficiario is null) then						
					
				if(
				(select exists(select * from transaccional.beneficiario where cliente_id = _nro_beneficiario)) and 
				(select exists(select * from seguridad.sist_seguridad_usuario where id_beneficiario = (select id from transaccional.beneficiario 
				where cliente_id = _nro_beneficiario) and email = _email))
				)then					
				
					update transaccional.beneficiario set email = _email where cliente_id = _nro_beneficiario;
					
					return query select 'OK'::varchar, 'SUCCESS'::varchar;	
				
				else
						return query select 'No existe, o fue borrado el beneficiario o el usuario'::varchar, 'ERROR'::varchar;
				end if;

			end if;
		else
			return query select 'El email no puede ser null'::varchar, 'ERROR'::varchar;
	  end if;
	
end;


 $function$
;
