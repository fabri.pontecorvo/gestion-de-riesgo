CREATE OR REPLACE FUNCTION intercambio.update_practica(_edad_desde smallint, _edad_hasta smallint, _cantidad_minima smallint, _cantidad_maxima smallint, _ambito character varying, _version_transferencia integer)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
	declare
	begin
		update intercambio.i_practica_nomenclada  set edad_desde = _edad_desde, edad_hasta = _edad_hasta, cantidad_minima = _cantidad_minima, cantidad_maxima = _cantidad_maxima, ambito = _ambito
		where version_transferencia = _version_transferencia;
		
		return query select * from intercambio.act_comei_practica(_version_transferencia);
		
	end
 $function$
;
