CREATE OR REPLACE FUNCTION intercambio.act_comei_practica(_version integer)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
AS $function$
    declare
    fecha_actual timestamp;
    proceso varchar(100);
    tabla varchar(100);
    
    begin
        fecha_actual := now();
        proceso := 'act_practica_nomenclada';
        tabla := 'i_practica_nomenclada';
        
            IF (NOT EXISTS(SELECT 1 FROM intercambio.i_practica_nomenclada WHERE version_transferencia = _version AND estado_procesado = 1))
        THEN
		RAISE NOTICE 'NO HAY REGISTROS PARA PROCESAR EN intercambio.% PARA LA VERSION %.', tabla, _version;
	return query select 'No hay registros para procesar en intercambio para la versión solicitada.'::varchar,'ERROR'::varchar;
		RETURN;
        END IF;

                                                                       
	INSERT INTO intercambio.log_actualizador_import(fecha, proceso, tabla, tabla_id, novedad, clave, fecha_informado, tipo_error, detalle) 
		( 	select 	now(), proceso, tabla, ipra.id as i_practica_nomenclada_id, ipra.tipo_novedad, null, ipra.fecha_generacion, 'error', 'el campo codigo no puede ser nulo'
			from 	intercambio.i_practica_nomenclada ipra
			where 	ipra.estado_procesado = 1 
				and ipra.codigo is null and version_transferencia =_version
		);

        UPDATE intercambio.i_practica_nomenclada SET estado_procesado = 3, fecha_procesado = now() 
        WHERE estado_procesado = 1 and codigo is  null and version_transferencia =_version;

        
                
        INSERT INTO intercambio.log_actualizador_import(fecha, proceso, tabla, tabla_id, novedad, clave, fecha_informado, tipo_error, detalle) 
        ( select now(), proceso, tabla, ipra.id as i_practica_nomenclada_id, ipra.tipo_novedad, null, ipra.fecha_generacion, 'error', 'el campo tipo_nomenclador no puede ser nulo'
                from intercambio.i_practica_nomenclada ipra
                where ipra.estado_procesado = 1 and ipra.tipo_nomenclador is null and version_transferencia =_version
              );

        UPDATE intercambio.i_practica_nomenclada SET estado_procesado = 3, fecha_procesado = now() 
        WHERE estado_procesado = 1 and tipo_nomenclador is null and version_transferencia =_version;
         
                INSERT INTO intercambio.log_actualizador_import(fecha, proceso, tabla, tabla_id, novedad, clave, fecha_informado, tipo_error, detalle) 
        ( select now(), proceso, tabla, ipra.id as i_practica_nomenclada_id, ipra.tipo_novedad, (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))::bigint, ipra.fecha_generacion, 'error', 'el campo descripcion no puede ser nulo'
                from intercambio.i_practica_nomenclada ipra
                where ipra.estado_procesado = 1 and ipra.descripcion is null  and version_transferencia =_version
              );

        UPDATE intercambio.i_practica_nomenclada SET estado_procesado = 3, fecha_procesado = now() 
        WHERE estado_procesado = 1 and descripcion is null and version_transferencia =_version; 

           
        
	UPDATE transaccional.nomenclador_practica SET fecha_delete = ipra.fecha_delete
        FROM intercambio.i_practica_nomenclada ipra
        WHERE ipra.estado_procesado = 1 and ipra.tipo_novedad = 'B' and cliente_id = (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')) and ipra.version_transferencia =_version;

                UPDATE intercambio.i_practica_nomenclada ipra SET fecha_procesado = now(), estado_procesado = 2  
        WHERE estado_procesado = 1  
              and tipo_novedad = 'B'
              and (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')) in (select cliente_id from transaccional.nomenclador_practica) and version_transferencia =_version;

                UPDATE intercambio.i_practica_nomenclada ipra SET fecha_procesado = now(), estado_procesado = 9  
        WHERE estado_procesado = 1  
              and tipo_novedad = 'B'
              and (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')) not in (select cliente_id from transaccional.nomenclador_practica) and version_transferencia =_version;

        
        INSERT INTO intercambio.log_actualizador_import(fecha, proceso, tabla, tabla_id, novedad, clave, fecha_informado, tipo_error, detalle) 
        ( select now(), proceso, tabla, ipra.id as i_practica_nomenclada_id, ipra.tipo_novedad,(lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))::bigint, ipra.fecha_generacion, 'warning', 'baja de nomenclador_practica inexistente'
                from intercambio.i_practica_nomenclada ipra
                where ipra.estado_procesado = 9  and version_transferencia =_version
              );
         
        UPDATE intercambio.i_practica_nomenclada SET estado_procesado = 6  
        WHERE estado_procesado = 9   and version_transferencia =_version;
               
	UPDATE transaccional.nomenclador_practica 
        SET  descripcion            = ipra.descripcion,
             honorario_medico       = ipra.honorario_medico,
             honorario_ayudante     = ipra.honorario_ayudante,
             honorario_anestesista  = ipra.honorario_anestesista,
             gastos                 = ipra.gastos,
             cantidad_ayudantes     = ipra.cant_ayudantes,
             tipo_gastos            = ipra.tipo_de_gastos,
             nivel                  = ipra.nivel,
             orden                  = ipra.orden,
             autorizacion           = ipra.autorizacion,
             practica_unica         = ipra.practica_unica,
             edad_desde             = ipra.edad_desde,
             edad_hasta             = ipra.edad_hasta,
             sexo                   = (case when ipra.sexo = '0' then 'F'
                                           when ipra.sexo = '1' then 'M'
                                           else ipra.sexo
                                      end),
             nomenclada             = ipra.cod_nomenclador,
             tipo_datos             = ipra.tipo_datos,
             fecha_update           = now(),
             version                = version + 1,
             fecha_delete           = ipra.fecha_delete,
             ambito                 = (select itop.ambito from intercambio.i_tope itop where itop.codigo_practica = ipra.codigo and itop.tipo_nomenclador= ipra.tipo_nomenclador),
             cantidad_maxima        = ipra.cantidad_maxima,
             cantidad_minima        = ipra.cantidad_minima,
             complejidad            = ipra.complejidad
             
        FROM intercambio.i_practica_nomenclada ipra
        WHERE
                    ipra.estado_procesado = 1 and ipra.tipo_novedad in ('A', 'M') and cliente_id = (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')) and ipra.version_transferencia =_version;

	UPDATE intercambio.i_practica_nomenclada ipra SET estado_procesado = 2, fecha_procesado = now()  
	FROM transaccional.nomenclador_practica trx
	WHERE ipra.estado_procesado = 1 and ipra.tipo_novedad = 'M' and (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')) = trx.cliente_id and ipra.version_transferencia =_version;


	UPDATE intercambio.i_practica_nomenclada ipra SET estado_procesado = 7, fecha_procesado = now()  
	FROM transaccional.nomenclador_practica trx
	WHERE ipra.estado_procesado = 1 and ipra.tipo_novedad = 'A' and (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')) = trx.cliente_id and ipra.version_transferencia =_version;

	INSERT INTO intercambio.log_actualizador_import(fecha, proceso, tabla, tabla_id, novedad, clave, fecha_informado, tipo_error, detalle) 
		( select now(), proceso, tabla, ipra.id as i_practica_nomenclada_id, ipra.tipo_novedad,(lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))::bigint,ipra.fecha_generacion, 'warning', 'alta tomada como modificacion'
               from intercambio.i_practica_nomenclada ipra
               where ipra.estado_procesado = 7 and version_transferencia =_version
		);

	UPDATE intercambio.i_practica_nomenclada SET estado_procesado = 6  
	WHERE estado_procesado = 7 and version_transferencia =_version;
              
              
	UPDATE intercambio.i_practica_nomenclada ipra SET estado_procesado = 4, fecha_procesado = now()  
	WHERE estado_procesado = 1 and tipo_novedad = 'A' and not exists (select 1 from transaccional.nomenclador_practica i where i.cliente_id = (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))) and version_transferencia =_version;  

	UPDATE intercambio.i_practica_nomenclada ipra SET estado_procesado = 5, fecha_procesado = now() 
	WHERE estado_procesado = 1 and tipo_novedad = 'M' and not exists (select 1 from transaccional.nomenclador_practica i where i.cliente_id =(lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))) and version_transferencia =_version;  

       
	INSERT INTO transaccional.nomenclador_practica(codigo, descripcion, sexo, edad_desde, edad_hasta, fecha_delete, tipo_nomenclador, honorario_medico, honorario_ayudante,
                                                        honorario_anestesista, gastos, cantidad_ayudantes, tipo_gastos, nivel, orden, autorizacion, practica_unica, tipo_datos,
                                                        cliente_id, nomenclada, cantidad_minima, cantidad_maxima, ambito, complejidad) 
              ( select ipra.codigo,  ipra.descripcion, (case when ipra.sexo = '0' then 'F' when ipra.sexo = '1' then 'M' else ipra.sexo end), ipra.edad_desde, ipra.edad_hasta, ipra.fecha_delete, ipra.tipo_nomenclador, ipra.honorario_medico,
                       ipra.honorario_ayudante, ipra.honorario_anestesista, ipra.gastos, ipra.cant_ayudantes, ipra.tipo_de_gastos, ipra.nivel, ipra.orden, ipra.autorizacion,
                       ipra.practica_unica, ipra.tipo_datos, (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0')), ipra.cod_nomenclador,
                       ipra.cantidad_minima, ipra.cantidad_maxima, 
                       (case when (select itop.ambito from intercambio.i_tope itop where itop.codigo_practica = ipra.codigo and itop.tipo_nomenclador= ipra.tipo_nomenclador) is not null then ipra.ambito else 'A' end), ipra.complejidad
               
                from intercambio.i_practica_nomenclada ipra
                where ipra.estado_procesado in (4,5) and ipra.tipo_novedad in ('A', 'M')
                       and not exists (select 1 from transaccional.nomenclador_practica where cliente_id = (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))) and version_transferencia =_version
                
        );

       
       INSERT INTO intercambio.log_actualizador_import(fecha, proceso, tabla, tabla_id, novedad, clave, fecha_informado, tipo_error, detalle) 
	( select now(), proceso, tabla, ipra.id as i_beneficiario_id, ipra.tipo_novedad, (lpad(ipra.tipo_nomenclador::varchar, 2, '0') || lpad(ipra.codigo::varchar,6, '0'))::bigint, ipra.fecha_generacion, 'warning', 'modificacion tomada como alta'
               from intercambio.i_practica_nomenclada ipra
               where ipra.estado_procesado = 5  and version_transferencia =_version
        );

	UPDATE intercambio.i_practica_nomenclada 
	SET estado_procesado = 6  
	WHERE estado_procesado = 5 and version_transferencia =_version;

	UPDATE intercambio.i_practica_nomenclada SET estado_procesado = 2  
	WHERE estado_procesado = 4 and version_transferencia =_version;

    return query select 'Proceso de intercambio de práctica nomenclada terminado.'::varchar,'SUCCESS'::varchar;


    end;
$function$
;
