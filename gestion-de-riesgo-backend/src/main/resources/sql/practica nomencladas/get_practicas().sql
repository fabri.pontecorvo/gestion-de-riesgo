CREATE OR REPLACE FUNCTION transaccional.get_practicas(_codigo character varying, _descripcion character varying)
 RETURNS TABLE(id bigint, descripcion text, codigo character varying, edad_desde integer, edad_hasta integer, cantidad_minima integer, cantidad_maxima integer, ambito character varying, esquema text, version bigint)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
	declare
	begin
		return query select np.id,trim(np.descripcion),np.codigo::character varying,np.edad_desde,
		np.edad_hasta,np.cantidad_minima,np.cantidad_maxima,np.ambito,'transaccional',0::bigint 
					 from transaccional.nomenclador_practica np where titulo is false and np.codigo::character varying ilike '%'||_codigo||'%' and np.descripcion ilike '%'||_descripcion||'%' 
				union 
				select inp.id,trim(inp.descripcion),inp.codigo::character varying,inp.edad_desde,inp.edad_hasta,
				inp.cantidad_minima,inp.cantidad_maxima,inp.ambito,'intercambio', inp.version_transferencia
					 from intercambio.i_practica_nomenclada inp where inp.codigo::character varying ilike '%'||_codigo||'%' and replace(inp.descripcion,' ','') ilike '%'||_descripcion||'%' 
					and inp.version_transferencia = (select max(inp2.version_transferencia) from intercambio.i_practica_nomenclada inp2  where inp2.codigo = inp.codigo and inp.estado_procesado = 1 and inp.tipo_novedad = 'A') ; 
	end
 $function$
;
