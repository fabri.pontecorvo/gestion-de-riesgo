CREATE OR REPLACE FUNCTION intercambio.impactar_faba_yyyymm(_prestador_id bigint, _fecha_transaccion date, _fecha_insert date, _yyyymm character)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
AS $function$ 
begin	
    --Borro posibles intentos anteriores no terminados del mismo mensual y prestador:
    DELETE 
    FROM intercambio.faba_yyyymm 
    WHERE mensual = _yyyymm AND 
        prestador_id = _prestador_id AND 
        NOT procesado AND
        fecha_procesado IS NULL;
     
    INSERT INTO intercambio.faba_yyyymm(prestador_id, faba_numero_transaccion, comei_numero_transaccion, fecha_transaccion, fecha_insert, mensual)
    SELECT _prestador_id, 
        faba_numero_transaccion::varchar(20), 
        comei_numero_transaccion::numeric(12,0),
        _fecha_transaccion, 
        _fecha_insert, 
        _yyyymm
    FROM intercambio.txt_faba_yyyymm
    WHERE	--(faba_numero_transaccion ~ '^[0-9]') and
		(comei_numero_transaccion ~ '^[0-9]');
        
    --Impacto en transaccional:
    INSERT INTO transaccional.rri_prestador (prestador_id, rri_conexia, rri_prestador, fecha_transaccion, fecha_insert) 
    SELECT prestador_id, comei_numero_transaccion, faba_numero_transaccion, fecha_transaccion, fecha_insert
    FROM	intercambio.faba_yyyymm
    WHERE mensual = _yyyymm AND 
        prestador_id = _prestador_id AND 
        NOT procesado AND
        fecha_procesado IS NULL;

    UPDATE intercambio.faba_yyyymm
    SET procesado = true,
        fecha_procesado = clock_timestamp()
    WHERE mensual = _yyyymm AND 
        prestador_id = _prestador_id AND 
        NOT procesado AND
        fecha_procesado IS NULL;
    
    RAISE NOTICE 'SE IMPORTO EL ARCHIVO "/home/backups/intercambio/COMEI_MM-YYYY.csv"';
    return query select 'Proceso de intercambio de FABA finalizó exitosamente.'::varchar,'SUCCESS'::varchar;
        
exception
    when others then
        RAISE NOTICE 'NO SE PUDO IMPORTAR EL ARCHIVO "/home/backups/intercambio/COMEI_MM-YYYY.csv"';
   		 return query select 'Ups! Al parecer hubo un error en el proceso.'::varchar,'ERROR'::varchar;
end;
 $function$
;
