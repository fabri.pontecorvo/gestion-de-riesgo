CREATE OR REPLACE FUNCTION intercambio.get_i_beneficiario(_nro_afiliado character varying, _numero_documento integer, _fecha_desde date, _fecha_hasta date)
 RETURNS TABLE(apellido_nombre character varying, nro_adherente_completo character, documento character varying, sexo character varying, fecha_nacimiento date, email character varying, situacion character varying, plan_codigo character varying, fecha_beneficio timestamp without time zone, fecha_habilitacion_desde_ambulatorio timestamp without time zone, fecha_habilitacion_desde_baja_complejidad timestamp without time zone, fecha_habilitacion_desde_mediana_complejidad timestamp without time zone, fecha_habilitacion_desde_alta_complejidad timestamp without time zone, fecha_baja timestamp without time zone, estado_procesado smallint, fecha_procesado timestamp without time zone, version_transferencia integer, tipo_novedad character, fecha_generacion timestamp without time zone)
 LANGUAGE sql
 SECURITY DEFINER
AS $function$

select
	apellido_nombre,
	nro_adherente_completo ,
	documento_tipo || ' ' || documento_numero "documento",
	sexo,
	fecha_nacimiento,
	email,
	situacion,
	plan_codigo,
	fecha_beneficio,
	fecha_habilitacion_desde_ambulatorio,
	fecha_habilitacion_desde_baja_complejidad,
	fecha_habilitacion_desde_mediana_complejidad,
	fecha_habilitacion_desde_alta_complejidad,
	fecha_baja, 
	estado_procesado,
	fecha_procesado,
	version_transferencia, 
	tipo_novedad,
	fecha_generacion
from
	intercambio.i_beneficiario
where
trim(nro_adherente_completo) = coalesce(_nro_afiliado, nro_adherente_completo)
and documento_numero = coalesce(_numero_documento,documento_numero)
and fecha_generacion::date between coalesce(_fecha_desde, now()::date) and coalesce(_fecha_hasta, now()::date)
$function$
;
