CREATE OR REPLACE FUNCTION transaccional.get_transacciones_beneficiario(_rri numeric, _cliente_id character varying, _fecha_desde date, _fecha_hasta date)
 RETURNS TABLE(rri numeric, fecha_atencion date, prescripcio_anulada timestamp without time zone, atencion_anulada timestamp without time zone, es_practica boolean, beneficiario_nombre character varying, numero_documento character, nro_beneficiario character varying, codigo_practica character, descripcion character varying, cantidad_solicitada smallint, cantidad_aprobada smallint, saldo smallint, estado_auditoria character varying, auditor text, origen character varying, razon_social_prestador character varying, delegacion_descripcion character varying)
 LANGUAGE sql
 SECURITY DEFINER
AS $function$

		select
		ate.rri,
		ate.fecha_atencion::date,
		pree.fecha_delete "prescripcion_anulada",
		ate.fecha_delete "atencion_anulada",
		pree.es_practica "es_practica",
		ben.apellido_nombre "beneficiario_nombre",
		ben.numero_documento "numero_documento",
		ben.cliente_id "nro_beneficiario",
		np.codigo "codigo_practica",
		np.descripcion,
		pree.cantidad_solicitada,
		pree.cantidad_aprobada,
		pree.saldo,
		ea.descripcion "estado_auditoria",
		a.nombre || ' ' || a.apellido "auditor",
		pree.origen "origen",
		pre_pre.razon_social "razon_social_prestador",
		dele.descripcion "delegacion_descripcion"
	from
		transaccional.atencion ate
	inner join transaccional.prescripcion pree on
		pree.atencion_id = ate.id
	left join transaccional.practica prac on
		prac.prescripcion_id = pree.id
	inner join transaccional.beneficiario ben on
		ben.id = ate.beneficiario_id
	left join transaccional.prestador pre_pre on
		pre_pre.id = ate.prestador_id
	inner join transaccional.nomenclador_practica np on
		np.id = pree.nomenclador_practica_id
	left join transaccional.auditoria au on
		au.prescripcion_id = pree.id
	left join transaccional.estado_auditoria ea on
		ea.id = pree.estado_auditoria_id
	left join transaccional.auditor a on
		a.id = au.motivo_auditoria_id
	left join transaccional.delegacion dele on 
		dele.id = ate.delegacion_id 
	where (case when _fecha_desde is not null and _fecha_hasta is not null and _fecha_desde <= _fecha_hasta then ate.fecha_atencion between _fecha_desde and _fecha_hasta else 1=1 end) 
		and 
			 ate.rri = coalesce(_rri,ate.rri) and ben.cliente_id = coalesce(_cliente_id,ben.cliente_id) 
		order by rri desc;

 $function$
;
