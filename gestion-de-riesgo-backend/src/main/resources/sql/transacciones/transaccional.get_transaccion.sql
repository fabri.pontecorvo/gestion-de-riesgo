CREATE OR REPLACE FUNCTION transaccional.get_transaccion(_rri numeric)
 RETURNS TABLE(rri numeric, codigo character, descripcion_practica character varying, id_at bigint, id_pre bigint, observaciones character varying, fecha_atencion timestamp without time zone, fecha_delete_atencion timestamp without time zone, anulado_ate boolean, fecha_delete_pres timestamp without time zone, anulado_pres boolean, cantidad_solicitada smallint, cantidad_aprobada smallint, saldo smallint, estado text, prescriptor character varying, apellido_nombre character varying, nro_adherente_completo character varying)
 LANGUAGE plpgsql
AS $function$ 
begin
	/*
	 * autor:ngoicoechea
	 */
	if (_rri is not null) then
 	return query 	
		SELECT ate.rri "RRI", nom.codigo "Codigo", nom.descripcion "Descripcion Practica",
			ate.id "id_at",
			pre.id "id_pre",
			ate.observaciones, 
			ate.fecha_atencion "Fecha Atencion",
			ate.fecha_delete "FechaDeleteAte",
			ate.es_anulado "AnuladoAte",
			pre.fecha_delete "FechaDeletePres",
			pre.es_anulado "AnuladoPres",
			pre.cantidad_solicitada "Cantidad Solicitada",
			pre.cantidad_aprobada "Cantidad Aprobada",
			pre.saldo "Saldo",
			case pre.estado_auditoria_id 
			when 3 then 'LA TOMO PARA AUDITAR'
			when 4 then 'RECHAZADO'
			when 5 then 'APROBADO'
			when 11 then 'DERIVADO'
			when 2 then 'AUDITOR PIDE AMPLIACION'
			when 8 then 'RECHAZADO'
			when 9 then 'STAND BY'
			when 1 then 'APROBADO AUTOMATICO'
			when 10 then 'ANULADO'
			when 12 then 'APROBADO AUTOMATICAMENTE'
			when 7 then 'AMPLIADO'
			when 6 then 'PARA AUDITAR'
			ELSE 'Sin Estado' 
			end as "Estado",
			coalesce(prescriptor.razon_social, del.descripcion) as "Prescriptor",
			ben.apellido_nombre,
			ben.nro_adherente_completo
			FROM transaccional.atencion ate
			  join transaccional.prescripcion pre on pre.atencion_id = ate.id
			  join transaccional.nomenclador_practica nom on pre.nomenclador_practica_id = nom.id
			  join transaccional.beneficiario ben on ate.beneficiario_id=ben.id
			  left join transaccional.delegacion del on del.id = ate.delegacion_id
			  left join transaccional.prestador prescriptor on prescriptor.id=ate.prestador_id
			where ate.rri = _rri
			order by ate.rri desc, cast(nom.codigo as numeric);
	end if;
end
 $function$
;
