CREATE OR REPLACE FUNCTION transaccional.anular_autorizacion(_rrn numeric, _motivo_anulacion character varying)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
        declare
		_rri_anulacion bigint;
        begin
	        
	    if(_rrn is not null and (select exists(select id from transaccional.atencion where rri = _rrn))) then
			_rri_anulacion = (select mensajeria.concentrador_get_next_id_transaccion_numero());
		 
			UPDATE 	transaccional.atencion 
			SET 	fecha_delete = now(), 
				fecha_update = now(), 
				es_anulado = TRUE
			WHERE 	rri = _rrn;
	
			UPDATE 	transaccional.prescripcion 
			SET  	fecha_delete = now(),
				fecha_update = now(),
				es_anulado = TRUE
			WHERE 	atencion_id IN (SELECT id FROM transaccional.atencion WHERE rri = _rrn);
	
			UPDATE 	transaccional.practica 
			SET  	fecha_delete = now(),
				fecha_update = now(),
				es_anulado = TRUE
			WHERE 	atencion_id IN (SELECT id FROM transaccional.atencion WHERE rri = _rrn);       
	
			UPDATE 	transaccional.auditoria 
			SET 	fecha_delete = now(),
				fecha_update = now(),
				es_anulado = TRUE
			WHERE 	atencion_id IN (SELECT id FROM transaccional.atencion WHERE rri = _rrn);       
	            
			INSERT INTO transaccional.anulado(fecha, tipo_anulacion, rri, system_trace, mensaje_id, rri_anulado, atencion_id) 
			select 	now(), 'T', _rri_anulacion, _motivo_anulacion, _rri_anulacion, _rrn, id 
			from 	transaccional.atencion 
			where 	rri = _rrn;
	
			INSERT INTO transaccional.anulado_detalle(anulado_id, prescripcion_id) 
			SELECT 	a.id, pr.id 
			FROM 	transaccional.prescripcion pr 
				inner join transaccional.anulado a on pr.mensaje_id = a.rri_anulado
			WHERE 	pr.mensaje_id = _rrn and 
				es_practica is false;
	             
			INSERT INTO transaccional.anulado_detalle(anulado_id, practica_id) 
			SELECT 	a.id, p.id 
			FROM 	transaccional.prescripcion pr 
				inner join transaccional.practica p on pr.id = p.prescripcion_id
				inner join transaccional.anulado a on pr.mensaje_id = a.rri_anulado
			WHERE 	pr.mensaje_id = _rrn and 
				es_practica is true;
			
			return query select 'OK'::varchar, 'SUCCESS'::varchar;

		else
		 	return query select 'RRI Invalido'::varchar, 'ERROR'::varchar;
		end if;
	        
		
        END;
    $function$
;
