CREATE OR REPLACE FUNCTION transaccional.get_prestador(_cuit character, _razon_social character varying, _numero_prestador character varying)
 RETURNS TABLE(id bigint, razon_social text, cuit character, nro_prestador bigint, homologa boolean, domicilio character varying)
 LANGUAGE plpgsql
AS $function$
declare
	_cuit_ character varying;
	_razon_social_ character varying;
	_numero_prestador_ character varying;
begin
	if (_cuit is not null) then
		_cuit_ = '%'|| _cuit::varchar || '%';
	end if;

	if (_razon_social is not null) then		
		_razon_social_ = '%'|| _razon_social::varchar || '%';
	end if;
	
if(_numero_prestador != '') then
	
	return query select p.id,trim(p.razon_social),p.cuit,p.nro_prestador,p.homologa,f.domicilio from transaccional.prestador p 
	left join transaccional.facturador f on f.cliente_id = p.nro_prestador::character varying
	where  p.razon_social ilike coalesce(_razon_social_,p.razon_social) and p.cuit ilike coalesce(_cuit_,p.cuit) and p.fecha_delete is null and p.habilitado is true
	and (case when _numero_prestador is not null then (similarity(p.nro_prestador::character varying,_numero_prestador) > 0.5) else 1=1 end) ;
else
	return query select p.id,trim(p.razon_social),p.cuit,p.nro_prestador,p.homologa,f.domicilio from transaccional.prestador p 
	left join transaccional.facturador f on f.cliente_id = p.nro_prestador::character varying
	where p.razon_social ilike coalesce(_razon_social_,p.razon_social) 
	and p.cuit ilike coalesce(_cuit_,p.cuit) and p.fecha_delete is null and p.habilitado is true;
end if;

end;
 $function$
;
