CREATE OR REPLACE FUNCTION seguridad.crear_usuario_rol(prestador_usuario character, pass character, email character, _id bigint, tipo_usuario character, rol character, telefono character varying)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

begin


declare
    
        apellido_nombre             varchar(50);
        rol_id                      int8;
        usuario_count                     int4;
        rol_count                     int4;
        usuario_id					int8;
begin
                if upper(tipo_usuario) = 'PR' then
                    apellido_nombre := (SELECT razon_social FROM transaccional.prestador WHERE id = _id);
                elsif upper(tipo_usuario) = 'AM' then
                    apellido_nombre := (SELECT apellido || ' ' || nombre FROM transaccional.auditor  WHERE id = _id);
				elsif upper(tipo_usuario) = 'DE' then
                    apellido_nombre := (SELECT descripcion FROM transaccional.delegacion WHERE id = _id);
               end if ;

       		  usuario_id := (SELECT u.id FROM seguridad.sist_seguridad_usuario u WHERE u.usuario =  prestador_usuario);
              usuario_count := (SELECT count(u.id) FROM seguridad.sist_seguridad_usuario u WHERE u.usuario = prestador_usuario or u.usuario like (prestador_usuario || '/BAJA%'));
 
             if usuario_count <= 0 then
                    INSERT INTO seguridad.sist_seguridad_usuario(usuario, password, nombre_apellido, telefono, email, fecha_alta, fecha_baja, fecha_cambio_clave, activo, clave_recuperacion, id_beneficiario) 
                    VALUES(prestador_usuario, pass, apellido_nombre, telefono,email, now(), null, null, 1, '', null);
               
                    usuario_id := (SELECT u.id FROM seguridad.sist_seguridad_usuario u WHERE u.usuario = prestador_usuario);
             
                    INSERT INTO seguridad.sist_seguridad_usuario_modelo( usuario_modelo_id, usuario_seguridad_id, tipo_usuario_modelo) 
                    VALUES(_id,usuario_id, tipo_usuario);
			
		           	  usuario_id := (SELECT u.id FROM seguridad.sist_seguridad_usuario u WHERE u.usuario = prestador_usuario);
		            	
		              rol_id := (SELECT id FROM seguridad.sist_seguridad_rol  WHERE upper(descripcion) = upper(rol));
				        
		              INSERT INTO seguridad.sist_seguridad_usuario_rol( id_usuario, id_entidad_sucursal, numero_consultorio, id_rol, tipo_ingreso) 
		              VALUES(usuario_id, 0, '', rol_id, '');
		
		              rol_id := (SELECT id FROM seguridad.sist_seguridad_rol  WHERE codigo ='3');
				      
		              rol_count := (SELECT count(1) FROM seguridad.sist_seguridad_usuario_rol ur
		                            join seguridad.sist_seguridad_rol r on ur.id_rol = r.id
		                            where r.codigo = '3' and ur.id_usuario = usuario_id);
		
		              if rol_count  <= 0 then
		                INSERT INTO seguridad.sist_seguridad_usuario_rol( id_usuario, id_entidad_sucursal, numero_consultorio, id_rol, tipo_ingreso) 
		                VALUES(usuario_id, 0, '', rol_id, '');
		              end if;
		             
		             return query select 'El usuario fue creado exitosamente.'::varchar,'SUCCESS'::varchar;
			 else
			 	return query select 'El Usuario ya existe'::varchar,'ERROR'::varchar;
             end if;
end;
end;
$function$
;
