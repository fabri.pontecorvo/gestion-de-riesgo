CREATE OR REPLACE FUNCTION seguridad.set_password_usuario(_tipo_usuario character varying, _campo_busqueda character varying, _usuario character varying, _pass character varying)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$ 
declare 
	_id int8;
begin
	if (_pass is not null and trim(_pass) <> '' and (_campo_busqueda is not null or _usuario is not null)) then
	
		if (_tipo_usuario is null or _tipo_usuario not in ('PR','AM','BE','DE')) then
		
			return query select 'El tipo de usuario debe ser de uno de los siguientes tipos: PR, AM, BE o DE'::varchar, 'ERROR'::varchar;
		else
			
			select	id 
			into 	_id 
			from 	transaccional.get_usuarios(_tipo_usuario,_campo_busqueda,_usuario);
		
			if (_id is not null) then
				update 	seguridad.sist_seguridad_usuario 
				set 	password = _pass 
				where 	id = _id;
				return query select 'OK'::varchar, 'SUCCESS'::varchar;
			else
				return query select 'No existe usuario'::varchar, 'ERROR'::varchar;	
			end if;
		
		end if;	
	
	else
		return query select 'El campo password, y el campo usuario o el campo de busqueda no puede estar vacio'::varchar, 'ERROR'::varchar;
		
	end if;
end
 $function$
;
