CREATE OR REPLACE FUNCTION transaccional.get_usuarios(_tipo_usuario character varying, _id bigint, _usuario character varying)
 RETURNS TABLE(id bigint, usuario character varying, password character varying, nombre_apellido character varying, telefono character varying, email character varying, fecha_alta timestamp without time zone, fecha_baja timestamp without time zone, fecha_cambio_clave timestamp without time zone, activo integer, clave_recuperacion character varying, id_beneficiario bigint, fecha_insert timestamp without time zone, fecha_update timestamp without time zone, fecha_delete timestamp without time zone, roles text)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
	declare
	begin
		if (upper(_tipo_usuario) = 'PR') then
			return query select usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete,string_agg(r.descripcion, ',') from seguridad.sist_seguridad_usuario usr
			inner join seguridad.sist_seguridad_usuario_modelo modelo on modelo.usuario_seguridad_id = usr.id and upper(modelo.tipo_usuario_modelo) = upper( _tipo_usuario)
			inner join transaccional.prestador prest on modelo.usuario_modelo_id = prest.id 
			left join seguridad.sist_seguridad_usuario_rol ur on ur.id_usuario = usr.id
			left join seguridad.sist_seguridad_rol r on r.id = ur.id_rol
			where (case when _usuario is null then 1=1 else usr.usuario ilike '%'|| _usuario ||'%' end) and (case when _id is null then 1=1 else prest.id = _id end) group by usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,
usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete;
		elseif (upper(_tipo_usuario) = 'AM') then
			return query select usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete,string_agg(r.descripcion, ',') from seguridad.sist_seguridad_usuario usr
			inner join seguridad.sist_seguridad_usuario_modelo modelo on modelo.usuario_seguridad_id = usr.id and upper(modelo.tipo_usuario_modelo) = upper( _tipo_usuario)
			inner join transaccional.auditor aud on modelo.usuario_modelo_id = aud.id 
			left join seguridad.sist_seguridad_usuario_rol ur on ur.id_usuario = usr.id
			left join seguridad.sist_seguridad_rol r on r.id = ur.id_rol
			where (case when _usuario is null then 1=1 else usr.usuario ilike '%'|| _usuario ||'%' end) and (case when _id is null then 1=1 else aud.id = _id end) group by usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,
usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete; 
		elseif (upper(_tipo_usuario) = 'DE') then
			return query select usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete,string_agg(r.descripcion, ',') from seguridad.sist_seguridad_usuario usr
			inner join seguridad.sist_seguridad_usuario_modelo modelo on modelo.usuario_seguridad_id = usr.id and upper(modelo.tipo_usuario_modelo) = upper( _tipo_usuario)
			inner join transaccional.delegacion dele on modelo.usuario_modelo_id = dele.id 
			left join seguridad.sist_seguridad_usuario_rol ur on ur.id_usuario = usr.id
			left join seguridad.sist_seguridad_rol r on r.id = ur.id_rol
			where  (case when _usuario is null then 1=1 else usr.usuario ilike '%'|| _usuario ||'%' end) and (case when _id is null then 1=1 else dele.id = _id end) group by usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,
usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete;			
		elseif (upper(_tipo_usuario) = 'BE') then
			return query select usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete,string_agg(r.descripcion, ',') from seguridad.sist_seguridad_usuario usr
			inner join transaccional.beneficiario bene on bene.id = usr.id_beneficiario and bene.id = _id
			left join seguridad.sist_seguridad_usuario_rol ur on ur.id_usuario = usr.id
			left join seguridad.sist_seguridad_rol r on r.id = ur.id_rol
			where (usr.fecha_delete is null or usr.fecha_delete > now()) or (bene.fecha_delete is null or bene.fecha_delete > now()) group by usr.id, usr.usuario,usr.password,usr.nombre_apellido,usr.telefono,usr.email,usr.fecha_alta,usr.fecha_baja,usr.fecha_cambio_clave,usr.activo,
usr.clave_recuperacion,usr.id_beneficiario,usr.fecha_insert,usr.fecha_update,usr.fecha_delete;			
		end if;
	end
 $function$
;
