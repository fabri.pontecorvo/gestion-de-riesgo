CREATE OR REPLACE FUNCTION transaccional.get_delegacion_by_id(_id bigint)
 RETURNS TABLE(id bigint, codigo character, descripcion character varying, abreviatura character varying, email character varying, fecha_insert timestamp without time zone, fecha_delete timestamp without time zone)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	
	
	return query
	
	select de.id, de.codigo, de.descripcion, de.abreviatura, de.email, de.fecha_insert, de.fecha_delete
	from transaccional.delegacion de
	where de.id = _id ;


end

 $function$
;
