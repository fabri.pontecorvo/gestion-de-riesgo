CREATE OR REPLACE FUNCTION seguridad.eliminar_roles(nombre_usuario character)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
declare
begin


               delete from seguridad.sist_seguridad_usuario_rol where id_usuario = (SELECT id FROM seguridad.sist_seguridad_usuario  WHERE usuario = nombre_usuario);
				
               return query select 'OK'::varchar,'SUCCESS'::varchar;

end;
end;
$function$
;
