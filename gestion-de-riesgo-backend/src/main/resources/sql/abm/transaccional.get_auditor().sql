CREATE OR REPLACE FUNCTION transaccional.get_auditor(_nombre character varying, _apellido character varying, _mail character varying)
 RETURNS TABLE(id bigint, cuit character, nombre character varying, apellido character varying, matricula numeric, tipo_matricula character, telefono character varying, mail character varying, fecha_baja timestamp without time zone, fecha_insert timestamp without time zone, fecha_delete timestamp without time zone, sml_apellido double precision)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$ 	
begin	
	
	if (_nombre is not null or _apellido is not null or _mail is not null)
	
		then return query
	
		select au.id, au.cuit, au.nombre, au.apellido, au.matricula, au.tipo_matricula, au.telefono, au.mail, au.fecha_baja, au.fecha_insert, au.fecha_delete, similarity(au.apellido, _apellido)::float as sml_apellido
		from transaccional.auditor au
		where similarity(au.nombre, coalesce(nullif(_nombre, ''), au.nombre)) > 0.4 and similarity(au.apellido, coalesce(nullif(_apellido, ''), au.apellido)) > 0.4 and similarity(au.mail, coalesce(nullif(_mail, ''), au.mail)) > 0.4
		order by sml_apellido desc, au.apellido;

	else
	
		raise exception 'Debe ingresar por lo menos un parámetro distinto de null';

	end if;

end
 $function$
;
