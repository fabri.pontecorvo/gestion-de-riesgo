CREATE OR REPLACE FUNCTION transaccional.get_auditor_by_id(_id bigint)
 RETURNS TABLE(id bigint, cuit character, nombre character varying, apellido character varying, matricula numeric, tipo_matricula character, telefono character varying, mail character varying, fecha_baja timestamp without time zone, fecha_insert timestamp without time zone, fecha_delete timestamp without time zone, sml_apellido double precision)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$ 	
begin	
	
	 return query select au.id, au.cuit, au.nombre, au.apellido, au.matricula, au.tipo_matricula, au.telefono, au.mail, au.fecha_baja, au.fecha_insert, au.fecha_delete, similarity(au.apellido, au.apellido)::float as sml_apellido
		from transaccional.auditor au
		where au.id = _id;

end
 $function$
;
