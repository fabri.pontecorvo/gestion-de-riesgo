CREATE OR REPLACE FUNCTION seguridad.asignar_rol_a_usuario(nombre_usuario character, rol character)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

begin


declare
    

        usuario_id					int8;

        rol_id int8;
begin
               usuario_id := (SELECT id FROM seguridad.sist_seguridad_usuario  WHERE usuario = nombre_usuario);

               rol_id := (SELECT id FROM seguridad.sist_seguridad_rol  WHERE upper(descripcion) = upper(rol) );

               INSERT INTO seguridad.sist_seguridad_usuario_rol(id_usuario, id_entidad_sucursal, id_rol) VALUES(usuario_id,0,rol_id);
				
               return query select 'OK'::varchar,'SUCCESS'::varchar;

end;
end;
$function$
;
