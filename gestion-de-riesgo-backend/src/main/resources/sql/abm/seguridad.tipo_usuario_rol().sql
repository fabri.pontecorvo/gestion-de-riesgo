CREATE OR REPLACE FUNCTION seguridad.tipo_usuario_rol()
 RETURNS TABLE(tipo_usuarios character varying, abreviatura character varying, roles character varying)
 LANGUAGE sql
 SECURITY DEFINER
AS $function$ 	
	select
	ap.descripcion,
	ap.abreviatura,
	string_agg(rol.descripcion, ',') roles
from
	seguridad.sist_seguridad_aplicacion ap
inner join seguridad.sist_seguridad_rol rol on
	rol.id_aplicacion = ap.id
where rol.descripcion <> 'DELEGACION'
group by
ap.descripcion,
ap.abreviatura

union
	select 
	'Delegación' as tipo_usuarios,
	'DE' as abreviatura,
	'DELEGACION' as roles

 $function$
;
