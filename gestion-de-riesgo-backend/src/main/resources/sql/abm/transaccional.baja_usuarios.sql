CREATE OR REPLACE FUNCTION transaccional.baja_usuarios(_tipo_usuario character varying, _id bigint, _usuario character varying)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$ 
declare 
	_id_usuario int8;
	_id_beneficiario int8;
	_id_entity int8;
begin
		if(_tipo_usuario is not null and (_id is not null or _usuario is not null)) then
			select id,id_beneficiario into _id_usuario,_id_beneficiario from transaccional.get_usuarios(_tipo_usuario,_id,_usuario);
			if(_id_usuario is not null) then
				
				if(select exists(select id from seguridad.sist_seguridad_usuario where id = _id_usuario and fecha_baja is null and fecha_delete is null )) then
					update seguridad.sist_seguridad_usuario set fecha_delete = now(),fecha_baja = now(),usuario = usuario || '/BAJA' where id=_id_usuario;
				
					return query select 'La baja fue realizada exitosamente.'::varchar,'SUCCESS'::varchar;

				else
					return query select 'El usuario ya se encuentra dado de baja.'::varchar,'ERROR'::varchar;
				end if;
				
			else
				return query select 'No existe el usuario.'::varchar,'ERROR'::varchar;	
		end if;
		else
			return query select 'El tipo de usuario y por lo menos el usuario o el campo busqueda deben ser distintos de null'::varchar,'ERROR'::varchar;	
		end if;
end
 $function$
;
