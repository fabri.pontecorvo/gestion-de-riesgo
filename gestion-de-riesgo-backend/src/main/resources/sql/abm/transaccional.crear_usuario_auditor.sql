CREATE OR REPLACE FUNCTION transaccional.crear_usuario_auditor(_cuit character varying, _nombre character varying, _apellido character varying, _matricula numeric, _telefono character varying, _email character varying, _usuario character varying, _password character varying, _rol character varying)
 RETURNS TABLE(detalle character varying, status character varying, id_entity bigint)
 LANGUAGE plpgsql
AS $function$ 	
declare
	_id int8;
begin	
	
	 insert into transaccional.auditor(cuit,nombre,apellido,matricula,tipo_matricula,telefono,mail) 
		  values (_cuit,_nombre,_apellido,_matricula,'T',_telefono,_email) 
		  returning id into _id;
	
	return query select *,_id from seguridad.crear_usuario_rol(_usuario,_password,_email,_id,'AM',_rol,_telefono); 
	
end
 $function$
;
