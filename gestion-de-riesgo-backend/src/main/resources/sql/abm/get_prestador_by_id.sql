CREATE OR REPLACE FUNCTION transaccional.get_prestador_by_id(_id bigint)
 RETURNS TABLE(id bigint, razon_social text, cuit character, nro_prestador bigint, homologa boolean, domicilio character varying)
 LANGUAGE plpgsql
AS $function$
declare

begin

	return query select p.id,trim(p.razon_social),p.cuit,p.nro_prestador,p.homologa,f.domicilio 
	from transaccional.prestador p 
	left join transaccional.facturador f on f.cliente_id = p.nro_prestador::character varying
	where p.id = _id  and p.fecha_delete is null and p.habilitado is true;

end;
 $function$
;
