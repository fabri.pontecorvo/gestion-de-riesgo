CREATE OR REPLACE FUNCTION transaccional.crear_usuario_delegacion(_descripcion character varying, _codigo character varying, _abreviatura character varying, _email character varying, _usuario character varying, _password character varying, _rol character varying)
 RETURNS TABLE(detalle character varying, status character varying, id_entity bigint)
 LANGUAGE plpgsql
AS $function$ 	
declare
	_id int8;
begin	
	
	 insert into transaccional.delegacion(descripcion,codigo,abreviatura,email) 
		  values (_descripcion,_codigo,_abreviatura,_email) 
		  returning id into _id;
	
	return query select *,_id from seguridad.crear_usuario_rol(_usuario,_password,_email,_id,'DE',_rol,''); 
	
end
 $function$
;
