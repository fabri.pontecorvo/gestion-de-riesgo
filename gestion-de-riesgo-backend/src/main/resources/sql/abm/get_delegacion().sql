CREATE OR REPLACE FUNCTION transaccional.get_delegacion(_descripcion character varying)
 RETURNS TABLE(id bigint, codigo character, descripcion character varying, abreviatura character varying, email character varying, fecha_insert timestamp without time zone, fecha_delete timestamp without time zone)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	if (_descripcion is not null)
	
	then
	
	return query
	
	select de.id, de.codigo, de.descripcion, de.abreviatura, de.email, de.fecha_insert, de.fecha_delete
	from transaccional.delegacion de
	where similarity(de.descripcion,_descripcion)> 0.2;

	else	
		raise exception 'El parámetro debe ser distinto de null';
	end if;
end

 $function$
;
