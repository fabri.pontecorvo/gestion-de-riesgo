CREATE OR REPLACE FUNCTION seguridad.modificar_usuario(_usuario_original character varying, _usuario_editar character varying, _password character varying, _email character varying, _telefono character varying, _habilitar boolean)
 RETURNS TABLE(detalle character varying, status character varying)
 LANGUAGE plpgsql
AS $function$ 
	declare
	_id int8;
	_id_tipo_doc int8;
begin
if(select exists (select id from  seguridad.sist_seguridad_usuario where usuario = _usuario_original )) then
	
	if(select exists (select id from  seguridad.sist_seguridad_usuario where usuario = _usuario_editar ) and _usuario_original <> _usuario_editar)then
		return query select 'El usuario ya existe.'::varchar, 'ERROR'::varchar ;	
	else
		update seguridad.sist_seguridad_usuario set usuario = _usuario_editar, password = _password, email = _email, telefono = _telefono, fecha_delete = (case when _habilitar then  null else fecha_delete end), fecha_baja = (case when _habilitar then  null else fecha_baja end), fecha_update = now()
		where usuario = _usuario_original;
	
		return query select 'Los datos fueron modificados exitosamente.'::varchar, 'SUCCESS'::varchar ;	
	end if;
	

else
	return query select 'No existe el usuario que desea editar.'::varchar, 'ERROR'::varchar ;	

end if;
end;

 $function$
;
