package com.conexia.riesgos.dto.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.conexia.riesgos.annotation.NotContainsWhitespace;

public class UsuarioAltaDelegacionDto {
	@NotContainsWhitespace
	private String descripcion;
	@NotContainsWhitespace
	private String abreviatura;
	@NotContainsWhitespace
	private String codigo;
	@Email(message = "Formato de email incorrecto")
	private String email;
	@NotContainsWhitespace
	private String usuario;
	@NotEmpty(message = "El password esta vacío")
	@Size(min = 6, max = 16)
	private String password;
	@NotNull
	private String rol;
	@NotNull
	private String telefono;
	
	

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public UsuarioAltaDelegacionDto(@NotContainsWhitespace String descripcion,
			@NotContainsWhitespace String abreviatura, @NotContainsWhitespace String codigo,
			@Email(message = "Formato de email incorrecto") String email, @NotContainsWhitespace String usuario,
			@NotEmpty(message = "El password esta vacío") @Size(min = 6, max = 16) String password, @NotNull String rol,
			@NotNull String telefono) {
		super();
		this.descripcion = descripcion;
		this.abreviatura = abreviatura;
		this.codigo = codigo;
		this.email = email;
		this.usuario = usuario;
		this.password = password;
		this.rol = rol;
		this.telefono = telefono;
	}

	public UsuarioAltaDelegacionDto() {
		super();
	}

}
