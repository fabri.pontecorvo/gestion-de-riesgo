package com.conexia.riesgos.dto.response;

public class GrupoRiesgoDto {
	private String codigo;
	private String descripcion;
	private Integer frecuenciaPoblacional;
	private Double porcentajePoblacionRiesgo;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getFrecuenciaPoblacional() {
		return frecuenciaPoblacional;
	}
	public void setFrecuenciaPoblacional(Integer frecuenciaPoblacional) {
		this.frecuenciaPoblacional = frecuenciaPoblacional;
	}
	public Double getPorcentajePoblacionRiesgo() {
		return porcentajePoblacionRiesgo;
	}
	public void setPorcentajePoblacionRiesgo(Double porcentajePoblacionRiesgo) {
		this.porcentajePoblacionRiesgo = porcentajePoblacionRiesgo;
	}
	public GrupoRiesgoDto(String codigo, String descripcion, Integer frecuenciaPoblacional,
			Double porcentajePoblacionRiesgo) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.frecuenciaPoblacional = frecuenciaPoblacional;
		this.porcentajePoblacionRiesgo = porcentajePoblacionRiesgo;
	}
	public GrupoRiesgoDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}