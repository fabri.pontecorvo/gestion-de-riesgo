package com.conexia.riesgos.dto.request;

public class UsuarioBajaConsultaDto extends AbstractUsuarioDto {

	private Long id;

	public UsuarioBajaConsultaDto() {
		super();
	}

	public UsuarioBajaConsultaDto(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

}
