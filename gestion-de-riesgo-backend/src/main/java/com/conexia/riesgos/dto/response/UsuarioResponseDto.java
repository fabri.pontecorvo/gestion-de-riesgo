package com.conexia.riesgos.dto.response;

import java.math.BigInteger;
import java.util.Date;

import com.conexia.riesgos.storedprocedure.UsuarioStoredProcedureEntity;

public class UsuarioResponseDto {
	private BigInteger id;
	private String usuario;
	private String password;
	private String nombreApellido;
	private String telefono;
	private String email;
	private Date fechaAlta;
	private Date fechaBaja;
	private Date fechaCambioClave;
	private Integer activo;
	private String claveRecuperacion;
	private BigInteger idBeneficiario;
	private Date fechaInsert;
	private Date fechaUpdate;
	private Date fechaDelete;
	private String[] roles;

	public UsuarioResponseDto(BigInteger id, String usuario, String password, String nombreApellido, String telefono,
			String email, Date fechaAlta, Date fechaBaja, Date fechaCambioClave, Integer activo,
			String claveRecuperacion, BigInteger idBeneficiario, Date fechaInsert, Date fechaUpdate, Date fechaDelete,
			String[] roles) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.password = password;
		this.nombreApellido = nombreApellido;
		this.telefono = telefono;
		this.email = email;
		this.fechaAlta = fechaAlta;
		this.fechaBaja = fechaBaja;
		this.fechaCambioClave = fechaCambioClave;
		this.activo = activo;
		this.claveRecuperacion = claveRecuperacion;
		this.idBeneficiario = idBeneficiario;
		this.fechaInsert = fechaInsert;
		this.fechaUpdate = fechaUpdate;
		this.fechaDelete = fechaDelete;
		this.roles = roles;
	}

	public UsuarioResponseDto(UsuarioStoredProcedureEntity entity) {
		this.id = entity.getId();
		this.usuario = entity.getUsuario();
		this.password = entity.getPassword();
		this.nombreApellido = entity.getNombreApellido();
		this.telefono = entity.getTelefono();
		this.email = entity.getEmail();
		this.fechaAlta = entity.getFechaAlta();
		this.fechaBaja = entity.getFechaBaja();
		this.fechaCambioClave = entity.getFechaCambioClave();
		this.activo = entity.getActivo();
		this.claveRecuperacion = entity.getClaveRecuperacion();
		this.idBeneficiario = entity.getIdBeneficiario();
		this.fechaInsert = entity.getFechaInsert();
		this.fechaUpdate = entity.getFechaUpdate();
		this.fechaDelete = entity.getFechaDelete();
		this.roles = entity.getRoles() == null ? null:entity.getRoles().split(",");
	}

	public UsuarioResponseDto() {

	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Date getFechaCambioClave() {
		return fechaCambioClave;
	}

	public void setFechaCambioClave(Date fechaCambioClave) {
		this.fechaCambioClave = fechaCambioClave;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public String getClaveRecuperacion() {
		return claveRecuperacion;
	}

	public void setClaveRecuperacion(String claveRecuperacion) {
		this.claveRecuperacion = claveRecuperacion;
	}

	public BigInteger getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(BigInteger idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	public Date getFechaInsert() {
		return fechaInsert;
	}

	public void setFechaInsert(Date fechaInsert) {
		this.fechaInsert = fechaInsert;
	}

	public Date getFechaUpdate() {
		return fechaUpdate;
	}

	public void setFechaUpdate(Date fechaUpdate) {
		this.fechaUpdate = fechaUpdate;
	}

	public Date getFechaDelete() {
		return fechaDelete;
	}

	public void setFechaDelete(Date fechaDelete) {
		this.fechaDelete = fechaDelete;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

}
