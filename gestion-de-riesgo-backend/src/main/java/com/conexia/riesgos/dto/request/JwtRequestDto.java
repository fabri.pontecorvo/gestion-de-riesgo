package com.conexia.riesgos.dto.request;

import java.io.Serializable;

public class JwtRequestDto implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;

	private String username;
	private String password;
	private String recaptchaResponse;
	private Boolean captchaError;

	// need default constructor for JSON Parsing
	public JwtRequestDto() {

	}

	public JwtRequestDto(String username, String password, String recaptchaResponse, Boolean captchaError) {
		super();
		this.username = username;
		this.password = password;
		this.recaptchaResponse = recaptchaResponse;
		this.captchaError = captchaError;
	}

	public JwtRequestDto(String username, String password, String recaptchaResponse) {
		super();
		this.username = username;
		this.password = password;
		this.recaptchaResponse = recaptchaResponse;
	}

	public JwtRequestDto(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
	}

	public Boolean getCaptchaError() {
		return captchaError;
	}

	public void setCaptchaError(Boolean captchaError) {
		this.captchaError = captchaError;
	}

	public String getRecaptchaResponse() {
		return recaptchaResponse;
	}

	public void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}