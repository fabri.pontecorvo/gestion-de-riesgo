package com.conexia.riesgos.dto.request;

import com.conexia.riesgos.annotation.NotContainsWhitespace;

public class AbstractUsuarioDto {

	private String usuario;

	@NotContainsWhitespace
	private String abreviatura;

	public AbstractUsuarioDto() {
		super();
	}

	public AbstractUsuarioDto(@NotContainsWhitespace String usuario, @NotContainsWhitespace String abreviatura) {
		super();
		this.usuario = usuario;
		this.abreviatura = abreviatura;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

}
