package com.conexia.riesgos.dto.request;

import javax.validation.constraints.NotNull;

import com.conexia.riesgos.annotation.NotContainsWhitespace;
import com.conexia.riesgos.annotation.NotEmptyIfAnotherFieldIsEmpty;

@NotEmptyIfAnotherFieldIsEmpty(fieldName = "usuario", dependFieldName = "campoBusqueda", message = "Debe ingresar el campo busqueda o el campo usuario")


public class UsuarioSetPasswordRequestDto {
	
	private String tipoUsuario;
	@NotNull
	private String campoBusqueda;
	@NotNull
	private String usuario;
	@NotContainsWhitespace
	private String password;
	
	
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}	
	
	public String getCampoBusqueda() {
		return campoBusqueda;
	}
	
	public void setCampoBusqueda(String campoBusqueda) {
		this.campoBusqueda = campoBusqueda;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}	
	
}
