package com.conexia.riesgos.dto.response;

public class ComboDto {

	private String codigo;
	
	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String Codigo) {
		this.codigo = Codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ComboDto(String Codigo, String descripcion) {
		super();
		this.codigo = Codigo;
		this.descripcion = descripcion;
	}

	public ComboDto() {
		super();
		// TODO Auto-generated constructor stub
	}
}