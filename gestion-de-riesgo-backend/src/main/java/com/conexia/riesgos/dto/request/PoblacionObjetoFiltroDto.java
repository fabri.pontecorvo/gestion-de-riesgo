package com.conexia.riesgos.dto.request;

public class PoblacionObjetoFiltroDto {
	
	private String codGenero;
	private Integer edadDesde;
	private Integer edadHasta;
	private String codProvincia;
	private String codLocalidad;
	
	private String codGrupoPoblacional;

	public String getCodGenero() {
		return codGenero;
	}

	public void setCodGenero(String codGenero) {
		this.codGenero = codGenero;
	}

	public Integer getEdadDesde() {
		return edadDesde;
	}

	public void setEdadDesde(Integer edadDesde) {
		this.edadDesde = edadDesde;
	}

	public Integer getEdadHasta() {
		return edadHasta;
	}

	public void setEdadHasta(Integer edadHasta) {
		this.edadHasta = edadHasta;
	}

	public String getCodProvincia() {
		return codProvincia;
	}

	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}

	public String getCodLocalidad() {
		return codLocalidad;
	}

	public void setCodLocalidad(String codLocalidad) {
		this.codLocalidad = codLocalidad;
	}

	public String getCodGrupoPoblacional() {
		return codGrupoPoblacional;
	}

	public void setCodGrupoPoblacional(String codGrupoPoblacional) {
		this.codGrupoPoblacional = codGrupoPoblacional;
	}
}
