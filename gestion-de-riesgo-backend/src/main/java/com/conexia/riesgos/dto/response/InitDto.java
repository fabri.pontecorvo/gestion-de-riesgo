package com.conexia.riesgos.dto.response;

import java.util.List;

public class InitDto {

	private List<ComboDto> ambitos;
	
	private List<ComboDto> generos;
	
	private List<ComboDto> diagnosticos;
	
	private List<ComboDto> procedimientos;
	
	private List<ComboDto> rangosEtarios;
	
	private List<ComboDto> tiposDocumentos;
	
	private List<ComboDto> tiposEmpadronamientos;
	
	private List<ComboDto> tiposParientes;
	
	private List<ComboDto> gruposRiegos;
	
	private List<ComboDto> gruposPoblacionales;
	
	private List<ProvinciaDto> provincias;

	public List<ComboDto> getAmbitos() {
		return ambitos;
	}

	public void setAmbitos(List<ComboDto> ambitos) {
		this.ambitos = ambitos;
	}

	public List<ComboDto> getGeneros() {
		return generos;
	}

	public void setGeneros(List<ComboDto> generos) {
		this.generos = generos;
	}

	public List<ComboDto> getDiagnosticos() {
		return diagnosticos;
	}

	public void setDiagnosticos(List<ComboDto> diagnosticos) {
		this.diagnosticos = diagnosticos;
	}

	public List<ComboDto> getProcedimientos() {
		return procedimientos;
	}

	public void setProcedimientos(List<ComboDto> procedimientos) {
		this.procedimientos = procedimientos;
	}

	public List<ComboDto> getRangosEtarios() {
		return rangosEtarios;
	}

	public void setRangosEtarios(List<ComboDto> rangosEtarios) {
		this.rangosEtarios = rangosEtarios;
	}

	public List<ComboDto> getTiposDocumentos() {
		return tiposDocumentos;
	}

	public void setTiposDocumentos(List<ComboDto> tiposDocumentos) {
		this.tiposDocumentos = tiposDocumentos;
	}

	public List<ComboDto> getTiposEmpadronamientos() {
		return tiposEmpadronamientos;
	}

	public void setTiposEmpadronamientos(List<ComboDto> tiposEmpadronamientos) {
		this.tiposEmpadronamientos = tiposEmpadronamientos;
	}

	public List<ComboDto> getTiposParientes() {
		return tiposParientes;
	}

	public void setTiposParientes(List<ComboDto> tiposParientes) {
		this.tiposParientes = tiposParientes;
	}

	public List<ComboDto> getGruposRiegos() {
		return gruposRiegos;
	}

	public void setGruposRiegos(List<ComboDto> gruposRiegos) {
		this.gruposRiegos = gruposRiegos;
	}

	public List<ComboDto> getGruposPoblacionales() {
		return gruposPoblacionales;
	}

	public void setGruposPoblacionales(List<ComboDto> gruposPoblacionales) {
		this.gruposPoblacionales = gruposPoblacionales;
	}

	public List<ProvinciaDto> getProvincias() {
		return provincias;
	}

	public void setProvincias(List<ProvinciaDto> provincias) {
		this.provincias = provincias;
	}
}
