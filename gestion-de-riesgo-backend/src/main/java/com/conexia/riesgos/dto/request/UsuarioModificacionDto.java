package com.conexia.riesgos.dto.request;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.conexia.riesgos.annotation.NotContainsWhitespace;

public class UsuarioModificacionDto {
	private String usuarioOriginal;
	@NotContainsWhitespace
	private String usuarioEditar;
	private String passwordEditar;
	@Email
	private String emailEditar;
	private String telefonoEditar;
	@NotNull
	private List<String> listaRolesEditar;
	private Boolean habilitar;

	public UsuarioModificacionDto(String usuarioOriginal, String usuarioEditar, String passwordEditar,
			String emailEditar, String telefonoEditar, List<String> listaRolesEditar, Boolean habilitar) {
		this.usuarioOriginal = usuarioOriginal;
		this.usuarioEditar = usuarioEditar;
		this.passwordEditar = passwordEditar;
		this.setEmailEditar(emailEditar);
		this.setTelefonoEditar(telefonoEditar);
		this.listaRolesEditar = listaRolesEditar;
		this.habilitar = habilitar;
	}


	public Boolean getHabilitar() {
		return habilitar;
	}

	public void setHabilitar(Boolean habilitar) {
		this.habilitar = habilitar;
	}

	public String getUsuarioOriginal() {
		return usuarioOriginal;
	}

	public void setUsuarioOriginal(String usuarioOriginal) {
		this.usuarioOriginal = usuarioOriginal;
	}

	public String getUsuarioEditar() {
		return usuarioEditar;
	}

	public void setUsuarioEditar(String usuarioEditar) {
		this.usuarioEditar = usuarioEditar;
	}

	public String getPasswordEditar() {
		return passwordEditar;
	}

	public void setPasswordEditar(String passwordEditar) {
		this.passwordEditar = passwordEditar;
	}

	public String getEmailEditar() {
		return emailEditar;
	}

	public void setEmailEditar(String emailEditar) {
		this.emailEditar = emailEditar == null ? "" : emailEditar;
	}

	public String getTelefonoEditar() {
		return telefonoEditar;
	}

	public void setTelefonoEditar(String telefonoEditar) {
		this.telefonoEditar = telefonoEditar == null ? "" : telefonoEditar;
	}

	public List<String> getListaRolesEditar() {
		return listaRolesEditar;
	}

	public void setListaRolesEditar(List<String> listaRolesEditar) {
		this.listaRolesEditar = listaRolesEditar;
	}

}
