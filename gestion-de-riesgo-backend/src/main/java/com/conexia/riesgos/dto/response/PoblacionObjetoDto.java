package com.conexia.riesgos.dto.response;

import java.util.List;

public class PoblacionObjetoDto {
	private List<String> rangosEtarios;
	private List<Integer> cantMasculinos;
	private List<Integer> cantFemeninos;
	
	private List<GrupoRiesgoDto> gruposRiegos;
	
	public List<String> getRangosEtarios() {
		return rangosEtarios;
	}
	public void setRangosEtarios(List<String> rangosEtarios) {
		this.rangosEtarios = rangosEtarios;
	}
	public List<Integer> getCantMasculinos() {
		return cantMasculinos;
	}
	public void setCantMasculinos(List<Integer> cantMasculinos) {
		this.cantMasculinos = cantMasculinos;
	}
	public List<Integer> getCantFemeninos() {
		return cantFemeninos;
	}
	public void setCantFemeninos(List<Integer> cantFemeninos) {
		this.cantFemeninos = cantFemeninos;
	}
	public List<GrupoRiesgoDto> getGruposRiegos() {
		return gruposRiegos;
	}
	public void setGruposRiegos(List<GrupoRiesgoDto> gruposRiegos) {
		this.gruposRiegos = gruposRiegos;
	}
}