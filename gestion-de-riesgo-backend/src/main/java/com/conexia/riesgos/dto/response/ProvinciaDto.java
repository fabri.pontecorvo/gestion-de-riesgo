package com.conexia.riesgos.dto.response;

import java.util.List;

public class ProvinciaDto {

	private ComboDto provincia;
	
	private List<ComboDto> localidades;

	public ComboDto getProvincia() {
		return provincia;
	}

	public void setProvincia(ComboDto provincia) {
		this.provincia = provincia;
	}

	public List<ComboDto> getLocalidades() {
		return localidades;
	}

	public void setLocalidades(List<ComboDto> localidades) {
		this.localidades = localidades;
	}
}