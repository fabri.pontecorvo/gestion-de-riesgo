package com.conexia.riesgos.dto.response;

import java.util.Map;
import java.util.TreeMap;

/***
 * 
 * Dto para el envio de mails. Para usar el envio de mails con templates de
 * freemarker se debe completar el MAP mensajeAsMap como un treeMap() cada
 * elemento de este atributo tiene que llamarse de la misma forma en el template
 * para que se renderice de forma correcta.
 *
 */
public class MailDto {

	private String destinatario;

	private String asunto;

	private String mailContacto;

	private boolean enviarCopia;

	private Integer codigoNotificacion;

	private Map<String, Object> mensajeAsMap;

	/**
	 * Llenar el map con los campos del templete
	 */

	public MailDto() {
		super();

		Map<String, Object> map = new TreeMap<>();

		this.mensajeAsMap = map;

	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public Map<String, Object> getMensajeAsMap() {
		return mensajeAsMap;
	}

	public void setMensajeAsMap(Map<String, Object> mensajeAsMap) {
		this.mensajeAsMap = mensajeAsMap;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getMailContacto() {
		return mailContacto;
	}

	public void setMailContacto(String mailContacto) {
		this.mailContacto = mailContacto;
	}

	public boolean getEnviarCopia() {
		return enviarCopia;
	}

	public void setEnviarCopia(boolean enviarCopia) {
		this.enviarCopia = enviarCopia;
	}

	public Integer getCodigoNotificacion() {
		return codigoNotificacion;
	}

	public void setCodigoNotificacion(Integer codigoNotificacion) {
		this.codigoNotificacion = codigoNotificacion;
	}

}