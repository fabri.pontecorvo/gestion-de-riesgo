package com.conexia.riesgos.dto.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public abstract class IntervaloFechaDto {
	
	@DateTimeFormat(pattern="dd/MM/yyyy")
	protected Date fechaDesde;
	@DateTimeFormat(pattern="dd/mm/yyyy")
	protected Date fechaHasta;
	
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
}
