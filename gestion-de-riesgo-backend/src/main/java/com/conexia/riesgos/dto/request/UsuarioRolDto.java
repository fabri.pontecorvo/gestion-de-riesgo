package com.conexia.riesgos.dto.request;

import com.conexia.riesgos.annotation.NotContainsWhitespace;

public class UsuarioRolDto {
	@NotContainsWhitespace
	private String usuario;
	@NotContainsWhitespace
	private String rol;


	public UsuarioRolDto(String usuario, String rol) {
		super();
		this.usuario = usuario;
		this.rol = rol;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

}
