package com.conexia.riesgos.dto.request;

import java.util.List;

public class ListaPoblacionObjetoFiltroDto {
	
	private List<PoblacionObjetoFiltroDto> filtros;

	public List<PoblacionObjetoFiltroDto> getFiltros() {
		return filtros;
	}

	public void setFiltros(List<PoblacionObjetoFiltroDto> filtros) {
		this.filtros = filtros;
	}
	
	
}
