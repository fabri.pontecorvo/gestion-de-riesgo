package com.conexia.riesgos.dto.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UsuarioAltaDto extends AbstractUsuarioDto {
	
	@NotNull
	private Long id;
	
	@NotEmpty(message="El password esta vacío")
	@Size(min=6, max=16)
	private String password;

	@Email(message = "Formato de email incorrecto")
	private String email;
	
	private Integer numeroPrestador;
	
	@NotNull
	private String rol;
	
	@NotNull
	private String telefono;
	
	
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getNumeroPrestador() {
		return numeroPrestador;
	}

	public void setNumeroPrestador(Integer numeroPrestador) {
		this.numeroPrestador = numeroPrestador;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
	
}
