package com.conexia.riesgos.dto.request;

import java.math.BigInteger;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.conexia.riesgos.annotation.NotContainsWhitespace;

public class UsuarioAltaAuditorDto {
	@NotContainsWhitespace
	private String cuit;
	@NotContainsWhitespace
	private String nombre;
	@NotContainsWhitespace
	private String apellido;
	@NotNull
	private BigInteger matricula;
	@NotContainsWhitespace
	private String telefono;
	@Email(message = "Formato de email incorrecto")
	private String email;
	@NotContainsWhitespace
	private String usuario;
	@NotEmpty(message="El password esta vacío")
	@Size(min=6, max=16)	
	private String password;
	@NotNull
	private String rol;
	
	
	
	public UsuarioAltaAuditorDto(String cuit, String nombre, String apellido, BigInteger matricula, String telefono,
			String email, String usuario, String password, String rol) {
		super();
		this.cuit = cuit;
		this.nombre = nombre;
		this.apellido = apellido;
		this.matricula = matricula;
		this.telefono = telefono;
		this.email = email;
		this.usuario = usuario;
		this.password = password;
		this.rol = rol;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public BigInteger getMatricula() {
		return matricula;
	}
	public void setMatricula(BigInteger matricula) {
		this.matricula = matricula;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	
	
}
