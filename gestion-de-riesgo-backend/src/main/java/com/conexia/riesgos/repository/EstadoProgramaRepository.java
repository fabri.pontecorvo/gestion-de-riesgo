package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EstadoPrograma;

@Repository
public interface EstadoProgramaRepository extends JpaRepository<EstadoPrograma, Long> {

	
}
