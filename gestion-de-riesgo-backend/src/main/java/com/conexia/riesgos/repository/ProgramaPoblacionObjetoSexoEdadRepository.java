package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaPoblacionObjetoSexoEdad;

@Repository
public interface ProgramaPoblacionObjetoSexoEdadRepository extends JpaRepository<ProgramaPoblacionObjetoSexoEdad, Long> {

	
}
