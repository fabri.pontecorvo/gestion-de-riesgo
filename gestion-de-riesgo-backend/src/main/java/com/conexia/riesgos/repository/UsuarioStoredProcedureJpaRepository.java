package com.conexia.riesgos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.storedprocedure.UsuarioStoredProcedureEntity;

@Repository
public interface UsuarioStoredProcedureJpaRepository extends JpaRepository<UsuarioStoredProcedureEntity, Long> {

	/**
	 * La query a la que hace referencia se encuentra en @NamedNativeQuery en UsuarioStoredProcedureEntity
	 * 
	 * @param abreviatura
	 * @param campoBusqueda
	 * @param usuario
	 * @return List<UsuarioStoredProcedureEntity>
	 */
	List<UsuarioStoredProcedureEntity> getUsuarios(String abreviatura, Long id, String usuario);


}
