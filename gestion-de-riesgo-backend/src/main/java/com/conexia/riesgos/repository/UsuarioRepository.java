package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	/**
	 * 
	 * @param usuario
	 * Metodo de Logeo
	 */
	Usuario findByUsuario(String usuario);

}
