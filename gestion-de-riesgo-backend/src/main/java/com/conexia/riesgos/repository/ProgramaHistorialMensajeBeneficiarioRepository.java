package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaHistorialMensajeBeneficiario;

@Repository
public interface ProgramaHistorialMensajeBeneficiarioRepository extends JpaRepository<ProgramaHistorialMensajeBeneficiario, Long> {

	
}
