package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.GrupoRiesgo;

@Repository
public interface GrupoRiesgoRepository extends JpaRepository<GrupoRiesgo, Long> {

	
}
