package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.Profesional;

@Repository
public interface ProfesionalRepository extends JpaRepository<Profesional, Long> {

	
}
