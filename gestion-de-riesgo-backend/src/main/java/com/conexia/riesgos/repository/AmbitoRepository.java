package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.Ambito;

@Repository
public interface AmbitoRepository extends JpaRepository<Ambito, Long> {

	
}
