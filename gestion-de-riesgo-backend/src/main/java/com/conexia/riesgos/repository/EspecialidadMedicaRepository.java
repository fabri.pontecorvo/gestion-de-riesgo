package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EspecialidadMedica;

@Repository
public interface EspecialidadMedicaRepository extends JpaRepository<EspecialidadMedica, Long> {

	
}
