package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EmpadronamientoCondicion;

@Repository
public interface EmpadronamientoCondicionRepository extends JpaRepository<EmpadronamientoCondicion, Long> {

	
}
