package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.TipoPariente;

@Repository
public interface TipoParienteRepository extends JpaRepository<TipoPariente, Long> {

	
}
