package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.Provincia;

@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Long> {

	
}
