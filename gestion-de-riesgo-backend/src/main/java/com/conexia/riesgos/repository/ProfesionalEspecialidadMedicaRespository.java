package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProfesionalEspecialidadMedica;

@Repository
public interface ProfesionalEspecialidadMedicaRespository extends JpaRepository<ProfesionalEspecialidadMedica, Long> {

	
}
