package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EmpadronamientoDocumentacion;

@Repository
public interface EmpadronamientoDocumentacionRepository extends JpaRepository<EmpadronamientoDocumentacion, Long> {

	
}
