package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.Genero;

@Repository
public interface GeneroRepository extends JpaRepository<Genero, Long> {

	
}
