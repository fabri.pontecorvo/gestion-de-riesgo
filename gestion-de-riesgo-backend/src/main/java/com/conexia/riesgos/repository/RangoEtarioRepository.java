package com.conexia.riesgos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.RangoEtario;

@Repository
public interface RangoEtarioRepository extends JpaRepository<RangoEtario, Long> {
	
	List<RangoEtario> findByOrderByEdadHastaDesc();

	
}
