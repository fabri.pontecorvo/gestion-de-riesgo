package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EmpadronamientoExcepcion;

@Repository
public interface EmpadronamientoExcepcionRepository extends JpaRepository<EmpadronamientoExcepcion, Long> {

	
}
