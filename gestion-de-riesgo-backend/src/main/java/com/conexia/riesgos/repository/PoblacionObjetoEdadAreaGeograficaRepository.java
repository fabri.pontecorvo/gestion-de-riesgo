package com.conexia.riesgos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.storedprocedure.PoblacionObjetoEdadAreaGeografica;

@Repository
public interface PoblacionObjetoEdadAreaGeograficaRepository extends JpaRepository<PoblacionObjetoEdadAreaGeografica, Long> {
	
	List<PoblacionObjetoEdadAreaGeografica> findByFilters(String codGenero, Integer edadDesde, Integer edadHasta, String codLocadad, String codProvincia);

	
}
