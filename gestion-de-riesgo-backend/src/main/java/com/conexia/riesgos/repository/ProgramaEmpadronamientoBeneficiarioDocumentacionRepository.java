package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaEmpadronamientoBeneficiarioDocumentacion;

@Repository
public interface ProgramaEmpadronamientoBeneficiarioDocumentacionRepository extends JpaRepository<ProgramaEmpadronamientoBeneficiarioDocumentacion, Long> {

	
}
