package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EmpadronamientoMotivo;

@Repository
public interface EmpadronamientoMotivoRepository extends JpaRepository<EmpadronamientoMotivo, Long> {

	
}
