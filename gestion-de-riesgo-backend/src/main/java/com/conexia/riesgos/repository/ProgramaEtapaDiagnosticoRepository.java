package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaEtapaDiagnostico;

@Repository
public interface ProgramaEtapaDiagnosticoRepository extends JpaRepository<ProgramaEtapaDiagnostico, Long> {

	
}
