package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaPoblacionObjetoAreaGeografica;

@Repository
public interface ProgramaPoblacionObjetoAreaGeograficaRepository extends JpaRepository<ProgramaPoblacionObjetoAreaGeografica, Long> {

	
}
