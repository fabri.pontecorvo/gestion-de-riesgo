package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaEmpadronamientoBeneficiario;

@Repository
public interface ProgramaEmpadronamientoBeneficiarioRepository extends JpaRepository<ProgramaEmpadronamientoBeneficiario, Long> {

	
}
