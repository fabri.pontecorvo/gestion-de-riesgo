package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaEmpadronamientoBeneficiarioDatoSuplementario;

@Repository
public interface ProgramaEmpadronamientoBeneficiarioDatoSuplementarioRepository extends JpaRepository<ProgramaEmpadronamientoBeneficiarioDatoSuplementario, Long> {

	
}
