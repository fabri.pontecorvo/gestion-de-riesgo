package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.EmpadronamientoAccion;

@Repository
public interface EmpadronamientoAccionRepository extends JpaRepository<EmpadronamientoAccion, Long> {

	
}
