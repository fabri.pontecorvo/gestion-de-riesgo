package com.conexia.riesgos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.ProgramaEtapa;

@Repository
public interface ProgramaEtapaRepository extends JpaRepository<ProgramaEtapa, Long> {

	
}
