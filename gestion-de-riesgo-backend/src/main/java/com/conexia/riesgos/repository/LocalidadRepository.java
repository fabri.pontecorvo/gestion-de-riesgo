package com.conexia.riesgos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conexia.riesgos.model.Localidad;

@Repository
public interface LocalidadRepository extends JpaRepository<Localidad, Long> {
	
	public List<Localidad> findByProvinciaCodigo (String provinciaCodigo);
	

	
}
