package com.conexia.riesgos.config;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = -7858869558953243875L;
	private static final String FAIL_AUTHENTICATION = "Full authentication is required to access this resource";

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		if (FAIL_AUTHENTICATION.equals(authException.getMessage()))
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "Acceso Denegado");
		else
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "No Autorizado");

	}
}
