package com.conexia.riesgos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

@Configuration
public class FreemarkerConfig {
	/**
	 * Este bean instancia el freemarker
	 */
	@Bean
	public FreeMarkerConfigurationFactoryBean freeMarkerConfiguration() {
		FreeMarkerConfigurationFactoryBean freeMarkerFactoryBean = new FreeMarkerConfigurationFactoryBean();
		freeMarkerFactoryBean.setTemplateLoaderPaths("classpath:/templates");
		freeMarkerFactoryBean.setPreferFileSystemAccess(true);
		freeMarkerFactoryBean.setDefaultEncoding("UTF-8");
		return freeMarkerFactoryBean;
	}
}
