package com.conexia.riesgos.config;

public class ApplicationWebSettings {

	public static final String MAIL_USER = "smtpcomei@conexia.com.ar";
	public static final String MAIL_PASSWORD = "";
	public static final String MAIL_IP = "relay.cx.ar";
	public static final String MAIL_PORT = "25";
	public static final String MAIL_PROTOCOL = "smtp";
	public static final String MAIL_TIPO_CONTENIDO = "text/html; charset=UTF-8";
	public static final String MAIL_ENVIADO_POR = "smtpcomei@conexia.com.ar";
	public static final String MAIL_REPLY_TO = "no-reply@conexia.com.ar";
	
	private ApplicationWebSettings() {
	}
}
