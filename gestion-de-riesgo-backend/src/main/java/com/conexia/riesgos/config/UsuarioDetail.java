package com.conexia.riesgos.config;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UsuarioDetail extends User {

	private static final long serialVersionUID = -8672185825638397048L;

	public UsuarioDetail(String username, String password, Collection<? extends GrantedAuthority> authorities,
			Boolean enabled) {
		super(username, password, enabled, true, true, true, authorities);
	}

}
