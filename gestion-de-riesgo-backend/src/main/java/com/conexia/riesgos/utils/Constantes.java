package com.conexia.riesgos.utils;

public class Constantes {
	public final static String GENERO_MASCULINO = "M";
	public final static String GENERO_FEMENINO = "F";
	public final static String GENERO_AMBOS = "A";
	
	public final static Integer SIN_VALOR_INT = -1;
	public final static String SIN_VALOR_STRING = "-1";

}
