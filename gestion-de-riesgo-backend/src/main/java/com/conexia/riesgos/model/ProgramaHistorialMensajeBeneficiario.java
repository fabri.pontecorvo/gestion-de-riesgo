package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_etapa_profesional", schema = "gestion_riesgo")
public class ProgramaHistorialMensajeBeneficiario extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_etapa_id")
	private ProgramaEtapa programaEtapa;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "profesinal_id")
	private Profesional profesional;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "profesional_especialidad_medica_id")
	private ProfesionalEspecialidadMedica profesionalEspeciliadadMedica;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEtapa getProgramaEtapa() {
		return programaEtapa;
	}

	public void setProgramaEtapa(ProgramaEtapa programaEtapa) {
		this.programaEtapa = programaEtapa;
	}

	public Profesional getProfesional() {
		return profesional;
	}

	public void setProfesional(Profesional profesional) {
		this.profesional = profesional;
	}

	public ProfesionalEspecialidadMedica getProfesionalEspeciliadadMedica() {
		return profesionalEspeciliadadMedica;
	}

	public void setProfesionalEspeciliadadMedica(ProfesionalEspecialidadMedica profesionalEspeciliadadMedica) {
		this.profesionalEspeciliadadMedica = profesionalEspeciliadadMedica;
	}

}