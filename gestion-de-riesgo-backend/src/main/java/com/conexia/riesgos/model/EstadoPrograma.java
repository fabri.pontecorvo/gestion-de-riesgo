package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "estado_programa", schema = "gestion_riesgo")
public class EstadoPrograma extends CodigoDescripcionEntity {

	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EstadoPrograma(Long id) {
		super();
		this.id = id;
	}

	public EstadoPrograma() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}