package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_poblacion_objeto_especial", schema = "gestion_riesgo")
public class ProgramaPoblacionObjetoEspecial extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_id")
	private Programa programa;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "provincia_id")
	private GrupoPoblacional grupoPoblacional;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public GrupoPoblacional getGrupoPoblacional() {
		return grupoPoblacional;
	}

	public void setGrupoPoblacional(GrupoPoblacional grupoPoblacional) {
		this.grupoPoblacional = grupoPoblacional;
	}
}

