package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_etapa_diagnostico", schema = "gestion_riesgo")
public class ProgramaEtapaDiagnostico extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_etapa_id")
	private ProgramaEtapa programaEtapa;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "diagnostico_id")
	private Diagnostico diagnostico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEtapa getProgramaEtapa() {
		return programaEtapa;
	}

	public void setProgramaEtapa(ProgramaEtapa programaEtapa) {
		this.programaEtapa = programaEtapa;
	}

	public Diagnostico getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(Diagnostico diagnostico) {
		this.diagnostico = diagnostico;
	}
	
	
	
		

}