package com.conexia.riesgos.model.generic;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;

@MappedSuperclass
public class FechaEntity {
	@Column(name = "fecha_insert", insertable=false, updatable=false )
	@Type(type="timestamp")
	private Date fechaInsert;	
	
	@Column(name = "fecha_update")
	@Type(type="timestamp")
	private Date fechaUpdate;
	
	@Column(name = "fecha_delete")
	@Type(type="timestamp")
	private Date fechaDelete;
	
	@Column(name = "deleted")
	private Boolean deleted;

	public Date getFechaInsert() {
		return fechaInsert;
	}

	public void setFechaInsert(Date fechaInsert) {
		this.fechaInsert = fechaInsert;
	}

	public Date getFechaUpdate() {
		return fechaUpdate;
	}

	public void setFechaUpdate(Date fechaUpdate) {
		this.fechaUpdate = fechaUpdate;
	}

	public Date getFechaDelete() {
		return fechaDelete;
	}

	public void setFechaDelete(Date fechaDelete) {
		this.fechaDelete = fechaDelete;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	

}
