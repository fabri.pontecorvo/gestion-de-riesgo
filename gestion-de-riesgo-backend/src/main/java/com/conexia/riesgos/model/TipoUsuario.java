package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "tipo_usuario", schema = "seguridad")
public class TipoUsuario extends CodigoDescripcionEntity {

	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoUsuario(Long id) {
		super();
		this.id = id;
	}

	public TipoUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}