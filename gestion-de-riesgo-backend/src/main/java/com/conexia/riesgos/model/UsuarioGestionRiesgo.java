package com.conexia.riesgos.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "usuario_gestion_riesgo", schema = "seguridad")
public class UsuarioGestionRiesgo extends FechaEntity {

	@Id
	private Long id;
	
	@Column(name = "usuario")
	private String usuario;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "nombreApellido")
	private String nombreApellido;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "email")
	private String email;	
	
	@Column(name = "fecha_alta")
	private LocalDateTime fechaAlta;
	
	@Column(name = "fecha_baja")
	private LocalDateTime fechaBaja;
	
	@Column(name = "fecha_cambio_clave")
	private LocalDateTime fechaCambioClave;
	
	@Column(name = "activo")
	private Integer activo;
	
	@Column(name = "clave_recuperacion")
	private String claveRecuperacion;
	
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "tipo_usuario_id")
	private TipoUsuario tipoUsuario;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getNombreApellido() {
		return nombreApellido;
	}


	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public LocalDateTime getFechaAlta() {
		return fechaAlta;
	}


	public void setFechaAlta(LocalDateTime fechaAlta) {
		this.fechaAlta = fechaAlta;
	}


	public LocalDateTime getFechaBaja() {
		return fechaBaja;
	}


	public void setFechaBaja(LocalDateTime fechaBaja) {
		this.fechaBaja = fechaBaja;
	}


	public LocalDateTime getFechaCambioClave() {
		return fechaCambioClave;
	}


	public void setFechaCambioClave(LocalDateTime fechaCambioClave) {
		this.fechaCambioClave = fechaCambioClave;
	}


	public Integer getActivo() {
		return activo;
	}


	public void setActivo(Integer activo) {
		this.activo = activo;
	}


	public String getClaveRecuperacion() {
		return claveRecuperacion;
	}


	public void setClaveRecuperacion(String claveRecuperacion) {
		this.claveRecuperacion = claveRecuperacion;
	}


	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}


	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
		
}