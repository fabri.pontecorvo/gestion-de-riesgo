package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_empadronamiento_documentacion", schema = "gestion_riesgo")
public class ProgramaEmpadronamientoDocumentacion extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_id")
	private Programa programa;
	
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "empadronamiento_documentacion_id")
	private EmpadronamientoDocumentacion empadronamientoDocumentacion;
	
	@Column (name = "renovacion_cantidad")
	private Integer renovacionCantidad;
	
	@Column (name = "renovacion_periodo")
	private Integer renovacioPeriodo;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "renovacion_periodo_unidad_tiempo_id")
	private UnidadTiempo renovacionPeriodoUnidadTiempo;
	
	@Column (name = "es_reempadronamiento")
	private Boolean esReempadronamiento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public EmpadronamientoDocumentacion getEmpadronamientoDocumentacion() {
		return empadronamientoDocumentacion;
	}

	public void setEmpadronamientoDocumentacion(EmpadronamientoDocumentacion empadronamientoDocumentacion) {
		this.empadronamientoDocumentacion = empadronamientoDocumentacion;
	}

	public Integer getRenovacionCantidad() {
		return renovacionCantidad;
	}

	public void setRenovacionCantidad(Integer renovacionCantidad) {
		this.renovacionCantidad = renovacionCantidad;
	}

	public Integer getRenovacioPeriodo() {
		return renovacioPeriodo;
	}

	public void setRenovacioPeriodo(Integer renovacioPeriodo) {
		this.renovacioPeriodo = renovacioPeriodo;
	}

	public UnidadTiempo getRenovacionPeriodoUnidadTiempo() {
		return renovacionPeriodoUnidadTiempo;
	}

	public void setRenovacionPeriodoUnidadTiempo(UnidadTiempo renovacionPeriodoUnidadTiempo) {
		this.renovacionPeriodoUnidadTiempo = renovacionPeriodoUnidadTiempo;
	}

	public Boolean getEsReempadronamiento() {
		return esReempadronamiento;
	}

	public void setEsReempadronamiento(Boolean esReempadronamiento) {
		this.esReempadronamiento = esReempadronamiento;
	}
	
	
}

