package com.conexia.riesgos.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "programa", schema = "gestion_riesgo")
public class Programa extends CodigoDescripcionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (name = "fecha_desde")
	private LocalDate fechaDesde;
	
	@Column (name = "fecha_hasta")
	private LocalDate fechaHasta;	
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "empadronamiento_condiciones_id")
	private EmpadronamientoCondicion empadronamientoCondicion;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_id")
	private Programa programa;
	
	@Column (name = "edad_desde")
	private Integer edadDesde;
	
	@Column (name = "edad_hasta")
	private Integer edadHasta;	
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "unidad_tiempo_edad_id")
	private UnidadTiempo unidadTiempoEdad;
	
	@Column (name = "vigencia_cantidad")
	private Integer vigenciaCantidad;
	
	@Column (name = "vigencia_periodo")
	private Integer vigenciaPeriodo;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "vigencia_periodo_unidad_tiempo_id")
	private UnidadTiempo vigenciaPeriodoUnidadTiempo;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "tipo_empadronamiento_id")
	private TipoEmpadronamiento tipoEmpadronamiento;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "estado_programa_id")
	private EstadoPrograma estadoPrograma;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EmpadronamientoCondicion getEmpadronamientoCondicion() {
		return empadronamientoCondicion;
	}

	public void setEmpadronamientoCondicion(EmpadronamientoCondicion empadronamientoCondicion) {
		this.empadronamientoCondicion = empadronamientoCondicion;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public Integer getEdadDesde() {
		return edadDesde;
	}

	public void setEdadDesde(Integer edadDesde) {
		this.edadDesde = edadDesde;
	}

	public Integer getEdadHasta() {
		return edadHasta;
	}

	public void setEdadHasta(Integer edadHasta) {
		this.edadHasta = edadHasta;
	}

	public UnidadTiempo getUnidadTiempoEdad() {
		return unidadTiempoEdad;
	}

	public void setUnidadTiempoEdad(UnidadTiempo unidadTiempoEdad) {
		this.unidadTiempoEdad = unidadTiempoEdad;
	}

	public Integer getVigenciaCantidad() {
		return vigenciaCantidad;
	}

	public void setVigenciaCantidad(Integer vigenciaCantidad) {
		this.vigenciaCantidad = vigenciaCantidad;
	}

	public Integer getVigenciaPeriodo() {
		return vigenciaPeriodo;
	}

	public void setVigenciaPeriodo(Integer vigenciaPeriodo) {
		this.vigenciaPeriodo = vigenciaPeriodo;
	}

	public UnidadTiempo getVigenciaPeriodoUnidadTiempo() {
		return vigenciaPeriodoUnidadTiempo;
	}

	public void setVigenciaPeriodoUnidadTiempo(UnidadTiempo vigenciaPeriodoUnidadTiempo) {
		this.vigenciaPeriodoUnidadTiempo = vigenciaPeriodoUnidadTiempo;
	}

	public TipoEmpadronamiento getTipoEmpadronamiento() {
		return tipoEmpadronamiento;
	}

	public void setTipoEmpadronamiento(TipoEmpadronamiento tipoEmpadronamiento) {
		this.tipoEmpadronamiento = tipoEmpadronamiento;
	}

	public EstadoPrograma getEstadoPrograma() {
		return estadoPrograma;
	}

	public void setEstadoPrograma(EstadoPrograma estadoPrograma) {
		this.estadoPrograma = estadoPrograma;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}	
	

}