package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "estado_auditoria", schema = "gestion_riesgo")
public class EstadoAuditoria extends FechaEntity {

	@Id
	private Long id;
	
	@Column(name = "codigo")
	private String codigo;
	
	@Column(name = "mensaje")
	private String mensaje;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "abreviatura")
	private String abreviatura;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

}