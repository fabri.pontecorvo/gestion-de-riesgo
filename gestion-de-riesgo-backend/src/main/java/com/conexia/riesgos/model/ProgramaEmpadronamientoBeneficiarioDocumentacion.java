package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_empadronamiento_beneficiario_documentacion", schema = "gestion_riesgo")
public class ProgramaEmpadronamientoBeneficiarioDocumentacion extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_empadronamiento_beneficiario_id")
	private ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "empadronamiento_documentacion_id")
	private EmpadronamientoDocumentacion empadronamientoDocumentacion;
	
	@Column(name = "url_adjunto") 
	private String urlAdjunto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEmpadronamientoBeneficiario getProgramaEmpadronamientoBeneficiario() {
		return programaEmpadronamientoBeneficiario;
	}

	public void setProgramaEmpadronamientoBeneficiario(
			ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario) {
		this.programaEmpadronamientoBeneficiario = programaEmpadronamientoBeneficiario;
	}

	public EmpadronamientoDocumentacion getEmpadronamientoDocumentacion() {
		return empadronamientoDocumentacion;
	}

	public void setEmpadronamientoDocumentacion(EmpadronamientoDocumentacion empadronamientoDocumentacion) {
		this.empadronamientoDocumentacion = empadronamientoDocumentacion;
	}

	public String getUrlAdjunto() {
		return urlAdjunto;
	}

	public void setUrlAdjunto(String urlAdjunto) {
		this.urlAdjunto = urlAdjunto;
	}

	
	
	
}

