package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_etapa_diagnostico", schema = "gestion_riesgo")
public class ProgramaEtapaProcedimiento extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_etapa_id")
	private ProgramaEtapa programaEtapa;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "procedimiento_id")
	private Procedimiento procedimiento;

	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "ambito_id")
	private Ambito ambito;

	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "especialidad_medica_id")
	private EspecialidadMedica especilidadMedica;
	
	@Column(name = "tope_cantidad")
	private Integer topeCantidad;
	
	@Column(name = "tope_periodo")
	private Integer topePeriodo;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "tope_periodo_unidad_tiempo_id")
	private UnidadTiempo topePeriodoUnidadTiempo;
	
	@Column(name = "es_reempadronamiento")
	private Integer esReempadronamiento;
	
	@Column(name = "es_prescriptor")
	private Integer esPrescriptor;
	
	@Column(name = "es_efector")
	private Integer esEfector;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEtapa getProgramaEtapa() {
		return programaEtapa;
	}

	public void setProgramaEtapa(ProgramaEtapa programaEtapa) {
		this.programaEtapa = programaEtapa;
	}

	public Procedimiento getProcedimiento() {
		return procedimiento;
	}

	public void setProcedimiento(Procedimiento procedimiento) {
		this.procedimiento = procedimiento;
	}

	public Ambito getAmbito() {
		return ambito;
	}

	public void setAmbito(Ambito ambito) {
		this.ambito = ambito;
	}

	public EspecialidadMedica getEspecilidadMedica() {
		return especilidadMedica;
	}

	public void setEspecilidadMedica(EspecialidadMedica especilidadMedica) {
		this.especilidadMedica = especilidadMedica;
	}

	public Integer getTopeCantidad() {
		return topeCantidad;
	}

	public void setTopeCantidad(Integer topeCantidad) {
		this.topeCantidad = topeCantidad;
	}

	public Integer getTopePeriodo() {
		return topePeriodo;
	}

	public void setTopePeriodo(Integer topePeriodo) {
		this.topePeriodo = topePeriodo;
	}

	public UnidadTiempo getTopePeriodoUnidadTiempo() {
		return topePeriodoUnidadTiempo;
	}

	public void setTopePeriodoUnidadTiempo(UnidadTiempo topePeriodoUnidadTiempo) {
		this.topePeriodoUnidadTiempo = topePeriodoUnidadTiempo;
	}

	public Integer getEsReempadronamiento() {
		return esReempadronamiento;
	}

	public void setEsReempadronamiento(Integer esReempadronamiento) {
		this.esReempadronamiento = esReempadronamiento;
	}

	public Integer getEsPrescriptor() {
		return esPrescriptor;
	}

	public void setEsPrescriptor(Integer esPrescriptor) {
		this.esPrescriptor = esPrescriptor;
	}

	public Integer getEsEfector() {
		return esEfector;
	}

	public void setEsEfector(Integer esEfector) {
		this.esEfector = esEfector;
	}

}