package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "beneficiario", schema = "maestro")
public class ProfesionalEspecialidadMedica extends FechaEntity {

	@Id
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "profesional_id")
	private Profesional profesional;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "especialidad_medica_id")
	private EspecialidadMedica especialidadMedica;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Profesional getProfesional() {
		return profesional;
	}

	public void setProfesional(Profesional profesional) {
		this.profesional = profesional;
	}

	public EspecialidadMedica getEspecialidadMedica() {
		return especialidadMedica;
	}

	public void setEspecialidadMedica(EspecialidadMedica especialidadMedica) {
		this.especialidadMedica = especialidadMedica;
	}
	
}