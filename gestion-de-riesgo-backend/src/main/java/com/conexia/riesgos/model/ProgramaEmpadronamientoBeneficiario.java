package com.conexia.riesgos.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_empadronamiento_beneficiario", schema = "gestion_riesgo")
public class ProgramaEmpadronamientoBeneficiario extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "beneficiario_id")
	private Beneficiario beneficiario;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "tipo_pariente_adherente_id")
	private TipoPariente tipoParienteAdherente;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "beneficiario_adherente_id")
	private Beneficiario beneficiarioAdherente;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_id")
	private Programa programa;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_etapa_id")	
	private ProgramaEtapa programaEtapa;
	
	@Column (name = "fecha_inicio")
	private LocalDate fechaInicio;
	
	@Column (name = "antedendentes")
	private String antecedente;

	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "estado_auditoria_id")
	private EstadoAuditoria estadoAuditoria;
	
	@Column (name = "estado_auditoria_codigo")
	private String estadoAuditoriaCodigo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}

	public TipoPariente getTipoParienteAdherente() {
		return tipoParienteAdherente;
	}

	public void setTipoParienteAdherente(TipoPariente tipoParienteAdherente) {
		this.tipoParienteAdherente = tipoParienteAdherente;
	}

	public Beneficiario getBeneficiarioAdherente() {
		return beneficiarioAdherente;
	}

	public void setBeneficiarioAdherente(Beneficiario beneficiarioAdherente) {
		this.beneficiarioAdherente = beneficiarioAdherente;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public ProgramaEtapa getProgramaEtapa() {
		return programaEtapa;
	}

	public void setProgramaEtapa(ProgramaEtapa programaEtapa) {
		this.programaEtapa = programaEtapa;
	}

	public LocalDate getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(LocalDate fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getAntecedente() {
		return antecedente;
	}

	public void setAntecedente(String antecedente) {
		this.antecedente = antecedente;
	}

	public EstadoAuditoria getEstadoAuditoria() {
		return estadoAuditoria;
	}

	public void setEstadoAuditoria(EstadoAuditoria estadoAuditoria) {
		this.estadoAuditoria = estadoAuditoria;
	}

	public String getEstadoAuditoriaCodigo() {
		return estadoAuditoriaCodigo;
	}

	public void setEstadoAuditoriaCodigo(String estadoAuditoriaCodigo) {
		this.estadoAuditoriaCodigo = estadoAuditoriaCodigo;
	}
}

