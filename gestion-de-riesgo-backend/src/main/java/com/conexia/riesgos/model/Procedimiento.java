package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "procedimiento", schema = "maestro")
public class Procedimiento extends CodigoDescripcionEntity {

	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Procedimiento(Long id) {
		super();
		this.id = id;
	}

	public Procedimiento() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}