package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_empadronamiento_beneficiario_accion", schema = "gestion_riesgo")
public class ProgramaEmpadronamientoBeneficiarioAccion extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "beneficiario_id")
	private ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "empadronamiento_accion_id")
	private EmpadronamientoAccion empadronamientoAccion;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "empadronamiento_motivo_id")
	private EmpadronamientoMotivo empadronamientoMotivo;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "usuario_gestion_riesgo")
	private UsuarioGestionRiesgo usuarioGestionRiesgo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEmpadronamientoBeneficiario getProgramaEmpadronamientoBeneficiario() {
		return programaEmpadronamientoBeneficiario;
	}

	public void setProgramaEmpadronamientoBeneficiario(
			ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario) {
		this.programaEmpadronamientoBeneficiario = programaEmpadronamientoBeneficiario;
	}

	public EmpadronamientoAccion getEmpadronamientoAccion() {
		return empadronamientoAccion;
	}

	public void setEmpadronamientoAccion(EmpadronamientoAccion empadronamientoAccion) {
		this.empadronamientoAccion = empadronamientoAccion;
	}

	public EmpadronamientoMotivo getEmpadronamientoMotivo() {
		return empadronamientoMotivo;
	}

	public void setEmpadronamientoMotivo(EmpadronamientoMotivo empadronamientoMotivo) {
		this.empadronamientoMotivo = empadronamientoMotivo;
	}

	public UsuarioGestionRiesgo getUsuarioGestionRiesgo() {
		return usuarioGestionRiesgo;
	}

	public void setUsuarioGestionRiesgo(UsuarioGestionRiesgo usuarioGestionRiesgo) {
		this.usuarioGestionRiesgo = usuarioGestionRiesgo;
	}
	
}

