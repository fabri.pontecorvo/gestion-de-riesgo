package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "empadronamiento_accion", schema = "gestion_riesgo")
public class EmpadronamientoAccion extends CodigoDescripcionEntity {

	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}