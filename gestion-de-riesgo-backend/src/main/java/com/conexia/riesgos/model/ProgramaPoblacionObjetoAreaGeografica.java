package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_poblacion_objeto_area_geografica", schema = "gestion_riesgo")
public class ProgramaPoblacionObjetoAreaGeografica extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_id")
	private Programa programa;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "provincia_id")
	private Provincia provincia;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "localidad_id")
	private Localidad localidad;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}
	

	
}

