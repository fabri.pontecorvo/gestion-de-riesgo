package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_empadronamiento_beneficiario_historial_mensajes", schema = "gestion_riesgo")
public class ProgramaEmpadronamientoBeneficiarioHistorialMensaje extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_empadronamiento_beneficiario_id")
	private ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario;
	
	@Column(name = "comentario") 
	private String comentario;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "usuario_gestion_riesgo_id")
	private UsuarioGestionRiesgo usuarioGestionRiesgo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEmpadronamientoBeneficiario getProgramaEmpadronamientoBeneficiario() {
		return programaEmpadronamientoBeneficiario;
	}

	public void setProgramaEmpadronamientoBeneficiario(
			ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario) {
		this.programaEmpadronamientoBeneficiario = programaEmpadronamientoBeneficiario;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public UsuarioGestionRiesgo getUsuarioGestionRiesgo() {
		return usuarioGestionRiesgo;
	}

	public void setUsuarioGestionRiesgo(UsuarioGestionRiesgo usuarioGestionRiesgo) {
		this.usuarioGestionRiesgo = usuarioGestionRiesgo;
	}
}

