package com.conexia.riesgos.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "especialidad_medica", schema = "maestro")
public class EspecialidadMedica extends CodigoDescripcionEntity {

	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EspecialidadMedica(Long id) {
		super();
		this.id = id;
	}

	public EspecialidadMedica() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}