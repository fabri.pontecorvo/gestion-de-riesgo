package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_etapa", schema = "gestion_riesgo")
public class ProgramaEtapa extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_id")
	private Programa programa;

	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "por_profesional")
	private Boolean porProfesional;
	
	@Column(name = "es_automatico")
	private Boolean esAutomatico;
	
	@Column(name = "tope_periodo")
	private Integer topePeriodo;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "tope_periodo_unidad_tiempo_id")
	private UnidadTiempo topePeriodoUnidadTiempo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getPorProfesional() {
		return porProfesional;
	}

	public void setPorProfesional(Boolean porProfesional) {
		this.porProfesional = porProfesional;
	}

	public Boolean getEsAutomatico() {
		return esAutomatico;
	}

	public void setEsAutomatico(Boolean esAutomatico) {
		this.esAutomatico = esAutomatico;
	}

	public Integer getTopePeriodo() {
		return topePeriodo;
	}

	public void setTopePeriodo(Integer topePeriodo) {
		this.topePeriodo = topePeriodo;
	}

	public UnidadTiempo getTopePeriodoUnidadTiempo() {
		return topePeriodoUnidadTiempo;
	}

	public void setTopePeriodoUnidadTiempo(UnidadTiempo topePeriodoUnidadTiempo) {
		this.topePeriodoUnidadTiempo = topePeriodoUnidadTiempo;
	}
}