package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "rango_etario", schema = "gestion_riesgo")
public class RangoEtario extends FechaEntity {

	@Id
	private Long id;
	
	@Column(name = "codigo")
	private String codigo;
	
	@Column (name = "edad_desde")
	private Integer edadDesde;
	
	@Column (name = "edad_hasta")
	private Integer edadHasta;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "unidad_tiempo_edad_id")
	private UnidadTiempo unidadTiempo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getEdadDesde() {
		return edadDesde;
	}

	public void setEdadDesde(Integer edadDesde) {
		this.edadDesde = edadDesde;
	}

	public Integer getEdadHasta() {
		return edadHasta;
	}

	public void setEdadHasta(Integer edadHasta) {
		this.edadHasta = edadHasta;
	}

	public UnidadTiempo getUnidadTiempo() {
		return unidadTiempo;
	}

	public void setUnidadTiempo(UnidadTiempo unidadTiempo) {
		this.unidadTiempo = unidadTiempo;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	

}