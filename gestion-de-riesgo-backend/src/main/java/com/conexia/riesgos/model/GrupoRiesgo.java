package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.CodigoDescripcionEntity;

@Entity
@Table(name = "grupo_riesgo", schema = "gestion_riesgo")
public class GrupoRiesgo extends CodigoDescripcionEntity {

	@Id
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "genero_id")
	private Genero genero;
	
	@Column (name = "edad_desde")
	private Integer edadDesde;
	
	@Column (name = "edad_hasta")
	private Integer edadHasta;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "unidad_tiempo_edad_id")
	private UnidadTiempo unidadTiempo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Integer getEdadDesde() {
		return edadDesde;
	}

	public void setEdadDesde(Integer edadDesde) {
		this.edadDesde = edadDesde;
	}

	public Integer getEdadHasta() {
		return edadHasta;
	}

	public void setEdadHasta(Integer edadHasta) {
		this.edadHasta = edadHasta;
	}

	public UnidadTiempo getUnidadTiempo() {
		return unidadTiempo;
	}

	public void setUnidadTiempo(UnidadTiempo unidadTiempo) {
		this.unidadTiempo = unidadTiempo;
	}

}