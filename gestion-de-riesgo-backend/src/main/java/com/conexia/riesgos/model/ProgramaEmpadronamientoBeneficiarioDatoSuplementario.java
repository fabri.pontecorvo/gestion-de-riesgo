package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "programa_empadronamiento_beneficiario_datos_suplementarios", schema = "gestion_riesgo")
public class ProgramaEmpadronamientoBeneficiarioDatoSuplementario extends FechaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_empadronamiento_beneficiario_id")
	private ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario;
	
	@OneToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name = "programa_empadronamiento_datos_suplementario_id")
	private ProgramaEmpadronamientoDatoSuplementario programaEmpadronamientoDatoSuplementario;
	
	@Column(name = "observaciones") 
	private String observacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProgramaEmpadronamientoBeneficiario getProgramaEmpadronamientoBeneficiario() {
		return programaEmpadronamientoBeneficiario;
	}

	public void setProgramaEmpadronamientoBeneficiario(
			ProgramaEmpadronamientoBeneficiario programaEmpadronamientoBeneficiario) {
		this.programaEmpadronamientoBeneficiario = programaEmpadronamientoBeneficiario;
	}

	public ProgramaEmpadronamientoDatoSuplementario getProgramaEmpadronamientoDatoSuplementario() {
		return programaEmpadronamientoDatoSuplementario;
	}

	public void setProgramaEmpadronamientoDatoSuplementario(
			ProgramaEmpadronamientoDatoSuplementario programaEmpadronamientoDatoSuplementario) {
		this.programaEmpadronamientoDatoSuplementario = programaEmpadronamientoDatoSuplementario;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	
	
}

