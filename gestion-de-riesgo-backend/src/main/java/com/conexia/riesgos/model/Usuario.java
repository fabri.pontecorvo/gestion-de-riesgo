package com.conexia.riesgos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.conexia.riesgos.model.generic.FechaEntity;

@Entity
@Table(name = "usuario_gestion_riesgo", schema = "seguridad")
public class Usuario extends FechaEntity {

	@Id
	@GeneratedValue(generator = "usuario_gestion_riesgo_id_seq", strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "seguridad.usuario_gestion_riesgo_id_seq", sequenceName = "seguridad.usuario_gestion_riesgo_id_seq")
	private Long id;

	@Column(name = "usuario")
	private String usuario;

	@Column(name = "password")
	private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", usuario=" + usuario + ", password=" + password + "]";
	}

}