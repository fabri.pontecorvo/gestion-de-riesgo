package com.conexia.riesgos.controller;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexia.riesgos.config.JwtTokenUtil;
import com.conexia.riesgos.dto.request.JwtRequestDto;
import com.conexia.riesgos.dto.response.JwtResponseDto;
import com.conexia.riesgos.service.CaptchaService;

@RequestMapping("/authenticate")
@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	private final Log logger = LogFactory.getLog(getClass());

	private static final String ATTRIBUTE_PATH = "org.springframework.web.servlet.HandlerMapping.pathWithinHandlerMapping";
	private static final String USER_DISABLED = "USER_DISABLED";
	private static final String INVALID_CREDENTIALS = "INVALID_CREDENTIALS";
	private static final String INVALID_CAPTCHA = "INVALID_CAPTCHA";

	@Autowired
	private CaptchaService captchaService;

	@PostMapping({ "/login", "/refresh" })
	public ResponseEntity<JwtResponseDto> createAuthenticationToken(HttpServletRequest request,
			@RequestBody JwtRequestDto authenticationRequest) throws Exception {

		Boolean captchaVerified = true;
		if (!authenticationRequest.getCaptchaError() && authenticationRequest.getRecaptchaResponse() != null
				&& !authenticationRequest.getRecaptchaResponse().equals("")) {
			captchaVerified = captchaService.verify(authenticationRequest.getRecaptchaResponse());
			logger.info("CAPTCHA VALIDO: >>>>>>" + captchaVerified);
		}

		if (captchaVerified) {
			logger.info(request.getAttribute(JwtAuthenticationController.ATTRIBUTE_PATH) + " "
					+ authenticationRequest.getUsername());

			final UserDetails userDetails = authenticate(authenticationRequest.getUsername(),
					authenticationRequest.getPassword());

			final String token = jwtTokenUtil.generateToken(userDetails);

			return ResponseEntity.ok(new JwtResponseDto(token));
		} else {
			return new ResponseEntity<>(new JwtResponseDto(JwtAuthenticationController.INVALID_CAPTCHA),
					HttpStatus.BAD_REQUEST);
		}

	}

	private UserDetails authenticate(String username, String password) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			return (UserDetails) auth.getPrincipal();
		} catch (DisabledException e) {
			throw new DisabledException(JwtAuthenticationController.USER_DISABLED, e);
		} catch (BadCredentialsException e) {
			throw new BadCredentialsException(JwtAuthenticationController.INVALID_CREDENTIALS, e);
		}
	}
}