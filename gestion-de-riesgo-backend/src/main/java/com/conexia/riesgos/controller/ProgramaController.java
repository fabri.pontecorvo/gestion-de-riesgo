package com.conexia.riesgos.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;



@RestController
@CrossOrigin("*")
@Api(tags = { "Programa" })
public class ProgramaController implements AbstractRestController {
	
	
}
