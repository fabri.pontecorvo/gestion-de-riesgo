package com.conexia.riesgos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexia.riesgos.dto.response.ComboDto;
import com.conexia.riesgos.dto.response.InitDto;
import com.conexia.riesgos.service.ComboService;

import io.swagger.annotations.Api;



@RestController
@CrossOrigin("*")
@Api(tags = { "Init" })
public class ComboController implements AbstractRestController {
	
	@Autowired
	ComboService comboService;	
	
    @GetMapping("/init")
    public ResponseEntity<InitDto> getInit() throws Exception {
    	
    	InitDto init = comboService.getAll();

    	return new ResponseEntity<>(init, HttpStatus.OK);
    }

    @GetMapping("/tipoPariente")
    public 	List<ComboDto> getTipoPariente() throws Exception {
    	
    	List<ComboDto> list  = comboService.getTiposParientes();

    	return list;
    }

    
}
