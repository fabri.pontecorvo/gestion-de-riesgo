package com.conexia.riesgos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.conexia.riesgos.dto.request.PoblacionObjetoFiltroDto;
import com.conexia.riesgos.dto.response.PoblacionObjetoDto;
import com.conexia.riesgos.service.PoblacionObjetoService;

import io.swagger.annotations.Api;



@RestController
@CrossOrigin("*")
@Api(tags = { "Poblacion Objeto" })
public class PoblacionObjetoController implements AbstractRestController {
	
	@Autowired
	PoblacionObjetoService poblacionObjetoService;
	
    @PostMapping("/poblacion-objeto")
    public ResponseEntity<PoblacionObjetoDto> getInformacion(@RequestBody List<PoblacionObjetoFiltroDto> request) throws Exception {
    	
    	PoblacionObjetoDto obj = poblacionObjetoService.getDatos(request);
    	
    	return new ResponseEntity<>(obj, HttpStatus.OK);
    }
	

}
