package com.conexia.riesgos.storedprocedure;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

@NamedNativeQueries(value = {
		@NamedNativeQuery(name = "UsuarioStoredProcedureEntity.getUsuarios", query = "select * from transaccional.get_usuarios(:abreviatura,:id,:usuario)", resultClass = UsuarioStoredProcedureEntity.class) })

@Entity
public class UsuarioStoredProcedureEntity {
	@Id
	private BigInteger id;
	private String usuario;
	private String password;
	private String nombreApellido;
	private String telefono;
	private String email;
	@Temporal(TemporalType.DATE)
	private Date fechaAlta;
	@Temporal(TemporalType.DATE)
	private Date fechaBaja;
	@Temporal(TemporalType.DATE)
	private Date fechaCambioClave;
	private Integer activo;
	private String claveRecuperacion;
	private BigInteger idBeneficiario;
	@Temporal(TemporalType.DATE)
	private Date fechaInsert;
	@Temporal(TemporalType.DATE)
	private Date fechaUpdate;
	@Temporal(TemporalType.DATE)
	private Date fechaDelete;
	private String roles;
	
	public String getRoles() {
		return roles;
	}
	
	public void setRoles(String roles) {
		this.roles = roles;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public String getClaveRecuperacion() {
		return claveRecuperacion;
	}

	public void setClaveRecuperacion(String claveRecuperacion) {
		this.claveRecuperacion = claveRecuperacion;
	}

	public BigInteger getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(BigInteger idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Date getFechaCambioClave() {
		return fechaCambioClave;
	}

	public void setFechaCambioClave(Date fechaCambioClave) {
		this.fechaCambioClave = fechaCambioClave;
	}

	public Date getFechaInsert() {
		return fechaInsert;
	}

	public void setFechaInsert(Date fechaInsert) {
		this.fechaInsert = fechaInsert;
	}

	public Date getFechaUpdate() {
		return fechaUpdate;
	}

	public void setFechaUpdate(Date fechaUpdate) {
		this.fechaUpdate = fechaUpdate;
	}

	public Date getFechaDelete() {
		return fechaDelete;
	}

	public void setFechaDelete(Date fechaDelete) {
		this.fechaDelete = fechaDelete;
	}

	public UsuarioStoredProcedureEntity(BigInteger id, String usuario, String password, String nombreApellido,
			String telefono, String email, Date fechaAlta, Date fechaBaja, Date fechaCambioClave, Integer activo,
			String claveRecuperacion, BigInteger idBeneficiario, Date fechaInsert, Date fechaUpdate, Date fechaDelete,String roles) {
		this.id = id;
		this.usuario = usuario;
		this.password = password;
		this.nombreApellido = nombreApellido;
		this.telefono = telefono;
		this.email = email;
		this.fechaAlta = fechaAlta;
		this.fechaBaja = fechaBaja;
		this.fechaCambioClave = fechaCambioClave;
		this.activo = activo;
		this.claveRecuperacion = claveRecuperacion;
		this.idBeneficiario = idBeneficiario;
		this.fechaInsert = fechaInsert;
		this.fechaUpdate = fechaUpdate;
		this.fechaDelete = fechaDelete;
		this.roles = roles;
	}

	public UsuarioStoredProcedureEntity() {

	}
}
