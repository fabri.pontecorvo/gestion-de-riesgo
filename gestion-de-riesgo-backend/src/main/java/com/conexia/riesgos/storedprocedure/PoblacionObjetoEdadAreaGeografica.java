package com.conexia.riesgos.storedprocedure;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

@NamedNativeQueries(value = {
		@NamedNativeQuery(name = "PoblacionObjetoEdadAreaGeografica.findByFilters", query = "select * from gestion_riesgo.get_poblacion_objeto_por_edad_area_geografica(:codGenero, :edadDesde,:edadHasta,:codLocadad,:codProvincia)", resultClass = PoblacionObjetoEdadAreaGeografica.class) })

@Entity
public class PoblacionObjetoEdadAreaGeografica {
	@Id
	@Column(name="edad")
	private Integer edad;
	
	@Column(name="cantidad_femenino")
	private Integer cantidadFemenino;
	
	@Column(name="cantidad_masculino")
	private Integer cantidadMasculino;

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Integer getCantidadFemenino() {
		return cantidadFemenino;
	}

	public void setCantidadFemenino(Integer cantidadFemenino) {
		this.cantidadFemenino = cantidadFemenino;
	}

	public Integer getCantidadMasculino() {
		return cantidadMasculino;
	}

	public void setCantidadMasculino(Integer cantidadMasculino) {
		this.cantidadMasculino = cantidadMasculino;
	}
	
	
	
}
