package com.conexia.riesgos.storedprocedure;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.NamedNativeQuery;

@NamedNativeQuery(name = "GenericResponseEntity.crearUsuario", query = "select * from seguridad.crear_usuario_rol(:usuario,:password,:email,:id,:abreviatura,:rol,:telefono)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.bajaUsuario", query = "select * from transaccional.baja_usuarios(:abreviatura,:id,:usuario)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.asignarRol", query = "select * from seguridad.asignar_rol_a_usuario(:usuario,:rol)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.anularTransaccion", query = "select * from transaccional.anular_autorizacion(:rri,:motivo)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.impactarFaba", query = "select * from intercambio.impactar_faba_yyyymm(:id, :primerDiaMesReporte, :fechaInsert, :mesReporte)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.modificarUsuario", query = "select * from seguridad.modificar_usuario(:usuarioOriginal, :usuarioEditar, :passwordEditar, :emailEditar, :telefonoEditar, :habilitar)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.eliminarRoles", query = "select * from seguridad.eliminar_roles(:usuario)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.eliminarPracticas", query = "select * from seguridad.eliminar_practicas(:id)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.procesarPractica", query = "select * from intercambio.update_practica(:edadDesde,:edadHasta,:cantidadMinima,:cantidadMaxima,:ambito, :version)", resultClass = GenericResponseEntity.class)

@NamedNativeQuery(name = "GenericResponseEntity.modificarUsuarioBene", query = "select * from transaccional.update_beneficiario(:usuarioOriginal, :usuarioEditar, :passwordEditar, :emailEditar, :telefonoEditar, :habilitar)", resultClass = GenericResponseEntity.class)




@Entity
public class GenericResponseEntity {
	@Id
	private String detalle;
	private String status;

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public GenericResponseEntity(String detalle, String status) {
		this.detalle = detalle;
		this.status = status;
	}

	public GenericResponseEntity() {

	}

}
