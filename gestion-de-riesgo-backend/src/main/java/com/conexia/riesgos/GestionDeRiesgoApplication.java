package com.conexia.riesgos;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionDeRiesgoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionDeRiesgoApplication.class, args);
	
	}

}
