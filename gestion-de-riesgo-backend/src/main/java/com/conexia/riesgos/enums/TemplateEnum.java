package com.conexia.riesgos.enums;

public enum TemplateEnum {
	
	 TEMPLATE("EmailAltaTemplate.ftl");


	private String template;
	

	private TemplateEnum(String template) {
		this.template = template;
	}


	public String getTemplate() {
		return template;
	}

	
}