package com.conexia.riesgos.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.conexia.riesgos.config.UsuarioDetail;
import com.conexia.riesgos.dto.request.UsuarioBajaConsultaDto;
import com.conexia.riesgos.dto.response.UsuarioResponseDto;
import com.conexia.riesgos.model.Usuario;
import com.conexia.riesgos.repository.UsuarioRepository;
import com.conexia.riesgos.repository.UsuarioStoredProcedureJpaRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService, UserDetailsService {


	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioStoredProcedureJpaRepository usuarioStoredProcedureJpaRepository;

/**
 * 
	@Autowired
	private MailService mailService;


	@Value("${email.para}")
	private String emailPara;

	@Value("${email.copia}")
	private String emailCopia;

	private static final String asunto = "COMEI/ABM/";
	private static final String falloEmail = "Ups! Hubo un error al enviar el email, pero el alta fue exitosa.";
	private static final String error = "ERROR";
	private static final String success = "SUCCESS";

	private final Log logger = LogFactory.getLog(getClass());

 */



	@Override
	public UserDetails loadUserByUsername(String user) {
		Usuario usuario = usuarioRepository.findByUsuario(user);
		if (usuario == null) {
			throw new UsernameNotFoundException(user);
		}
		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("ADMIN"));
		Boolean enabled = usuario.getFechaDelete() == null;
		return new UsuarioDetail(usuario.getUsuario(), encoder.encode(usuario.getPassword()), roles, enabled);

	}

	

	

	@Override
	public List<UsuarioResponseDto> consultaUsuario(UsuarioBajaConsultaDto usuario) {
		List<UsuarioResponseDto> usuarios = new ArrayList<>();
		usuarioStoredProcedureJpaRepository.getUsuarios(usuario.getAbreviatura(), usuario.getId(), usuario.getUsuario())
				.stream().forEach(item -> usuarios.add(new UsuarioResponseDto(item)));
		return usuarios;

	}

	/**
	 *EJ: de MailService 
	
	private GenericResponseDto enviarMail(GenericResponseDto response, UsuarioAltaDto usuario) {
		if (response.getStatus().equals(UsuarioServiceImpl.success)) {

			MailDto mail = null;
			switch (usuario.getAbreviatura()) {
			case "PR":
				PrestadorResponseDto prestador = prestadorService.consultaPrestadorByID(usuario.getId());
				mail = new MailDto(this.emailPara, UsuarioServiceImpl.asunto, this.emailCopia, true, usuario, prestador);
				break;
			case "AM":
				AuditorResponseDto auditor = auditorService.consultarAuditorById(usuario.getId());
				mail = new MailDto(this.emailPara, UsuarioServiceImpl.asunto, this.emailCopia, true, usuario, auditor);
				break;
			case "DE":
				DelegacionResponseDto delegacion = delegacionService.consultarDelegacionById(usuario.getId());
				mail = new MailDto(this.emailPara, UsuarioServiceImpl.asunto, this.emailCopia, true, usuario, delegacion);
				break;
			default:
				break;
			}

			TemplateEnum tmp = TemplateEnum.TEMPLATE;
			try {
				mailService.mailSender(mail, tmp.getTemplate());
			} catch (MailServiceException e) {
				response.setDetalle(UsuarioServiceImpl.falloEmail);
				response.setStatus(UsuarioServiceImpl.error);
				logger.info(e.getMessage());
			}

		}

		return response;
	}
*/
	

}
