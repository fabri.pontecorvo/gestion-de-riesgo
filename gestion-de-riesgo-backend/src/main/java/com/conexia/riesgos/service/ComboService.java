package com.conexia.riesgos.service;

import java.util.List;

import com.conexia.riesgos.dto.response.ComboDto;
import com.conexia.riesgos.dto.response.InitDto;
import com.conexia.riesgos.dto.response.ProvinciaDto;

public interface ComboService {

	InitDto getAll();
	
	List<ComboDto> getAmbitos();
	
	List<ComboDto> getGeneros();

	List<ComboDto> getDiagnosticos();

	List<ComboDto> getProcedimientos();

	List<ComboDto> getRangosEtarios();

	List<ComboDto> getTiposDocumentos();

	List<ComboDto> getTiposEmpadronamientos();

	List<ComboDto> getTiposParientes();

	List<ComboDto> getGruposRiesgos();

	List<ComboDto> getGruposPoblacionales();

	List<ProvinciaDto> getProvincias();
	
}
