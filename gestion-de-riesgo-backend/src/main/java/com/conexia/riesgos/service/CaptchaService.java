package com.conexia.riesgos.service;

public interface CaptchaService {

	boolean verify(String recaptchaResponse);

}
