package com.conexia.riesgos.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexia.riesgos.dto.request.PoblacionObjetoFiltroDto;
import com.conexia.riesgos.dto.response.GrupoRiesgoDto;
import com.conexia.riesgos.dto.response.PoblacionObjetoDto;
import com.conexia.riesgos.model.GrupoRiesgo;
import com.conexia.riesgos.model.RangoEtario;
import com.conexia.riesgos.repository.GrupoRiesgoRepository;
import com.conexia.riesgos.repository.PoblacionObjetoEdadAreaGeograficaRepository;
import com.conexia.riesgos.repository.RangoEtarioRepository;
import com.conexia.riesgos.storedprocedure.PoblacionObjetoEdadAreaGeografica;
import com.conexia.riesgos.utils.Constantes;

@Service
public class PoblacionObjetoServiceImpl implements PoblacionObjetoService {
	
	@Autowired
	private PoblacionObjetoEdadAreaGeograficaRepository poblacionObjetoEdadAreaGeograficaRepository;
	
	@Autowired
	private RangoEtarioRepository rangoEtarioRepository;
	
	@Autowired
	private GrupoRiesgoRepository grupoRiesgoRepository;
	

	@Override
	@Transactional
	public PoblacionObjetoDto getDatos(List<PoblacionObjetoFiltroDto> filtros) {

		List<RangoEtario> rangoEtario = rangoEtarioRepository.findByOrderByEdadHastaDesc();
		
		List<GrupoRiesgo> riesgos = grupoRiesgoRepository.findAll();
		
		List<String> rangoEtarioString = rangoEtario.stream().map(e -> e.getEdadDesde() + " a " + e.getEdadHasta()).collect(Collectors.toList());
		
		List<PoblacionObjetoEdadAreaGeografica> listaTotalTemp = null;

		for (PoblacionObjetoFiltroDto filtro : filtros) {
			
			List<PoblacionObjetoEdadAreaGeografica> listaParcial;
			
			if (filtro.getCodGrupoPoblacional()!=null) {
				listaParcial = null;
			} else {
				listaParcial = callStore(filtro);
			}
			
			listaTotalTemp = sumTwoList(listaTotalTemp, listaParcial);
		}
		
		List<PoblacionObjetoEdadAreaGeografica> listaTotal = listaTotalTemp;
		List<Integer> masculinoTotal = rangoEtario.stream().map(e -> getCantidadMasculino(listaTotal, e.getEdadDesde(), e.getEdadHasta())).collect(Collectors.toList());
		List<Integer> femeninoTotal = rangoEtario.stream().map(e -> getCantidadFemenino(listaTotal, e.getEdadDesde(), e.getEdadHasta())).collect(Collectors.toList());

		List<GrupoRiesgoDto> riesgosDto = riesgos.stream().map(e -> getGrupoRiesgoDto(e, listaTotal)).collect(Collectors.toList());
		
		PoblacionObjetoDto response = new PoblacionObjetoDto();
		response.setRangosEtarios(rangoEtarioString);
		response.setCantFemeninos(femeninoTotal);
		response.setCantMasculinos(masculinoTotal);
		response.setGruposRiegos(riesgosDto);
		

		
		
		return response;
	}
	
	private List<PoblacionObjetoEdadAreaGeografica> sumTwoList (List<PoblacionObjetoEdadAreaGeografica> listTot, List<PoblacionObjetoEdadAreaGeografica> list) {
		List<PoblacionObjetoEdadAreaGeografica> listaTotal = null;
		
		if (listTot != null && listTot.size() > 0 ) {
			listaTotal = listTot;
			
			for (PoblacionObjetoEdadAreaGeografica item : list) {
				
				Optional<PoblacionObjetoEdadAreaGeografica> forSum = listTot.stream().filter(e -> e.getEdad().equals(item.getEdad())).findFirst();
				
				PoblacionObjetoEdadAreaGeografica forSave = new PoblacionObjetoEdadAreaGeografica();

				forSave.setEdad(item.getEdad());

				if (forSum.isPresent()) {
					forSave.setCantidadFemenino(item.getCantidadFemenino() + forSum.get().getCantidadFemenino());
					forSave.setCantidadMasculino(item.getCantidadMasculino() + forSum.get().getCantidadMasculino());
				} else {
					forSave.setCantidadFemenino(item.getCantidadFemenino());
					forSave.setCantidadMasculino(item.getCantidadMasculino());
				}
				
				listaTotal.add(forSave);
			}
		} else {
			listaTotal = list;
		}
		
		return listaTotal;	
	}
	
	private Integer getCantidadFemenino(List<PoblacionObjetoEdadAreaGeografica> lista, Integer edadDesde, Integer edadHasta) {
		return lista.stream().filter(p -> p.getEdad()>=edadDesde && p.getEdad()<edadHasta).mapToInt(x->x.getCantidadFemenino()).sum();
		
	}
	
	private Integer getCantidadMasculino(List<PoblacionObjetoEdadAreaGeografica> lista, Integer edadDesde, Integer edadHasta) {
		return lista.stream().filter(p -> p.getEdad()>=edadDesde && p.getEdad()<edadHasta).mapToInt(x->x.getCantidadMasculino()).sum();
		
	}	
	
	private Integer getCantidadAmbos(List<PoblacionObjetoEdadAreaGeografica> lista, Integer edadDesde, Integer edadHasta) {
		return lista.stream().filter(p -> p.getEdad()>=edadDesde && p.getEdad()<edadHasta).mapToInt(x->x.getCantidadFemenino()+x.getCantidadMasculino()).sum();
		
	}
	
	private List<PoblacionObjetoEdadAreaGeografica> callStore(PoblacionObjetoFiltroDto filtro) {
		return poblacionObjetoEdadAreaGeograficaRepository.findByFilters(
				filtro.getCodGenero()==null||filtro.getCodGenero().contentEquals(Constantes.GENERO_AMBOS)?Constantes.SIN_VALOR_STRING:filtro.getCodGenero(),
				filtro.getEdadDesde()==null?Constantes.SIN_VALOR_INT:filtro.getEdadDesde(),
				filtro.getEdadHasta()==null?Constantes.SIN_VALOR_INT:filtro.getEdadHasta(),
				filtro.getCodLocalidad()==null?Constantes.SIN_VALOR_STRING:filtro.getCodLocalidad(),
				filtro.getCodProvincia()==null?Constantes.SIN_VALOR_STRING:filtro.getCodProvincia()
			);
	}
	
	private GrupoRiesgoDto getGrupoRiesgoDto (GrupoRiesgo gr, List<PoblacionObjetoEdadAreaGeografica> lista) {
		GrupoRiesgoDto dto = new GrupoRiesgoDto();
		
		dto.setCodigo(gr.getCodigo());
		dto.setDescripcion(gr.getDescripcion());
		
		Integer frecuenciaPoblacional = null;
		Integer total = null;
		
		if (gr.getGenero().getCodigo().equals(Constantes.GENERO_AMBOS)) {
			frecuenciaPoblacional = getCantidadAmbos(lista, gr.getEdadDesde(), gr.getEdadHasta());
			total = lista.stream().mapToInt(x -> x.getCantidadFemenino()+x.getCantidadMasculino()).sum();
		} else if (gr.getGenero().getCodigo().equals(Constantes.GENERO_FEMENINO)) {
			frecuenciaPoblacional = getCantidadFemenino(lista, gr.getEdadDesde(), gr.getEdadHasta());
			total = lista.stream().mapToInt(x -> x.getCantidadFemenino()).sum();
		} else if (gr.getGenero().getCodigo().equals(Constantes.GENERO_MASCULINO)) {
			frecuenciaPoblacional = getCantidadMasculino(lista, gr.getEdadDesde(), gr.getEdadHasta());	
			total = lista.stream().mapToInt(x -> x.getCantidadMasculino()).sum();
		}
		
		dto.setFrecuenciaPoblacional(frecuenciaPoblacional);
		
		dto.setPorcentajePoblacionRiesgo(total.equals(0)?0:((double)frecuenciaPoblacional/total*100));
		
		return dto;
	}
	

}
