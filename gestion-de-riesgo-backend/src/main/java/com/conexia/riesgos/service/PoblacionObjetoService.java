package com.conexia.riesgos.service;

import java.util.List;

import com.conexia.riesgos.dto.request.PoblacionObjetoFiltroDto;
import com.conexia.riesgos.dto.response.PoblacionObjetoDto;

public interface PoblacionObjetoService {

	public PoblacionObjetoDto getDatos(List<PoblacionObjetoFiltroDto> filtro);

}
