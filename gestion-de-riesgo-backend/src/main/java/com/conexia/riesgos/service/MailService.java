package com.conexia.riesgos.service;

import com.conexia.riesgos.dto.response.MailDto;
import com.conexia.riesgos.exception.MailServiceException;

public interface MailService {

	public String mailSender(MailDto mailDto, String template)
			throws MailServiceException;
}
