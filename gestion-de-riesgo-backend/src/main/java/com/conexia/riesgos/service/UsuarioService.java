package com.conexia.riesgos.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import com.conexia.riesgos.dto.request.UsuarioBajaConsultaDto;
import com.conexia.riesgos.dto.response.UsuarioResponseDto;

public interface UsuarioService {



	List<UsuarioResponseDto> consultaUsuario(UsuarioBajaConsultaDto usuario);


	UserDetails loadUserByUsername(String username);


}
