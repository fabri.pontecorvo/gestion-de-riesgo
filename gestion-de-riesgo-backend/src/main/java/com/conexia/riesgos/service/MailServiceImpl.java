package com.conexia.riesgos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexia.riesgos.component.SendMailFreemarker;
import com.conexia.riesgos.dto.response.MailDto;
import com.conexia.riesgos.exception.MailServiceException;

@Service("mailService")
public class MailServiceImpl implements MailService {

	@Autowired
	private SendMailFreemarker sendMailFreemarker;

	/**
	 * Retorna el mail como string para poder gardarlo en mensajeria.notificaciones
	 */
	@Override
	public String mailSender(MailDto mailDto, String template) throws MailServiceException {

		return sendMailFreemarker.sendMail(mailDto, template);
	}
}