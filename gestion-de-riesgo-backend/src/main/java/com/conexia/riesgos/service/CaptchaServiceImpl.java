package com.conexia.riesgos.service;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.conexia.riesgos.dto.response.RecaptchaResponseDto;

@Service 
public class CaptchaServiceImpl implements CaptchaService {
	private final Log logger = LogFactory.getLog(getClass());

	private final RestTemplate restTemplate;

    public CaptchaServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Value("${google.recaptcha.secret.key}")
    public String recaptchaSecret;
    @Value("${google.recaptcha.verify.url}")
    public String recaptchaVerifyUrl;
    
    @Override
    public boolean verify(String response) {
        MultiValueMap<Object, Object> param= new LinkedMultiValueMap<>();
        param.add("secret", recaptchaSecret);
        param.add("response", response);

        RecaptchaResponseDto recaptchaResponse = null;
        try {
            recaptchaResponse = this.restTemplate.postForObject(recaptchaVerifyUrl, param, RecaptchaResponseDto.class);
        }catch(RestClientException e){
        	logger.error(e.getMessage());
        }
       if(recaptchaResponse.isSuccess()){
            return true;
        }else {
            return false;
        }
    }

}
