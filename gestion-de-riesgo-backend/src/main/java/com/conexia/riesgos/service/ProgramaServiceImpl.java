package com.conexia.riesgos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexia.riesgos.model.EmpadronamientoAccion;
import com.conexia.riesgos.model.EmpadronamientoCondicion;
import com.conexia.riesgos.model.EmpadronamientoDocumentacion;
import com.conexia.riesgos.model.EmpadronamientoExcepcion;
import com.conexia.riesgos.model.EmpadronamientoMotivo;
import com.conexia.riesgos.model.EstadoAuditoria;
import com.conexia.riesgos.model.EstadoPrograma;
import com.conexia.riesgos.model.Genero;
import com.conexia.riesgos.model.GrupoPoblacional;
import com.conexia.riesgos.model.GrupoRiesgo;
import com.conexia.riesgos.model.Programa;
import com.conexia.riesgos.model.RangoEtario;
import com.conexia.riesgos.model.TipoEmpadronamiento;
import com.conexia.riesgos.model.TipoPariente;
import com.conexia.riesgos.model.UnidadTiempo;
import com.conexia.riesgos.repository.EmpadronamientoAccionRepository;
import com.conexia.riesgos.repository.EmpadronamientoCondicionRepository;
import com.conexia.riesgos.repository.EmpadronamientoDocumentacionRepository;
import com.conexia.riesgos.repository.EmpadronamientoExcepcionRepository;
import com.conexia.riesgos.repository.EmpadronamientoMotivoRepository;
import com.conexia.riesgos.repository.EstadoAuditoriaRepository;
import com.conexia.riesgos.repository.EstadoProgramaRepository;
import com.conexia.riesgos.repository.GeneroRepository;
import com.conexia.riesgos.repository.GrupoPoblacionalRepository;
import com.conexia.riesgos.repository.GrupoRiesgoRepository;
import com.conexia.riesgos.repository.ProgramaRepository;
import com.conexia.riesgos.repository.RangoEtarioRepository;
import com.conexia.riesgos.repository.TipoEmpadronamientoRepository;
import com.conexia.riesgos.repository.TipoParienteRepository;
import com.conexia.riesgos.repository.UnidadTiempoRepository;

@Service
public class ProgramaServiceImpl implements ProgramaService {
	
	@Autowired
	private EmpadronamientoAccionRepository empadronamientoAccionRepository;
	
	@Autowired
	private EmpadronamientoCondicionRepository empadronamientoCondicionRepository;

	@Autowired
	private EmpadronamientoDocumentacionRepository empadronamientoDocumentacionRepository;
	
	@Autowired
	private EmpadronamientoExcepcionRepository empadronamientoExcepcionRepository;
	
	@Autowired
	private EmpadronamientoMotivoRepository empadronamientoMotivoRepository;
	
	@Autowired
	private EstadoAuditoriaRepository estadoAuditoriaRepository;
	
	@Autowired
	private EstadoProgramaRepository estadoProgramaRepository;
	
	@Autowired
	private GrupoPoblacionalRepository grupoPoblacionalRepository;
	
	@Autowired
	private GeneroRepository generoRepository;
	
	@Autowired
	private UnidadTiempoRepository unidadTiempoRepository;
	
	@Autowired
	private GrupoRiesgoRepository grupoRiegoRepository;
	
	@Autowired
	private RangoEtarioRepository rangoEtarioRepository;
	
	@Autowired
	private TipoEmpadronamientoRepository tipoEmpadronamientoRepository;
	
	@Autowired
	private TipoParienteRepository tipoParienteRepository;
	
	@Autowired
	private ProgramaRepository programaRepository;


	@Override
	public void registrar() {
		
		List<EmpadronamientoAccion> lista1 = empadronamientoAccionRepository.findAll();
		
		List<EmpadronamientoCondicion> lista2 = empadronamientoCondicionRepository.findAll();
		
		List<EmpadronamientoDocumentacion> lista3 = empadronamientoDocumentacionRepository.findAll();
		
		List<EmpadronamientoExcepcion> lista4 = empadronamientoExcepcionRepository.findAll();
		
		List<EmpadronamientoMotivo> lista5 = empadronamientoMotivoRepository.findAll();
		
		List<EstadoAuditoria> lista6 = estadoAuditoriaRepository.findAll();
		
		List<EstadoPrograma> lista7 = estadoProgramaRepository.findAll();
		
		List<GrupoPoblacional> lista8 = grupoPoblacionalRepository.findAll();
		
		List<Genero> lista9 = generoRepository.findAll();
		
		List<UnidadTiempo> lista10 = unidadTiempoRepository.findAll();
		
		List<GrupoRiesgo> lista11 = grupoRiegoRepository.findAll();
		
		List<RangoEtario> lista12 = rangoEtarioRepository.findAll();
		
		List<TipoEmpadronamiento> lista13 = tipoEmpadronamientoRepository.findAll();
		
		List<TipoPariente> lista14 = tipoParienteRepository.findAll();
		
		List<Programa> lista15 = programaRepository.findAll();		
		
		
		System.out.println(lista1.size());
		
		System.out.println(lista2.size());
		
		System.out.println(lista3.size());
		
		System.out.println(lista4.size());
		
		System.out.println(lista5.size());
		
		System.out.println(lista6.size());
		
		System.out.println(lista7.size());
		
		System.out.println(lista8.size());
		
		System.out.println(lista9.size());
		
		System.out.println(lista10.size());
		
		System.out.println(lista11.size());
		
		System.out.println(lista12.size());
		
		System.out.println(lista13.size());
		
		System.out.println(lista14.size());
		
		System.out.println(lista15.size());
		
		Programa programa1 = new Programa();
		
		
		
		programaRepository.save(programa1);
		
		
		// TODO Auto-generated method stub
		
	}



}
