package com.conexia.riesgos.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexia.riesgos.dto.response.ComboDto;
import com.conexia.riesgos.dto.response.InitDto;
import com.conexia.riesgos.dto.response.ProvinciaDto;
import com.conexia.riesgos.model.TipoPariente;
import com.conexia.riesgos.repository.AmbitoRepository;
import com.conexia.riesgos.repository.DiagnosticoRepository;
import com.conexia.riesgos.repository.GeneroRepository;
import com.conexia.riesgos.repository.GrupoPoblacionalRepository;
import com.conexia.riesgos.repository.GrupoRiesgoRepository;
import com.conexia.riesgos.repository.LocalidadRepository;
import com.conexia.riesgos.repository.ProcedimientoRepository;
import com.conexia.riesgos.repository.ProvinciaRepository;
import com.conexia.riesgos.repository.RangoEtarioRepository;
import com.conexia.riesgos.repository.TipoDocumentoRepository;
import com.conexia.riesgos.repository.TipoEmpadronamientoRepository;
import com.conexia.riesgos.repository.TipoParienteRepository;

@Service
public class ComboServiceImpl implements ComboService {
	
	@Autowired
	private AmbitoRepository ambitoRepository;

	@Autowired
	private GeneroRepository generoRepository;
	
	@Autowired
	private DiagnosticoRepository diagnosticoRepository;
	
	@Autowired
	private ProcedimientoRepository procedimientoRepository;
	
	@Autowired
	private RangoEtarioRepository rangoEtarioRepository;
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	
	@Autowired
	private TipoEmpadronamientoRepository tipoEmpadronamientoRepository;
	
	@Autowired
	private TipoParienteRepository tipoParienteRespository;
	
	@Autowired
	private GrupoRiesgoRepository grupoRiesgoRepository;
	
	@Autowired
	private GrupoPoblacionalRepository grupoPoblacionalRepository;
	
	@Autowired
	private ProvinciaRepository provinciaRepository;
	
	@Autowired
	private LocalidadRepository localidadRepository;
	
	@Override
	public List<ComboDto> getAmbitos() {
		
		List<ComboDto> list = ambitoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}
	
	@Override
	public List<ComboDto> getGeneros() {
		
		List<ComboDto> list = generoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}	
	
	@Override
	public List<ComboDto> getDiagnosticos() {
		
		List<ComboDto> list = diagnosticoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}	
	
	@Override
	public List<ComboDto> getProcedimientos() {
		
		List<ComboDto> list = procedimientoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}
	
	
	@Override
	public List<ComboDto> getRangosEtarios() {
		
		List<ComboDto> list = rangoEtarioRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getEdadDesde() + " a " + e.getEdadHasta())).collect(Collectors.toList());
		
		return list;
	}
	
	@Override
	public List<ComboDto> getTiposDocumentos() {
		
		List<ComboDto> list = tipoDocumentoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}
	
	@Override
	public List<ComboDto> getTiposEmpadronamientos() {
		
		List<ComboDto> list = tipoEmpadronamientoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}
	
	@Override
	public List<ComboDto> getTiposParientes() {
		
		List<TipoPariente> tiposParientes = tipoParienteRespository.findAll();
		
		List<ComboDto> list = tiposParientes.stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}	
	
	@Override
	public List<ComboDto> getGruposRiesgos() {
		
		List<ComboDto> list = grupoRiesgoRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}
	
	@Override
	public List<ComboDto> getGruposPoblacionales() {
		
		List<ComboDto> list = grupoPoblacionalRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		return list;
	}
	
	@Override
	public List<ProvinciaDto> getProvincias() {
		
		List<ComboDto> list = provinciaRepository.findAll().stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
		
		List<ProvinciaDto> provincias = new ArrayList<ProvinciaDto>();
		
		for (ComboDto comboDto : list) {
			
			ProvinciaDto p = new ProvinciaDto();
			List<ComboDto> localidades = localidadRepository.findByProvinciaCodigo(comboDto.getCodigo()).stream().map(e -> new ComboDto(e.getCodigo(), e.getDescripcion())).collect(Collectors.toList());
			
			p.setProvincia(comboDto);
			p.setLocalidades(localidades);
			
			provincias.add(p);
		}
			    
		return provincias;
	}

	@Override
	public InitDto getAll() {
		InitDto init = new InitDto();
		
		init.setAmbitos(this.getAmbitos());
		init.setDiagnosticos(this.getDiagnosticos());
		init.setGeneros(this.getGeneros());
		init.setGruposPoblacionales(this.getGruposPoblacionales());
		init.setGruposRiegos(this.getGruposRiesgos());
		init.setProcedimientos(this.getProcedimientos());
		
		init.setRangosEtarios(this.getRangosEtarios());
		init.setTiposDocumentos(this.getTiposDocumentos());
		init.setTiposParientes(this.getTiposParientes());
		
		init.setProvincias(this.getProvincias());
		
		return init;
	}

}
