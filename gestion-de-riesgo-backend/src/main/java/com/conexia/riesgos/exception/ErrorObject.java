package com.conexia.riesgos.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ErrorObject implements Serializable{
	
	private static final long serialVersionUID = -7540354574581744202L;
	private List<Error> errorList = new ArrayList<>();
	private boolean hasError;			
	
	public void addError(String etiqueta,String error){
		Error errorObj = new Error();
		errorObj.setEtiqueta(etiqueta);
		errorObj.setError(error);
		this.errorList.add(errorObj);
	}
	
	public List<Error> getErrorList(){
		return this.errorList;
	}
	
	public boolean getHasError() {
		if(this.errorList.isEmpty()){
			this.hasError = false;
		}else{
			this.hasError = true;
		}
			
		return hasError;
	}
	
}