package com.conexia.riesgos.exception;

public class MailServiceException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MailServiceException(String tittle, String message){
		this.error.addError(tittle, message);
	}
}