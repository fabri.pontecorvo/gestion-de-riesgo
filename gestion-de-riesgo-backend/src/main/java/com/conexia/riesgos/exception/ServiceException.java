package com.conexia.riesgos.exception;

import java.io.Serializable;
import java.util.List;

public class ServiceException extends Exception implements Serializable {

	
	private static final long serialVersionUID = -4288780289012966615L;
	protected ErrorObject error = new ErrorObject();

	public ErrorObject getError() {
		return error;
	}

	public void setError(ErrorObject error) {
		this.error = error;
	}
	
	public List<Error> getErrorList(){
		return this.error.getErrorList();
	}
}