package com.conexia.riesgos.exception;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Error  implements Serializable{

	private static final long serialVersionUID = -5810066121090189930L;
	String etiqueta;
	String error;

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}