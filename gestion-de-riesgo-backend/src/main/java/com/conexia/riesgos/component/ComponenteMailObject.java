package com.conexia.riesgos.component;

import java.util.ArrayList;
import java.util.List;

public class ComponenteMailObject {	
	private List<ComponenteMail> componenteMailList = new ArrayList<>();
	private boolean hasComponente;			
	
	public void addComponenteMail(String label,String descripcion){
		ComponenteMail componente = new ComponenteMail();
		componente.setLabel(label);
		componente.setDescripcion(descripcion);
		this.componenteMailList.add(componente);
	}
	
	public List<ComponenteMail> getComponenteMailList(){
		return this.componenteMailList;
	}
	
	public boolean getHasComponente() {
		if(this.componenteMailList.isEmpty()){
			this.hasComponente = false;
		}else{
			this.hasComponente = true;
		}
			
		return hasComponente;
	}
	
}