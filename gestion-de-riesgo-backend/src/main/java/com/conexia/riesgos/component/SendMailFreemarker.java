package com.conexia.riesgos.component;

import java.io.StringWriter;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeUtility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.conexia.riesgos.config.ApplicationWebSettings;
import com.conexia.riesgos.dto.response.MailDto;
import com.conexia.riesgos.exception.ErrorObject;
import com.conexia.riesgos.exception.MailServiceException;

import freemarker.template.Configuration;
import freemarker.template.Template;

/***
 * Componente generico para el envio de mails con templates, invocar el metodo
 * sendMail y pasa por parametros MailDto con el atributo "mensajeAsMap" seteado
 * (de aca se van a tomar los datos para cargar el template) completo. y el
 * nombre del template (ej: TrasladosTemplate.ftl).
 * 
 * Cada elemento del map debe tener su correspondencia en el ftl para que se
 * rederice correctamente en el mail recibido.
 * 
 * @author jprino
 *
 */
@Component
public class SendMailFreemarker {

	private MailSender mailSender;

	private static final Logger logger = LogManager.getLogger(SendMailFreemarker.class);

	private String contentToPut = "";

	@Autowired
	private Configuration freemarkerConfiguration;
	
	@Value("${email.ip}")
	private String ip;
	
	/**
	 * Envia el mail con los datos especificados en el dto, para que funcione
	 * correctamente tiene que estar completado "mensajeAsMap" (de aca se van a
	 * tomar los datos para cargar el template).
	 * 
	 * @param MailDto
	 * @param mailTemplate (ej: trasladoTemplate.ftl)
	 * @throws MailServiceException
	 * 
	 *                              Retorna el mail como string para poder gardarlo
	 *                              en mensajeria.notificaciones
	 */
	public String sendMail(MailDto dto, String template) throws MailServiceException {

		if (dto.getEnviarCopia()) {
			if (dto.getDestinatario().contains("@")) {
				dto.setDestinatario(dto.getDestinatario() + ";" + dto.getMailContacto());
			} else {
				throw new MailServiceException("Mail de contacto: ", "Complete con un mail valido, por favor");
			}

		}

		this.processSend(dto, template);

		return contentToPut;
	}

	/**
	 * configuracion de comunicacion del smtp
	 */
	private void setUpMail() {
		logger.info("Configurando envio de mail");
		this.mailSender = new MailSender();
		this.mailSender.setUsuario(ApplicationWebSettings.MAIL_USER);
		this.mailSender.setClave(ApplicationWebSettings.MAIL_PASSWORD);
		this.mailSender.setHost(this.ip);
		this.mailSender.setPort(ApplicationWebSettings.MAIL_PORT);
		this.mailSender.setProtocoloTrasporte(ApplicationWebSettings.MAIL_PROTOCOL);
		this.mailSender.setDebug(true);

		try {
			this.mailSender.setEnviadoPor(ApplicationWebSettings.MAIL_ENVIADO_POR);
			this.mailSender.addReplyTo(ApplicationWebSettings.MAIL_REPLY_TO);
			this.mailSender.setTipoContenido(ApplicationWebSettings.MAIL_TIPO_CONTENIDO);

		} catch (AddressException e) {
			logger.error(" Error de direccion de internet ");
			logger.error(e.getMessage(), e);
		}
	}

	private boolean destinatarioInvalido(MailDto dto) {
		return (StringUtils.containsWhitespace(dto.getDestinatario()) || StringUtils.isEmpty(dto.getDestinatario()));
	}

	/**
	 * Realiza el envio del mail
	 * 
	 * @param dto
	 * @param mailTemplate
	 * @throws MailServiceException
	 */
	private void processSend(MailDto dto, String mailTemplate) throws MailServiceException {
		ErrorObject error = new ErrorObject();
		StringWriter sWriter = new StringWriter();
		Template template = null;

		try {

			logger.info("Generando mail ");

			if (destinatarioInvalido(dto)) {
				logger.info(" El destinatario es invalido : " + dto.getDestinatario());
			}

			this.setUpMail();

			for (String destinatario : dto.getDestinatario().split(";")) {
				mailSender.addDestinatario(destinatario);
			}

			this.mailSender.setAsunto(MimeUtility.encodeWord(dto.getAsunto(), "utf-8", "B"));

			template = freemarkerConfiguration.getTemplate(mailTemplate);

			template.process(dto.getMensajeAsMap(), sWriter);
			sWriter.flush();
			sWriter.close();
			contentToPut = sWriter.toString();

			this.mailSender.setContenido(contentToPut);

			logger.info("Antes de mandar con el mail sender :" + mailSender);
			this.mailSender.send();
			logger.info(" Luego de mandar el mail con el mail sender ");

		} catch (MessagingException e) {
			logger.error("Error de messaing exception", e);
			logger.error(e.getMessage(), e);
			error.addError("Error al enviar mail a: ", dto.getDestinatario());

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			error.addError("Error al enviar mail: ", "Error inesperado");
		}
		if (error.getHasError()) {
			throw new MailServiceException("Error al enviar mail: ", "Se ha producido un error");
		}
	}
}