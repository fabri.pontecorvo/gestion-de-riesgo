package com.conexia.riesgos.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class MailSender {

	public static final String MAILTRANSPORTPROTOCOLSMTPS = "smtps";
	public static final String MAILTRANSPORTPROTOCOLSMTP = "smtp";

	public static final String MIMETYPETEXTPLAIN = "text/plain";
	public static final String MIMETYPETEXTHTML = "text/html";

	protected String protocoloTrasporte = "smtp";
	protected boolean isDebug = false;
	
	protected String host = null;
	protected String port = null;
	protected String usuario = null;
	/** usuario de la cuenta que env�a */
	protected String clave = null;
	/** clave del usuario de la cuenta que env�a */
	protected InternetAddress enviadoPor = null;
	/** cuenta de email del que env�a "From" */
	protected List<InternetAddress> replyTo = new ArrayList<>();
	/** cuenta de email a la que se enviara la respuesta */
	protected String asunto = null;
	protected List<InternetAddress> destinatarios = new ArrayList<>();
	/** cuentas a las que se env�a "To" */
	protected List<InternetAddress> enCopia = new ArrayList<>();
	/** cuentas a las que se env�a en copia "cc" */
	protected List<InternetAddress> enCopiaOculta = new ArrayList<>();
	/** cuentas a las que se env�a en copia oculta "bcc" */
	protected String contenido = null;
	/** cuerpo del mensaje */
	protected String tipoContenido = null;
	/** mimeType del mensaje */
	protected List<String[]> adjuntos = new ArrayList<>();
	/**
	 * el primer string contiene el nombre que va al content-id y el segundo el path
	 * al recurso
	 */
	protected Properties mailProperties = new Properties();
	protected String[] firmaAsImage = { null, null };
	
	private static  Log logger = LogFactory.getLog(MailSender.class);


	/** para incluir la firma como una imagen */

	public void send() throws MessagingException {

		mailProperties.setProperty("mail.transport.protocol", this.getProtocoloTrasporte());
		mailProperties.setProperty("mail.host", this.getHost());
		mailProperties.setProperty("mail.port", this.getPort());
		mailProperties.setProperty("mail.user", this.getUsuario());
		mailProperties.setProperty("mail.password", this.getClave());
		mailProperties.setProperty("mail.auth", "true");
		mailProperties.setProperty("mail.smtps.auth", "true");
		mailProperties.setProperty("mail.debug", Boolean.toString(isDebug()));

		Authenticator authenticator = new SmtpAuthenticator();
		Session mailSession = Session.getInstance(mailProperties, authenticator);
		Transport transport = mailSession.getTransport();
		MimeMessage message = new MimeMessage(mailSession);

		message.setSubject(this.getAsunto());
		message.setFrom(this.getEnviadoPor());

		message.setReplyTo(getReplyTo());

		this.destinatariosToMessage(message);

		if (this.isMailSoloTexto()) {

			message.setContent(this.getContenido(), this.getTipoContenido());

		} else {
			MimeMultipart multipart = this.getNewMimeMultipart();

			this.addTextoMail(multipart);

			this.firmaToMessage(multipart);

			this.adjuntosToMessage(multipart);

			message.setContent(multipart);
		}

		transport.connect();
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}

	protected void addTextoMail(MimeMultipart multipart) throws MessagingException {

		if (this.getContenido() != null && this.getContenido().length() != 0) {

			BodyPart messageContentBodyPart = new MimeBodyPart();

			messageContentBodyPart.setContent(this.getContenido(), this.getTipoContenido());

			multipart.addBodyPart(messageContentBodyPart);
		}
	}

	/**
	 * 
	 * @return
	 */
	protected MimeMultipart getNewMimeMultipart() {

		MimeMultipart multipart = null;

		if (this.getFirmaAsImage()[0] != null) {

			multipart = new MimeMultipart("related");
		} else {

			multipart = new MimeMultipart("mixed");
		}
		return multipart;
	}

	protected boolean isMailSoloTexto() {

		return this.adjuntos.isEmpty() && this.getFirmaAsImage()[0] == null;
	}

	protected void destinatariosToMessage(MimeMessage message) throws MessagingException {

		message.addRecipients(Message.RecipientType.TO, this.getDestinatariosAsArray());

		if (!this.enCopia.isEmpty()) {

			message.addRecipients(Message.RecipientType.CC, this.getEnCopiaAsArray());
		}

		if (!this.enCopiaOculta.isEmpty()) {

			message.addRecipients(Message.RecipientType.BCC, this.getEnCopiaOcultaAsArray());
		}

	}

	/**
	 * Agrega la firma al mensaje si es que est� seteada
	 * 
	 * @param multipart
	 * @throws MessagingException
	 */
	protected void firmaToMessage(MimeMultipart multipart) throws MessagingException {

		if (this.getFirmaAsImage()[0] != null) {

			BodyPart bodyPart = new MimeBodyPart();
			DataSource dataSource = new FileDataSource(this.getFirmaAsImage()[1]);

			bodyPart.setDataHandler(new DataHandler(dataSource));
			bodyPart.setHeader("Content-ID", "<" + this.getFirmaAsImage()[0] + ">"); // este nombre debe coincidir con
																						// el cid del html, y ac� hay
																						// que agregar los s�mbolos de
																						// mayor y menor
			bodyPart.setDisposition("inline"); // para que se muestre la imagen esta propiedad es necesaria
			bodyPart.setFileName(this.getFirmaAsImage()[0]);

			multipart.addBodyPart(bodyPart);
		}
	}

	/**
	 * Agrega los adjuntos al mensaje si es que hay elementos para agregar
	 * 
	 * @param multipart
	 * @throws MessagingException
	 */
	protected void adjuntosToMessage(MimeMultipart multipart) throws MessagingException {

		if (!this.adjuntos.isEmpty()) {

			for (int i = 0; i < this.adjuntos.size(); i++) {

				String[] adjunto = this.adjuntos.get(i);
				BodyPart bodyPart = new MimeBodyPart();
				DataSource dataSource = new FileDataSource(adjunto[1]);

				bodyPart.setDataHandler(new DataHandler(dataSource));
				bodyPart.setDisposition("attachment");
				bodyPart.setFileName(adjunto[0]);

				multipart.addBodyPart(bodyPart);
			}
		}
	}

	/**
	 * 
	 * @param nombre
	 * @param path
	 */
	public void setFirmaAsImage(String nombre, String path) {

		this.firmaAsImage[0] = nombre;
		this.firmaAsImage[1] = path;
	}

	/**
	 * 
	 * @return
	 */
	public String[] getFirmaAsImage() {

		return this.firmaAsImage;
	}

	/**
	 * 
	 */
	public void addAdjunto(String nombreAdjunto, String pathAdjunto) {

		String[] adjunto = { nombreAdjunto, pathAdjunto };

		this.adjuntos.add(adjunto);
	}

	/**
	 * @throws AddressException
	 * 
	 */
	public void addDestinatario(String destinatario) throws AddressException {
		this.destinatarios.add(new InternetAddress(destinatario));
	}

	/**
	 * @throws AddressException
	 * 
	 */
	public void addEnCopia(String destinatario) throws AddressException {
		this.enCopia.add(new InternetAddress(destinatario));
	}

	/**
	 * @throws AddressException
	 * 
	 */
	public void addEnCopiaOculta(String destinatario) throws AddressException {
		this.enCopiaOculta.add(new InternetAddress(destinatario));
	}

	/**
	 * 
	 */
	public InternetAddress[] getDestinatariosAsArray() {

		InternetAddress[] destinatariosAsArray = {};
		return destinatarios.toArray(destinatariosAsArray);
	}

	/**
	 * 
	 */
	public InternetAddress[] getEnCopiaAsArray() {

		InternetAddress[] enCopiaAsArray = {};
		return this.enCopia.toArray(enCopiaAsArray);
	}

	/**
	 * 
	 */
	public InternetAddress[] getEnCopiaOcultaAsArray() {

		InternetAddress[] enCopiaOcultaAsArray = {};

		return this.enCopiaOculta.toArray(enCopiaOcultaAsArray);
	}

	/**
	 * @return the protocoloTrasporte
	 */
	public String getProtocoloTrasporte() {
		return protocoloTrasporte;
	}

	/**
	 * @param protocoloTrasporte the protocoloTrasporte to set
	 */
	public void setProtocoloTrasporte(String protocoloTrasporte) {
		this.protocoloTrasporte = protocoloTrasporte;
	}

	/**
	 * @return the isDebug
	 */
	public boolean isDebug() {
		return isDebug;
	}

	/**
	 * @param isDebug the isDebug to set
	 */
	public void setDebug(boolean isDebug) {
		this.isDebug = isDebug;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the enviadoPor
	 */
	public InternetAddress getEnviadoPor() {
		return enviadoPor;
	}

	/**
	 * @param enviadoPor the enviadoPor to set
	 */
	public void setEnviadoPor(String enviadoPor) throws AddressException {
		this.enviadoPor = new InternetAddress(enviadoPor);
	}

	/**
	 * @return the replyTo
	 */
	public InternetAddress[] getReplyTo() {
		InternetAddress[] replyToAsArray = {};
		return replyTo.toArray(replyToAsArray);
	}

	/**
	 * @throws AddressException
	 * 
	 */
	public void addReplyTo(String replyTo) throws AddressException {
		this.replyTo.add(new InternetAddress(replyTo));
	}

	/**
	 * @return the asunto
	 */
	public String getAsunto() {
		return asunto;
	}

	/**
	 * @param asunto the asunto to set
	 */
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	/**
	 * @return the contenido
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * @param contenido the contenido to set
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	/**
	 * @return the tipoContenido
	 */
	public String getTipoContenido() {
		return tipoContenido;
	}

	/**
	 * @param tipoContenido the tipoContenido to set
	 */
	public void setTipoContenido(String tipoContenido) {
		this.tipoContenido = tipoContenido;
	}

	public class SmtpAuthenticator extends javax.mail.Authenticator {
		String pass = "";
		String login = "";

		public SmtpAuthenticator() {

			super();

			login = mailProperties.getProperty("mail.user");
			pass = mailProperties.getProperty("mail.password");

		}

		public SmtpAuthenticator(String login, String pass) {
			super();
			this.login = login;
			this.pass = pass;
		}
		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			if (pass.equals(""))
				return null;
			else
				return new PasswordAuthentication(login, pass);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		MailSender mailSender = new MailSender();

		mailSender.setDebug(false);
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort("465");
		mailSender.setProtocoloTrasporte(MAILTRANSPORTPROTOCOLSMTPS);
		mailSender.setUsuario("usuario@dominio"); // cuenta desde la cual se envia el mail
		mailSender.setClave("clave");

		try {
			mailSender.setEnviadoPor("alguien <usuario@dominio>");

		} catch (AddressException e) {
			logger.info(e.getMessage());
		}

		try {
			mailSender.addDestinatario("alias <usuario@dominio>");

		} catch (AddressException e) {
			logger.info(e.getMessage());
		}

		mailSender.setAsunto("asunto");

		

		try {
			mailSender.send();
		} catch (MessagingException e) {
			logger.info(e.getMessage());
		}
	}
}
