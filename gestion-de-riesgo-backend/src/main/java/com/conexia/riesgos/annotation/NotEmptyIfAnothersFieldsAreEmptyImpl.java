package com.conexia.riesgos.annotation;



import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.StringUtils;


public class NotEmptyIfAnothersFieldsAreEmptyImpl
		implements ConstraintValidator<NotEmptyIfAnothersFieldsAreEmpty, Object> {

	private String fieldName;	
	private String dependFieldName;
	private String dependFieldName2;

	@Override
	public void initialize(NotEmptyIfAnothersFieldsAreEmpty annotation) {
		fieldName = annotation.fieldName();		
		dependFieldName = annotation.dependFieldName();
		dependFieldName2 = annotation.dependFieldName2();
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {

		if (value == null) {
			return true;
		}

		try {
			String fieldValue = BeanUtils.getProperty(value, fieldName);
			String dependFieldValue = BeanUtils.getProperty(value, dependFieldName);	
			String dependFieldValue2 = BeanUtils.getProperty(value, dependFieldName2);	
			
			if (StringUtils.isEmpty(fieldValue) && StringUtils.isEmpty(dependFieldValue) && StringUtils.isEmpty(dependFieldValue2)) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
						.addPropertyNode(dependFieldName).addConstraintViolation();

				return false;
			}

		} catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}

		return true;
	}

}
