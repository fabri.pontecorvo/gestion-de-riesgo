package com.conexia.riesgos.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;


public class NotContainsWhitespaceImpl implements ConstraintValidator<NotContainsWhitespace, String> {	

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return !StringUtils.isEmpty(value) && !StringUtils.containsWhitespace(value);
	}

}
