package com.conexia.riesgos.annotation;



import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.StringUtils;


public class NotEmptyIfAnotherFieldIsEmptyImpl
		implements ConstraintValidator<NotEmptyIfAnotherFieldIsEmpty, Object> {

	private String fieldName;	
	private String dependFieldName;

	@Override
	public void initialize(NotEmptyIfAnotherFieldIsEmpty annotation) {
		fieldName = annotation.fieldName();		
		dependFieldName = annotation.dependFieldName();
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {

		if (value == null) {
			return true;
		}

		try {
			String fieldValue = BeanUtils.getProperty(value, fieldName);
			String dependFieldValue = BeanUtils.getProperty(value, dependFieldName);			
			
			if (StringUtils.isEmpty(fieldValue) && StringUtils.isEmpty(dependFieldValue)) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
						.addPropertyNode(dependFieldName).addConstraintViolation();

				return false;
			}

		} catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}

		return true;
	}

}